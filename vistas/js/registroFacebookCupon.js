/*=============================================
BOTÓN FACEBOOK
=============================================*/

$(".facebookCupon").click(function() {
    

    FB.login(function(response) {
        
        validarUsuarioCupon();

    }, { scope: 'public_profile, email' })

})

/*=============================================
VALIDAR EL INGRESO
=============================================*/

function validarUsuarioCupon() {

    FB.getLoginStatus(function(response) {
        

        statusChangeCallbackCupon(response);

    })

}

/*=============================================
VALIDAMOS EL CAMBIO DE ESTADO EN FACEBOOK
=============================================*/

function statusChangeCallbackCupon(response) {

    if (response.status === 'connected') {
        

        testApiCupon();

    } else {

        swal({
                title: "¡ERROR!",
                text: "¡Ocurrió un error al ingresar con Facebook, vuelve a intentarlo!",
                type: "error",
                confirmButtonText: "Cerrar",
                closeOnConfirm: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    window.location = localStorage.getItem("rutaActual");
                }
            });

    }

}

/*=============================================
INGRESAMOS A LA API DE FACEBOOK
=============================================*/

function testApiCupon() {
    

    FB.api('/me?fields=id,name,email,picture', function(response) {

        

        if (response.email == null) {

            swal({
                    title: "¡ERROR!",
                    text: "¡Para poder ingresar al sistema debe proporcionar la información del correo electrónico!",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location = localStorage.getItem("rutaActual");
                    }
                });

        } else {

            

            var email = response.email;
            var nombre = response.name;
            var foto = "http://graph.facebook.com/" + response.id + "/picture?type=large";

            var datos = new FormData();
            datos.append("email", email);
            datos.append("nombre", nombre);
            datos.append("foto", foto);
            datos.append("cupon", "si");

            

            $.ajax({

                url: rutaOculta + "ajax/usuarios.ajax.php",
                method: "POST",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $('#modalRegistroConDescuento').modal('toggle');
                    $.blockUI({message: '<h1>Procesando...</h1>', css: { 
                      border: 'none', 
                      padding: '15px', 
                      backgroundColor: '#fff', 
                      '-webkit-border-radius': '10px', 
                      '-moz-border-radius': '10px', 
                      opacity: .5, 
                      color: '#000' 
                  } }); 
                  },
                success: function(respuesta) {
                    console.log(respuesta);

                    if (respuesta == "ok") {
                        
                       window.location = localStorage.getItem("rutaActual")+"cupon";
            
                    } 
                    else if(respuesta == "noCupon"){


                        cargaCarritoAbandonado(email);
                        
                        swal({
                            title: "QUE BUENO VERTE DE NUEVO!",
                            text: "¡Adelante, continua explorando!",
                            type: "success",
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                window.location = localStorage.getItem("rutaActual");
                            }
                        });

                    }
                    else {
                        swal({
                                title: "¡ERROR!",
                                text: "¡El correo electrónico " + email + " ya está registrado en el sistema!",
                                type: "error",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    FB.getLoginStatus(function(response) {
                                        if (response.status === 'connected') {
                                            FB.logout(function(response) {
                                                deleteCookie("fblo_338416820669046");
                                                setTimeout(function() {
                                                    window.location = rutaOculta + "salir";
                                                }, 500)
                                            });
                                            function deleteCookie(name) {
                                                document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                                            }
                                        }
                                    })
                                }
                            });
                    }
                },
                complete: function(){
                    $.unblockUI();
                   },
            })
        }
    })
}

/*=============================================
SALIR DE FACEBOOK
=============================================*/

$(".salir").click(function(e) {

    e.preventDefault();

    FB.getLoginStatus(function(response) {

        if (response.status === 'connected') {

            FB.logout(function(response) {

                deleteCookie("fblo_338416820669046");

                console.log("salir");

                setTimeout(function() {

                    window.location = rutaOculta + "salir";

                }, 500)

            });

            function deleteCookie(name) {

                document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

            }

        }

    })

})