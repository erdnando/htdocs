/*=============================================
BOTÓN FACEBOOK
=============================================*/

$(".facebookIngreso").click(function() {

    FB.login(function(response) {

        validarUsuarioIngreso();

    }, { scope: 'public_profile, email' })

})

/*=============================================
VALIDAR EL INGRESO
=============================================*/

function validarUsuarioIngreso() {

    FB.getLoginStatus(function(response) {

        statusChangeCallbackIngreso(response);

    })

}

/*=============================================
VALIDAMOS EL CAMBIO DE ESTADO EN FACEBOOK
=============================================*/

function statusChangeCallbackIngreso(response) {

    if (response.status === 'connected') {

        testApiIngreso();

    } else {

        swal({
                title: "¡ERROR!",
                text: "¡Ocurrió un error al ingresar con Facebook, vuelve a intentarlo!",
                type: "error",
                confirmButtonText: "Cerrar",
                closeOnConfirm: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    window.location = localStorage.getItem("rutaActual");
                }
            });

    }

}

/*=============================================
INGRESAMOS A LA API DE FACEBOOK
=============================================*/

function testApiIngreso() {

    FB.api('/me?fields=id,name,email,picture', function(response) {

        if (response.email == null) {

            swal({
                    title: "¡ERROR!",
                    text: "¡Para poder ingresar al sistema debe proporcionar la información del correo electrónico!",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                },

                function(isConfirm) {
                    if (isConfirm) {
                        window.location = localStorage.getItem("rutaActual");
                    }
                });

        } else {

            var email = response.email;

            var datos = new FormData();
            datos.append("emailLogin", email);
            datos.append("login", "fb");

            $.ajax({

                url: rutaOculta + "ajax/usuarios.ajax.php",
                method: "POST",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                success: function(respuesta) {
               console.log(respuesta);
                    if (respuesta == "ok") {
                        console.log("entrando al sistema fb");
                        //LOGIN OK
                        cargaCarritoAbandonado(email);

                        swal({
                            title: "QUE BUENO VERTE DE NUEVO!",
                            text: "¡Adelante, continua explorando!",
                            type: "success",
                            confirmButtonText: "Cerrar",
                            closeOnConfirm: false
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                window.location = "inicio";
                            }
                        });

                    } else {
                       //NO ESTA REGISTRADO EN EL SISTEMA
                        swal({
                                title: "¡ERROR!",
                                text: "¡El correo electrónico " + email + " no está registrado en el sistema!",
                                type: "error",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    FB.getLoginStatus(function(response) {
                                        if (response.status === 'connected') {
                                            FB.logout(function(response) {
                                                deleteCookie("fblo_338416820669046");
                                                setTimeout(function() {
                                                    window.location = rutaOculta + "salir";
                                                }, 500)
                                            });
                                            function deleteCookie(name) {
                                                document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                                            }
                                        }
                                    })
                                }
                            });
                    }
                }
            })

        }

    })

}


function cargaCarritoAbandonado(email){

            var datos = new FormData();
            datos.append("optCarritoEmail-email", email);
            datos.append("optCarritoEmail", "fb");

            $.ajax({
                url: rutaOculta + "ajax/usuarios.ajax.php",
                method: "POST",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                success: function(respuesta) {
               
                       // console.log("carrito recreado fb");
                       // console.log(JSON.parse(respuesta));
                        var arrCarrito = JSON.parse(respuesta);
                        var listaCarrito = [];

                        console.log(arrCarrito);
                        
                        for(var i=0;i<arrCarrito.length;i++){

                            //------------------------------------------------------------
                            var idProducto = arrCarrito[i].idProducto;
                            var imagen = arrCarrito[i].imagen;
                            var titulo = arrCarrito[i].titulo;
                            var precio = arrCarrito[i].precio;
                            var tipo = arrCarrito[i].tipo;
                            var peso = arrCarrito[i].peso;
                            var cantidad = arrCarrito[i].cantidad;

                            listaCarrito.push({
                                "idProducto": idProducto,
                                "imagen": imagen,
                                "titulo": titulo,
                                "precio": precio,
                                "tipo": tipo,
                                "peso": peso,
                                "cantidad": cantidad
                            });                            
                            //------------------------------------------------------------
                        localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));
                        var cantidadCesta = Number($(".cantidadCesta").html()) + 1;
                        var sumaCesta = Number($(".sumaCesta").html()) + Number(precio);
                        $(".cantidadCesta").html(cantidadCesta);
                        $(".sumaCesta").html(sumaCesta);

                        localStorage.setItem("cantidadCesta", cantidadCesta);
                        localStorage.setItem("sumaCesta", sumaCesta);

                        }

                        
                        

                }
            })

}

/*=============================================
SALIR DE FACEBOOK
=============================================*/

$(".salir").click(function(e) {

    e.preventDefault();

    FB.getLoginStatus(function(response) {

        if (response.status === 'connected') {

            FB.logout(function(response) {

                deleteCookie("fblo_338416820669046");

                console.log("salir");

                setTimeout(function() {

                    window.location = rutaOculta + "salir";

                }, 500)

            });

            function deleteCookie(name) {

                document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

            }

        }

    })

})