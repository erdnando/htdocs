<!--=====================================
FOOTER
======================================-->
<footer class="container-fluid">
	<div class="container">
		<div class="row">
		 	<!--=====================================
			CATEGORÍAS Y SUBCATEGORÍAS FOOTER
			======================================-->
			<div class="col-lg-5 col-md-6 col-xs-12 footerCategorias">
			<?php
				$url = Ruta::ctrRuta();
				$item = null;
				$valor = null;
				$categorias = ControladorProductos::ctrMostrarCategorias($item, $valor);

				foreach ($categorias as $key => $value) {
					echo '<div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
						<h4><a href="'.$url.$value["ruta"].'" class="pixelCategorias" titulo="'.$value["categoria"].'">'.$value["categoria"].'</a></h4>
						<hr>
						<ul>';
						$item = "id_categoria";
						$valor = $value["id"];
						$subcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);
						foreach ($subcategorias as $key => $value) {
							echo '<li><a href="'.$url.$value["ruta"].'" class="pixelSubCategorias" titulo="'.$value["subcategoria"].'">'.$value["subcategoria"].'</a></li>';
						}
						echo '</ul>
					</div>';
				}
			?>
			</div>
			<!--=====================================
			DATOS CONTACTO
			======================================-->
			<div class="col-md-3 col-sm-6 col-xs-12 text-left infoContacto">
				<h5>Dudas e inquietudes, contáctenos en:</h5>
				<br>
				<h5>
					<i class="fa fa-phone-square" aria-hidden="true"></i> (52) 33 3242 2256
					<br><br>
					<i class="fa fa-envelope" aria-hidden="true"></i> contacto@dumbbells.com
					<br><br>
					<i class="fa fa-map-marker" aria-hidden="true"></i> Calle Volcan Fogo 5964 Col Huentitan el bajo
					<br><br>
					Jalisco | México
				</h5>
				<iframe src="https://maps.google.com/maps?q=Calle%20volcan%20Fogo%205964%20col%20huentitan%20el%20bajo&t=&z=13&ie=UTF8&iwloc=&output=embed" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>	
			</div>

			<!--=====================================
			FORMULARIO CONTÁCTENOS
			======================================-->

			<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 " style="color: white;">  <!--formContacto-->
				<h4>RESUELVA SU INQUIETUD</h4>
				<!--<form role="form" method="post" onsubmit="return validarContactenos()">-->
			  		<input type="text" id="nombreContactenos" name="nombreContactenos" class="form-control" placeholder="Escriba su nombre" required> 
			   		<br>
   					<input type="email" id="emailContactenos" name="emailContactenos" class="	form-control" placeholder="Escriba su correo electrónico" required>  
   					<br>
	       			<textarea id="mensajeContactenos" name="mensajeContactenos" class="form-control" placeholder="Escriba su mensaje" rows="5" required></textarea>
					   <br>
					   <button  class="btn btn-default backColor pull-right" id="btnEnviarComentario" name="btnEnviarComentario" >Enviar</button>
	       			<!--<input type="submit" value="Enviar" class="btn btn-default backColor pull-right" id="enviar">  -->       
				<!--</form>-->
				<?php 
					//$contactenos = new ControladorUsuarios();
					//$contactenos -> ctrFormularioContactenos();
				?>
			</div>
		</div>
	</div>
</footer>

<!--=====================================
FINAL
======================================-->

<div class="container-fluid final">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12 text-left text-muted">
				<h5>&copy; 2020 Todos los derechos reservados. Sitio elaborado por @erdnando</h5>
			</div>
			<div class="col-sm-6 col-xs-12 text-right social">
			<ul>
			<?php
				
			$social = ControladorPlantilla::ctrEstiloPlantilla();

				$jsonRedesSociales = json_decode($social["redesSociales"],true);		

				foreach ($jsonRedesSociales as $key => $value) {
					echo '<li>
							<a href="'.$value["url"].'" target="_blank">
								<i class="fa '.$value["red"].' redSocial '.$value["estilo"].'" aria-hidden="true"></i>
							</a>
						</li>';
				}
			?>

			</ul>

			</div>

		</div>

	</div>

</div>


<div class="container">
<div id="cookie-bar" class="fixed" style="visibility:collapse">
	<div class="row" style="margin-left: 5px;margin-right: 5px;">
		<p>En este sitio utilizamos cookies propias y de terceros con la finalidad de mejorar tu experiencia. Te invitamos a conocer nuestro <a href="https://dumbbells.mx/aviso-privacidad.html" target="_blank" class="cb-policy">Aviso de privacidad</a>
		
		</p>
	</div>
	<div class="row">
	   <a href="#" class="cb-enable" onclick="this.parentNode.parentNode.style.display = 'none'">Aceptar</a>
	</div>
  </div>
</div>