<?php

echo "webhook.....billpocket";
$data = json_decode(file_get_contents("php://input"));

//https://dumbbells.mx/api/webhook/listenermx86676660000012
/*$data = new stdClass();
$data->checkout= 'c722617c-373a-4c2c-8b27-95d441d76ce8';
$data->externalId= 'T825347315';
$data->total= '355.00';
$data->authorization= 'BP8392';
$data->ticketUrl= '93f8512c890cd1690911124ea55025945c27cd41';
$data->opId= '61195';
$data->maskedPAN= '424242******4242';
$data->message= 'APROBADA';
$data->token= '627facc9b321bed6d4b5bcb7c22832ca';
*/

if($data->checkout !=''){
    //REGISTRA RESPUESTA DE BILLPOCKET DE LA VENTA

 

    try{
        //REGISTRA EN LOG
        error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "checkout:".$data->checkout."\n".
                                            "externalId:".$data->externalId."\n".
                                            "total:".$data->total."\n".
                                            "authorization:".$data->authorization."\n".
                                            "ticketUrl:".$data->ticketUrl."\n".
                                            "opId:".$data->opId."\n".
                                            "maskedPAN:".$data->maskedPAN."\n".
                                            "message:".$data->message."\n".
                                            "token:".$data->token."\n".
                                            "signature:".$data->signature."\n".
                                            "=============================================================="."\n", 3, "billpocket.txt");

    }catch(Exception $e){
        error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "Error Escribir:".$e->getMessage()."\n".
                                            "Response:". json_decode($data, true)."\n".
                                            "=============================================================="."\n", 3, "billpocket.txt");
    }



    //-----------------------------------------------------------------------------------------------------------------------------------
    try{
        //REGISTRA EN DB LAS APROBADAS
        if($data->message =='APROBADA'){
            $registraVenta = new ControladorUsuarios();

            $registraVenta -> ctrInsertaVentaBillPocket($data,$usuario);

            error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
            "Transaccion OK:\n".
            "Response: ".json_encode($registraVenta) ."\n".
            "=============================================================="."\n", 3, "billpocket.txt");


         }else{
            error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
            "Transaccion NO APROBADA:"."\n".
            "Response:\n".
            "=============================================================="."\n", 3, "billpocket.txt");

         }
    }catch(Exception $e){
        error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "Error InsertarDB:".$e->getMessage()."\n".
                                            "Response:". json_decode($data, true)."\n".
                                            "=============================================================="."\n", 3, "billpocket.txt");
    }
//---------------------------------------------------------------------------------------------------------------------------------------

}else{
    //REGISTRA PETICION NULA POR PARTE DE BILLPOCKET
    error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "Error NULL:"."WEBHOOK con checkout en nulo"."\n".
                                            "Response:". json_decode($data, true)."\n".
                                            "=============================================================="."\n", 3, "billpocket.txt");
    
}



?>