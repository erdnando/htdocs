<?php

echo "webhook carrito abandonado.....";
$data = json_decode(file_get_contents("php://input"));

    try{
        //REGISTRA EN LOG
        error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "buscando carritos abandonados:\n".
                                            "=============================================================="."\n", 3, "cronCarrito.txt");

           $carrito = new ControladorCarrito();
           $carritos = $carrito -> ctrObtenrCarritosAbandonados();


            foreach ($carritos as $item=>$carro) {
                
                $idCarrito = $carro["id"];
                //array of products
                $carrito_json = json_decode($carro["carrito_json"]);
                $fechaabandonado = $carro["ultima_modificacion"];
                $id_usuario = $carro["id_usuario"];

                $usuario = ModeloUsuarios::mdlMostrarUsuario("usuarios", "id", $id_usuario);
                $email = $usuario["email"];
                $nombre = $usuario["nombre"];
                $veces = 0;
                $altura= 100;
                $logCarrito="";

                //var_dump($carrito_json);
                foreach($carrito_json as $key => $producto)
                {
                    $strCarrito .= '<div clas="row itemCarrito" style="color: white;display: flex;margin-left: -25px;font-size: smaller;height: 43px;">
                                        <table style="width:100%">
                                         <tr>
                                           <td style="width:14px">
                                            <figure style="margin:5px;">
                                            <img style="border-radius: 20px;margin-top:-4px;width:40px;height:40px" src="'.$producto->imagen .'" class="img-thumbnail">
                                            </figure>
                                           </td>
                                           <td  width=* style="text-align:center;">
                                              <p class="tituloCarritoCompra text-left">'.$producto->titulo .'</p>
                                           </td>
                                           <td style="width:75px;font-size: smaller;">
                                           <p class="precioCarritoCompra text-center">MXN $<span>'.$producto->precio .'</span></p>
                                           </td>
                                         </tr>
                                        </table>
                                   </div>';
                    $logCarrito .='\n-->'.$email . '->' .$producto->titulo.'->'.$producto->precio.'\n';
                    $veces++;
                }

                //send email
                $variables = array();
                $variables['ALTURA'] = ($altura + ($veces*70)) . "px";
                $variables['CARRITO'] = $strCarrito;
                $variables['NOMBRE'] = $nombre;
                

                $template = file_get_contents("plantilla-carrito-abandonado-correo.html",true);

                foreach($variables as $key => $value)
                {
                    $template = str_replace('{{ '.$key.' }}', $value, $template);
                }

                $datosCorreo = array("tituloFrom"=>"Dumbbells - carrito pendiente!",
                            "subject"=> "Tu carrito sigue esperando por ti ",
                            "address"=> $email,
                            "msgHtml"=> $template);

            $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
                //actualiza estatus notificado a 1
             ModeloCarrito::mdlActualizaCarritoNotificado("carritos", "notificado", 1, "id_usuario", $id_usuario);
            
             
             error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "Carrito detectado:\n".
                                            $logCarrito ."\n".
                                            "=============================================================="."\n", 3, "cronCarrito.txt");

            }


    }catch(Exception $e){
        error_log(date('m/d/Y h:i:s a', time())."\n=============================================================="."\n".
                                            "Error Escribir:".$e->getMessage()."\n".
                                            "Response Error al ejecutar proceso de carrito abandonado:\n".
                                            "=============================================================="."\n", 3, "cronCarrito.txt");
    }
//---------------------------------------------------------------------------------------------------------------------------------------
?>