<?php

$url = Ruta::ctrRuta();

if(!isset($_SESSION["validarSesion"])){

   echo '<script>window.location = "'.$url.'";</script>';

   exit();

}


/*=============================================
PAGO BILL POCKET RESPONSE
=============================================*/
   echo '<script>

   localStorage.removeItem("listaProductos");
   localStorage.removeItem("cantidadCesta");
   localStorage.removeItem("sumaCesta");

   </script>';
?>

<div class="container">

<div class="row">
  
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center error404">
               
    <h1><small>Gracias</small></h1>
    
    <h2>La compra ha sido exitosa! En breve le llegará un correo con la información de su compra</h2>

    <br>
      <a href="<?php echo $url; ?>" ><button class="btn btn-default backColor btn-lg">CONTINUAR COMPRANDO</button></a>

  </div>

</div>

</div>