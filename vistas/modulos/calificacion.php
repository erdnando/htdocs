
<div class="container modalTitulo">
			<h3 class="backColor">CALIFICA ESTE PRODUCTO</h3>
			
			<form method="post" onsubmit="return validarComentarioCalificacion()">
				<input type="hidden" value="" id="idVenta" name="idVenta">
				<h1 class="text-center" id="estrellas">
		       		<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
					<i class="fa fa-star text-success" aria-hidden="true"></i>
				</h1>

				<div class="form-group text-center">
		       		<label class="radio-inline"><input type="radio" name="puntaje" value="0.5">0.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.0">1.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="1.5">1.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.0">2.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="2.5">2.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.0">3.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="3.5">3.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.0">4.0</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="4.5">4.5</label>
					<label class="radio-inline"><input type="radio" name="puntaje" value="5.0" checked>5.0</label>
				</div>
				<div class="form-group">
			  		<label for="comment" class="text-muted">Tu opinión acerca de este producto: <span><small>(máximo 300 caracteres)</small></span></label>
			  		<textarea class="form-control" rows="5" id="comentario" name="comentario" maxlength="300" required></textarea>
			  		<br>
					<input type="submit" class="btn btn-default backColor btn-block" value="ENVIAR">
				</div>
				<?php

					$actualizarComentario = new ControladorUsuarios();
					$actualizarComentario -> ctrSendCalificacion();
				?>
			</form>
		</div>