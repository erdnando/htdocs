
<?php

/*=============================================
CREADOR DE IP
=============================================*/

//https://www.browserling.com/tools/random-ip

$ip = $_SERVER['REMOTE_ADDR'];

//$ip = "201.141.216.169";

//http://www.geoplugin.net/

$informacionPais = file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip);

$datosPais = json_decode($informacionPais);

$pais = $datosPais->geoplugin_countryName;

$enviarIp = ControladorVisitas::ctrEnviarIp($ip, $pais);

$totalVisitas = ControladorVisitas::ctrMostrarTotalVisitas();

?>


<!--=====================================
CONDICIONES
======================================-->
<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">

		    <div class="col-md-3 col-sm-3 col-xs-12 text-center">
		 	    <li ><i  class="fa fa-plane" aria-hidden="true" style=" color: rgb(11, 13, 14);font-size:28px"></i></li>
				<p class="text-muted text-center" style="font-size:12px;color: rgb(11, 13, 14);"> Recibe de 3 a 5 dias</p>
				<p class="text-muted text-center" style="font-size:12px;margin-top: -10px;color: rgb(11, 13, 14);"> DHL/FEDEX/ESTAFETA/REDPACK</p>
			</div>



			<div class="col-md-3 col-sm-3 col-xs-12 text-center">
			<li ><i  class="fa fa-heart" aria-hidden="true" style=" color: rgb(11, 13, 14);font-size:28px"></i></li>
				<p class="text-muted text-center" style="font-size:12px;color: rgb(11, 13, 14);"> Envío gartis</p>
				<p class="text-muted text-center" style="font-size:12px;margin-top: -10px;color: rgb(11, 13, 14);"> En compras mayores a $999</p>
			</div>

			<div class="col-md-3 col-sm-3 col-xs-12 text-center">
			<li ><i  class="fa fa-cart-plus" aria-hidden="true" style=" color: rgb(11, 13, 14);font-size:28px"></i></li>
				<p class="text-muted text-center" style="font-size:12px;color: rgb(11, 13, 14);"> Compra +10</p>
				<p class="text-muted text-center" style="font-size:12px;margin-top: -10px;color: rgb(11, 13, 14);"> Obtén precio de mayoreo</p>
			</div>


			<div class="col-md-3 col-sm-3 col-xs-12 text-center">
			<li ><i  class="fa fa-credit-card" aria-hidden="true" style=" color: rgb(11, 13, 14);font-size:28px"></i></li>
				<p class="text-muted text-center" style="font-size:12px;color: rgb(11, 13, 14);"> Pago cifrado y seguro</p>
				<p class="text-muted text-center" style="font-size:12px;margin-top: -10px;color: rgb(11, 13, 14);"> Aceptamos todas las tarjetas de crédito</p>
			</div>

		</div>

	</div>

</div>

<!--=====================================
COMENTARIOS 4 ultimos
======================================-->
<div class="container-fluid productos">
<div class="container">
<div class="row">
	<div class="col-xs-12 tituloDestacado">
	   <div class="col-sm-6 col-xs-12">
			<h1><small>COMENTARIOS </small></h1>
		</div>
		<div class="col-sm-6 col-xs-12">
			<h1><small> </small></h1>
		</div>
	</div>
	
</div>
</div>
</div>


<div class="container-fluid well well-sm">
	<div class="container">
        <!--  ------------------------------------------------------------------------------------------------------------  -->
		
		<div class="row">
			<?php
			$comentarios = ControladorUsuarios::ctrMostrar5ComentariosPerfil();
			$cantidad = 0;

				foreach ($comentarios as $key => $value){
					if($value["comentario"] != ""){
						$cantidad += 1;//count($value["id"]);
					}
				}
			?>
		</div>

		<!-- -------------------------------------2a parte----------------------------------------------------------------- -->
		<div class="row ">
		<?php
		foreach ($comentarios as $key => $value) {
			if($value["comentario"] != ""){
				$item = "id";
				$valor = $value["id_usuario"];
				$usuario = ControladorUsuarios::ctrMostrarUsuario($item, $valor);
				$infoproducto = ControladorProductos::ctrMostrarInfoProducto($item, $value["id_producto"]);

				echo '<div class=" col-md-3 col-sm-6 col-xs-12" style="text-align:center;" >
					<div class="panel panel-default" style="margin-bottom:0px;">
						<div class="col-md-12 col-sm-12 col-xs-12 ">';					
								echo '<img value="1"  class="img-thumbnail" style="margin-bottom:5px;margin-top:10px;padding:0px!important" src="'.$servidor.$infoproducto["portada"].'" alt="'.$infoproducto["titulo"].'">';					 
				  echo '</div>
				


				      <div class="panel-heading text-uppercase">
				      	'.$usuario["nombre"].'
				      	<span class="text-right">';
				      	if($usuario["modo"] == "directo"){
				      		if($usuario["foto"] == ""){
				      			echo '<img class="img-circle pull-right" src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" width="20%">';	
				      		}else{
				      			echo '<img class="img-circle pull-right" src="'.$url.$usuario["foto"].'" width="20%">';	
				      		}
				      	}else{
				      		echo '<img class="img-circle pull-right" src="'.$usuario["foto"].'" width="20%">';	
				      	}
				      	echo '</span>
				      </div>
				      <div class="panel-body"><small>'.$value["comentario"].'</small></div>
				      <div class="panel-footer">';

				      	switch($value["calificacion"]){
							case 0.5:
							echo '<i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 1.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 1.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 2.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 2.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 3.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 3.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 4.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-o text-success" aria-hidden="true"></i>';
							break;
							case 4.5:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star-half-o text-success" aria-hidden="true"></i>';
							break;
							case 5.0:
							echo '<i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>
								  <i class="fa fa-star text-success" aria-hidden="true"></i>';
							break;
						}
				      echo '</div>
				    </div>
				</div>';
			}
		}
		?>
		</div>
	
        <!--  ------------------------------------------------------------------------------------------------------------  -->


	</div>

</div>

<!--=====================================
BREADCRUMB VISITAS
======================================-->
<div class="container-fluid well well-sm">

	<div class="container">
	
		<div class="row">

			<ul class="breadcrumb lead">

			<h2 class="pull-right"><small>Tu eres nuestro visitante # <?php echo $totalVisitas["total"];?></small></h2>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
MÓDULO VISITAS
======================================-->

<div class="container-fluid">
	
	<div class="container">
		
		<div class="row">

		<?php

		$paises = ControladorVisitas::ctrMostrarPaises();

		$coloresPaises = array("#09F","#900","#059","#260","#F09","#02A");	

		$indice = -1;

		foreach($paises as $key => $value){

			$promedio = $value["cantidad"] * 100 / $totalVisitas["total"];

			$indice++;
	
			echo '<div class="col-md-2 col-sm-4 col-xs-12 text-center">
				
					<h2 class="text-muted">'.$value["pais"].'</h2>

					<input type="text" class="knob" value="'.round($promedio).'" data-width="90" data-height="90" data-fgcolor="'.$coloresPaises[$indice].'" data-readonly="true">
 
					<p class="text-muted text-center" style="font-size:12px"> '.round($promedio).'% de las visitas</p>
	
				</div>';
		}


		?>

		</div>

	</div>

</div>