<!--=====================================
VALIDAR SESIÓN
======================================-->

<?php

$url = Ruta::ctrRuta();
$servidor = Ruta::ctrRutaServidor();

if(!isset($_SESSION["validarSesion"])){

	echo '<script>
	
		window.location = "'.$url.'";

	</script>';

	exit();

}

?>

<!--=====================================
BREADCRUMB PERFIL
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">INICIO</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
SECCIÓN RECOMIENDANOS
======================================-->

<div class="container-fluid">

	<div class="container" id="tabsRecomiendanos">
		<ul class="nav nav-tabs">
	  		<li class="active" > 	
	  			<a data-toggle="tab" href="#recomiendanos" id="tabRecomiendanos">
	  			<i class="fa fa-user"></i> RECOMIENDANOS</a>
	  		</li>
		</ul>
		<div class="tab-content">
			<!--=====================================
			PESTAÑA RECOMIENDANOS
			======================================-->
			<!--in active-->
		  	<div id="recomiendanos" class="tab-pane fade in active">
		    	
				<div class="row">
						<div class="col-md-3 col-sm-4 col-xs-12 text-center">
							<br>
							<figure id="imgPerfil">
							<?php
							echo '<input type="hidden" value="'.$_SESSION["id"].'" id="idUsuario" name="idUsuario">
							      <input type="hidden" value="'.$_SESSION["password"].'" name="passUsuario">
								  <input type="hidden" value="'.$_SESSION["foto"].'" name="fotoUsuario" id="fotoUsuario">
								  <input type="hidden" value="'.$_SESSION["email"].'" name="email" id="email">
								  <input type="hidden" value="'.$_SESSION["nombre"].'" name="nombreLogeado" id="nombreLogeado">
							      <input type="hidden" value="'.$_SESSION["modo"].'" name="modoUsuario" id="modoUsuario">';

							  echo '<img src="'.$servidor.'vistas/img/plantilla/recomiendanos.jpg" class="img-thumbnail">';
							?>
							</figure>
							<br>
						</div>	

						<div class="col-md-9 col-sm-8 col-xs-12">
						<br>	
						<?php
							echo '<label class="control-label text-muted text-uppercase">'.$_SESSION["nombre"].':</label>
									<div class="input-group">
									<BR>
									Recuerda que por cada recomendado que se registre tendrás beneficios que te compartiremos en tu correo electrónico.
										<BR>
										<BR>
										No hay límite. Invita a todos tus amigos y familiares para que todos se vean beneficiados con nuestros cupones de descuento.
										<BR>
									</div>
									<br>
									<label class="control-label text-muted text-uppercase">Correo electrónico:</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
										<input type="text" id="emailReferenciado" name="emailReferenciado" class="form-control" value="" placeholder="Ingresa un correo a la vez" >
									</div>
									<br>
									<br>';
						?>
							   <button  class="btn btn-default backColor pull-right" id="btnEnviarCorreoInvitado" name="btnEnviarCorreoInvitado" 
							            style="margin-right: 15px;margin-top:-74px;position: relative;z-index: 1000;margin-right: 0px;">Invitar</button>  
						</div>
				</div>

		  	</div>
          
			  <!--=====================================
			  FIN TABS
			======================================-->


		</div>

	</div>

</div>



<!--=====================================
VENTANA MODAL PARA CRUD DE FORMAS DE PAGO
======================================-->

<div  class="modal fade modalFormulario" id="modalFormaPago" role="dialog">
	
	<div class="modal-content modal-dialog">
		
		<div class="modal-body modalTitulo">
			
			<h3 class="backColor"> <span id=pagoTitulo></span> </h3>

			<button  type="button" class="close" data-dismiss="modal">&times;</button>

			<form method="post" onsubmit="return validarformapago(this)">

				<div class="col-md-12 col-sm-12 col-xs-12">
						<?php
							echo '
									<input type="hidden" value="" id="hdIdformapago" name="hdIdformapago">
									<label class="control-label text-muted text-uppercase" for="pagotarjeta">No. Tarjeta:</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
										<input type="text" class="form-control" onkeypress="justNumbers(event)" onblur="detectaTipoTC(this)"  id="pagotarjeta" name="pagotarjeta" maxlength="16"  placeholder="Escriba su no. de tarjeta">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagonombre">Nombre:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" style="text-transform: uppercase;" class="form-control" id="pagonombre" name="pagonombre" maxlength="80" placeholder="Escriba su nombre como en la tarjeta">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagovigencia">Vigencia (mm/yy):</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
										<input type="text" class="form-control" id="pagovigencia" name="pagovigencia" maxlength="5" placeholder="Escriba la vigencia de la tarjeta, ejemplo 01/23">
									</div>
								<br>

								<label class="control-label text-muted text-uppercase" for="pagotipo">tipo tarjeta:</label>
								<div class="input-group">
										<span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
										<input type="text" style="text-transform: uppercase;" class="form-control" readonly id="pagotipo" name="pagotipo" maxlength="30" placeholder="Tipo tarjeta">
									</div>
								<br>

								

								<button type="submit" class="btn btn-default backColor btn-md pull-right">Aceptar</button>';
						?>
						</div>
				<?php
					$actualizarPago = new ControladorUsuarios();
					$actualizarPago -> ctrActualizarformapago();
				?>
			</form>

		</div>

		<div class="modal-footer">
      	
      	</div>

	</div>

</div>
