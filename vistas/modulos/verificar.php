<!--=====================================
VERIFICAR
======================================-->

<?php

	$usuarioVerificado = false;
	
	$item = "EmailEncriptado";
	$valor =  $rutas[1];

	//Obtiene el usuario q ha verificado (sminetworks)
	$respuesta = ControladorUsuarios::ctrMostrarUsuario($item, $valor);

	if($valor == $respuesta["emailEncriptado"]){

		$id = $respuesta["id"];
		$item2 = "verificacion";
		$valor2 = 0;

		//Marca usuario como verificado (sminetworks) en la tabla usuarios con 0
		$respuesta2 = ControladorUsuarios::ctrActualizarUsuario($id, $item2, $valor2);
		//Obtiene el cupon por el registro generico
		$cuponRegistro = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','REGISTRO');


		
		//Obtiene el correo del q lo recomendo                              //erdnando@sminetworks.com
		$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta["email"]);
		//erdnando@gmail.com

		//si es un recomendado
		if($recomendado != null){
			//Marca el campo aplicado como 1                                            //erdnando@sminetworks.com
			$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta["email"]);

			if($recomendadoAplicado=="ok"){

				//Obtiene el cupon de un nivel1
				$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');

				$variables = array();
				$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
				$variables['NOMBRE'] = $recomendado["recomendadopor"];

				$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

				foreach($variables as $key => $value)
				{
					$template = str_replace('{{ '.$key.' }}', $value, $template);
				}
				
				//-------------------------------------------------------------------------
				$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
							   "subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
							   "address"=> $recomendado["recomendadopor"],
							   "msgHtml"=> $template);

				$respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);

				//---------------------------------------------------------------------------
			}
		}

		if($respuesta2 == "ok"){

			$usuarioVerificado = true;
			if($respuesta["cuponregistro"]=="si"){
				/*=============================================
					    	ENVÍO CORREO ELECTRÓNICO
				=============================================*/
				$variables = array();
				$variables['CUPON'] = $cuponRegistro["codigo"];
				$variables['NOMBRE'] = $respuesta["nombre"];
				$template = file_get_contents("controladores/plantilla-registro-nuevo.html",true);
				
				foreach($variables as $key => $value)
				{
					$template = str_replace('{{ '.$key.' }}', $value, $template);
				}





				$datosCorreo = array("tituloFrom"=>"!Su cupón de descuento por registrarse!",
							   "subject"=> "!Su cupón de descuento por registrarse al ecommerce Dummbells!",
							   "address"=> $respuesta["email"],
							   "msgHtml"=> $template);

				$respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);

				if($respuestaCorreo!="ok"){
					echo '<script> 
							swal({
								  title: "¡ERROR!",
								  text: "¡Ha ocurrido un problema enviando cambio de contraseña a '.$_POST["passEmail"].'!,
								  type:"error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										window.location = '.$url.';
									}
							});
						</script>';
				}else{
					$_SESSION["validarSesionAux"] = "ok";
						echo '<script> 
							swal({
								  title: "Registro exitoso!",
								  html:true,
								  text: "¡Hemos verificado su correo electrónico y enviado su cupón a su correo, ya puede ingresar al sistema",
								  type:"success",
								  confirmButtonText: "Aceptar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										window.location = '.$url.';
									}
							});
						</script>';

				}

				//=============================================
			}
		}
	}
?>

<div class="container">
	
	<div class="row">
	 
		<div class="col-xs-12 text-center verificar">
			
			<?php

				if($usuarioVerificado){

					if($respuesta["cuponregistro"]=="si"){
						echo '<h3>Gracias</h3>
						<h2><small>¡Hemos verificado su correo electrónico y enviado su cupón a su correo, ya puede ingresar al sistema!</small></h2>
						<br>
						<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default backColor btn-lg">INGRESAR</button></a>';
					}else{
						echo '<h3>Gracias</h3>
						<h2><small>¡Hemos verificado su correo electrónico, ya puede ingresar al sistema!</small></h2>
						<br>
						<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default backColor btn-lg">INGRESAR</button></a>';
					}

					
				

				}else{

					echo '<h3>Error</h3>

					<h2><small>¡No se ha podido verificar el correo electrónico,  vuelva a registrarse!</small></h2>

					<br>

					<a href="#modalRegistro" data-toggle="modal"><button class="btn btn-default backColor btn-lg">REGISTRO</button></a>';


				}

			?>

		</div>

	</div>

</div>

