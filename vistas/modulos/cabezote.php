<?php

$servidor = Ruta::ctrRutaServidor();
$url = Ruta::ctrRuta();

/*=============================================
INICIO DE SESIÓN USUARIO
=============================================*/

if(isset($_SESSION["validarSesion"])){

	if($_SESSION["validarSesion"] == "ok"){

		echo '<script>
			localStorage.setItem("usuario","'.$_SESSION["id"].'");
		</script>';
	}

}
/*=============================================
API DE GOOGLE
=============================================*/

// https://console.developers.google.com/apis
// https://github.com/google/google-api-php-client

/*=============================================
CREAR EL OBJETO DE LA API GOOGLE
=============================================*/

$cliente = new Google_Client();
$cliente->setAuthConfig('modelos/client_secret.json');
$cliente->setAccessType("offline");//cambiar a online cuando sea productivo
$cliente->setScopes(['profile','email']);

/*=============================================
RUTA PARA EL LOGIN DE GOOGLE
=============================================*/

$rutaGoogle = $cliente->createAuthUrl();

/*=============================================
RECIBIMOS LA VARIABLE GET DE GOOGLE LLAMADA CODE
=============================================*/

if(isset($_GET["code"])){

	$token = $cliente->authenticate($_GET["code"]);

	$_SESSION['id_token_google'] = $token;

	$cliente->setAccessToken($token);

}

/*=============================================
RECIBIMOS LOS DATOS CIFRADOS DE GOOGLE EN UN ARRAY
=============================================*/

if($cliente->getAccessToken()){

 	$item = $cliente->verifyIdToken();

 	$datos = array("nombre"=>$item["name"],
				   "email"=>$item["email"],
				   "foto"=>$item["picture"],
				   "password"=>"null",
				   "modo"=>"google",
				   "cupon"=>"no",
				   "verificacion"=>0,
				   "emailEncriptado"=>"null");

		$respuesta = ControladorUsuarios::ctrRegistroRedesSociales($datos);
	 
	// var_dump($respuesta);
	//DETECTA SI SE ESTA REGISTRANDO O REINGRESANDO CON GOOGLE
	//cargaCarritoAbandonadoGoogle('.$item["email"].');
	 if($respuesta=="reingreso"){

		$datosUsuario = ModeloUsuarios::mdlMostrarUsuario("usuarios","email", $item["email"]);
		$datosCarrito = ModeloUsuarios::mdlObtenerCarritoUsuario("carritos","id_usuario", $datosUsuario["id"]);


		$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$item["email"]);
		//si es un recomendado
		if($recomendado != null){
			//Marca el campo aplicado como 1                                           
			$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$item["email"]);
		}





		$arrCarrito = $datosCarrito["carrito_json"];
		//var_dump($arrCarrito);

		echo '<script>

		swal({
			title: "QUE BUENO VERTE DE NUEVO!",
			text: "¡Adelante, continua explorando!",
			type: "success",
			confirmButtonText: "Cerrar",
			closeOnConfirm: false
		},
		function(isConfirm) {
			if (isConfirm) {

				//reload items carrito
				var listaCarrito = [];
				var arrCarrito = '.$arrCarrito.';
				
				$(".cantidadCesta").html("0")
				var cantidadCesta=0;

				for(var i=0;i<arrCarrito.length;i++){
					//------------------------------------------------------------
					var idProducto = arrCarrito[i].idProducto;
					var imagen = arrCarrito[i].imagen;
					var titulo = arrCarrito[i].titulo;
					var precio = arrCarrito[i].precio;
					var tipo = arrCarrito[i].tipo;
					var peso = arrCarrito[i].peso;
					var cantidad = arrCarrito[i].cantidad;

					listaCarrito.push({
						"idProducto": idProducto,
						"imagen": imagen,
						"titulo": titulo,
						"precio": precio,
						"tipo": tipo,
						"peso": peso,
						"cantidad": cantidad
					});   

					//------------------------------------------------------------
					localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));
					cantidadCesta = Number($(".cantidadCesta").html()) + 1;
					var sumaCesta = Number($(".sumaCesta").html()) + Number(precio);

					$(".cantidadCesta").html(cantidadCesta);
					$(".sumaCesta").html(sumaCesta);

					localStorage.setItem("cantidadCesta", cantidadCesta);
					localStorage.setItem("sumaCesta", sumaCesta);
				}


				window.location = localStorage.getItem("rutaActual");
			}
		});
	
		 </script>';

	 }else if($respuesta=="registro"){
		echo '<script> 
				swal({
				title: "¡Gracias por registrarte!",
				text: "¡Su cupón ha sido enviado a su correo, gracias por participar!",
				type: "success",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
				},

				function(isConfirm){
						if (isConfirm) {	  
							window.location = localStorage.getItem("rutaActual");
							}
				});
			</script>';
	 }
}
?>

<!--=====================================
TOP
======================================-->

<div class="container-fluid barraSuperior" id="top">
	
	<div class="container">
		
		<div class="row">
	
			<!--=====================================
			SOCIAL
			======================================-->

			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 social">
				
				<ul>	

					<?php

					$social = ControladorPlantilla::ctrEstiloPlantilla();
                    //var_dump($social["redesSociales"]);
					$jsonRedesSociales = json_decode($social["redesSociales"],true);		
					
					foreach ((array)$jsonRedesSociales as $key => $value) {

						echo '<li>
								<a href="'.$value["url"].'" target="_blank">
									<i title="'.$value["url"].'" class="fa '.$value["red"].' redSocial '.$value["estilo"].'" aria-hidden="true"></i>
								</a>
							</li>';
					}

					?>
			
				</ul>

			</div>

			<!--=====================================
			REGISTRO
			======================================-->

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 registro">
				
				<ul>

				<?php

				if(isset($_SESSION["validarSesion"])){

					if($_SESSION["validarSesion"] == "ok"){

						if($_SESSION["modo"] == "directo"){

							if($_SESSION["foto"] != ""){

								echo '<li>

										<img class="img-circle" src="'.$url.$_SESSION["foto"].'" width="10%">

									 </li>';

							}else{

								echo '<li>

									<img class="img-circle" src="'.$servidor.'vistas/img/usuarios/default/anonymous.png" width="10%">

								</li>';

							}

							echo '<li>|</li>
							 <li><a href="'.$url.'perfil">Ver Perfil</a></li>
							 <li>|</li>
							 <li><a href="'.$url.'salir">Salir</a></li>
							 <li>|</li>
							 <li><a href="'.$url.'recomiendanos">Recomiendanos</a></li>';


						}

						if($_SESSION["modo"] == "facebook"){

							echo '<li>

									<img class="img-circle" src="'.$_SESSION["foto"].'" width="10%">

								   </li>
								   <li>|</li>
						 		   <li><a href="'.$url.'perfil">Ver Perfil</a></li>
						 		   <li>|</li>
									<li><a href="'.$url.'salir">Salir</a></li>
									<li>|</li>
									<li><a href="'.$url.'recomiendanos">Recomiendanos</a></li>';

						}

						if($_SESSION["modo"] == "google"){

							echo '<li>

									<img class="img-circle" src="'.$_SESSION["foto"].'" width="10%">

								   </li>
								   <li>|</li>
						 		   <li><a href="'.$url.'perfil">Ver Perfil</a></li>
									<li>|</li>
									<li><a href="'.$url.'salir">Salir</a></li>
									<li>|</li>
									<li><a href="'.$url.'recomiendanos">Recomiendanos</a></li>';

						}

					}

				}else{

					echo '<li><a href="#modalIngreso" data-toggle="modal">Ingresar</a></li>
						  <li>|</li>
						  <li><a href="#modalRegistro" data-toggle="modal">Crear una cuenta</a></li>';

				}

				?>
	
				</ul>

			</div>	

		</div>	

	</div>

</div>

<!--=====================================
HEADER
======================================-->

<header class="container-fluid">
	
	<div class="container">
		
		<div class="row" id="cabezote">

			<!--=====================================
			LOGOTIPO
			======================================-->
			
			<div class="col-lg-3 col-md-3 col-sm-2 col-xs-12" id="logotipo">
				
				<a href="<?php echo $url; ?>">
						
					<img style="width:100%" src="<?php echo $servidor.$social["logo"]; ?>" class="img-responsive">

				</a>
				
			</div>

			<!--=====================================
			BLOQUE CATEGORÍAS Y BUSCADOR
			======================================-->

			<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
					
				<!--=====================================
				BOTÓN CATEGORÍAS
				======================================-->

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 backColor" id="btnCategorias" style="border-radius: 5px;">
					
					<p style="cursor:pointer">CATEGORÍAS
					
						<span class="pull-right">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</span>
					
					</p>

				</div>

				<!--=====================================
				BUSCADOR
				======================================-->
				<div class="input-group col-lg-8 col-md-8 col-sm-8 col-xs-12" id="buscador">
					<input type="search" name="buscar" class="form-control" placeholder="Buscar...">	
					<span class="input-group-btn">
						<a href="<?php echo $url; ?>buscador/1/recientes">
							<button class="btn btn-default backColor" type="submit">
								<i class="fa fa-search"></i>
							</button>
						</a>
					</span>
				</div>
			</div>

			<!--=====================================
			CARRITO DE COMPRAS
			======================================-->
			<div class="col-lg-3 col-md-3 col-sm-2 col-xs-12" id="carrito">
				<a href="<?php echo $url;?>carrito-de-compras">
					<button class="btn btn-default pull-left backColor"> 
						<i class="fa fa-shopping-cart" aria-hidden="true"></i>
					</button>
				   <p>TU CESTA <span class="cantidadCesta"></span> <br> MXN $ <span class="sumaCesta"></span></p>	
				</a>
			</div>
		</div>
		<!--=====================================
		CATEGORÍAS
		======================================-->

		<div class="col-xs-12 backColor" id="categorias">
			<?php
				$item = null;
				$valor = null;
				$categorias = ControladorProductos::ctrMostrarCategorias($item, $valor);
				foreach ($categorias as $key => $value) {
					echo '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
							<h4>
								<a href="'.$url.$value["ruta"].'" class="pixelCategorias" titulo="'.$value["categoria"].'">'.$value["categoria"].'</a>
							</h4>
							<hr>
							<ul>';

							$item = "id_categoria";
							$valor = $value["id"];
							$subcategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);
							
							foreach ($subcategorias as $key => $value) {
									echo '<li><a href="'.$url.$value["ruta"].'" class="pixelSubCategorias" titulo="'.$value["subcategoria"].'">'.$value["subcategoria"].'</a></li>';
								}	
							echo '</ul>
						</div>';
				}

			?>	

		</div>

	</div>

</header>
<div id="loader"></div>
<input type="hidden" id="tipoLogin" name="tipoLogin" value="<?php echo $tipoLogin;?>">
<!--=====================================
VENTANA MODAL PARA EL REGISTRO
======================================-->


<div class="modal fade modalFormulario" id="modalRegistro" role="dialog">
    <div class="modal-content modal-dialog">
        <div class="modal-body modalTitulo">
        	<h3 class="backColor">REGISTRARSE</h3>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
			<!--=====================================
			REGISTRO FACEBOOK
			======================================-->
			<div class="col-sm-6 col-xs-12 facebook">
				<p>
				  <i class="fa fa-facebook"></i>
					Registro con Facebook
				</p>
			</div>
			<!--=====================================
			REGISTRO GOOGLE
			======================================-->
			<a href="<?php echo $rutaGoogle; ?>">
				<div class="col-sm-6 col-xs-12 google">
					<p>
					  <i class="fa fa-google"></i>
						Registro con Google
					</p>
				</div>
			</a>
			<!--=====================================
			REGISTRO DIRECTO
			======================================-->
			<form method="post" id="frmRegistro" onsubmit="return registroUsuario()">
			<hr>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="glyphicon glyphicon-user"></i>
						</span>
						<input type="text" class="form-control text-uppercase" id="regUsuario" name="regUsuario" placeholder="Nombre Completo" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="glyphicon glyphicon-envelope"></i>
						</span>
						<input type="email" class="form-control" id="regEmail" name="regEmail" placeholder="Correo Electrónico" required>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="glyphicon glyphicon-lock"></i>
						</span>
						<input type="password" class="form-control" id="regPassword" name="regPassword" placeholder="Contraseña" required>
					</div>
				</div>
				<!--=====================================
				https://www.iubenda.com/ CONDICIONES DE USO Y POLÍTICAS DE PRIVACIDAD
				======================================-->
				<div class="checkBox">
					<label>
						<input id="regPoliticas" type="checkbox">
							<small>
								Al registrarse, usted acepta nuestras condiciones de uso y políticas de privacidad
								<br>
								<!--<a href="//www.iubenda.com/privacy-policy/8146355" class="iubenda-white iubenda-embed" title="condiciones de uso y políticas de privacidad">Leer más</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>-->
								
								<a href="#modalPrivacidad" data-dismiss="modal" data-toggle="modal">Aviso integral de privacidad</a>

							</small>
					</label>
				</div>
				<?php
					$registro = new ControladorUsuarios();
					$registro -> ctrRegistroUsuario();
				?>
				<input type="submit" class="btn btn-default backColor btn-block"  value="ENVIAR">	
			</form>
        </div>
        <div class="modal-footer">
			¿Ya tienes una cuenta registrada? | <strong><a href="#modalIngreso" data-dismiss="modal" data-toggle="modal">Ingresar</a></strong>
        </div>
    </div>
</div>


<!--=====================================
VENTANA MODAL PARA EL REGISTRO CON CUPON DE DESCUENTO
======================================-->


<div class="modal fade modalFormulario" id="modalRegistroConDescuento" role="dialog">

	<div class="modal-dialog modal-lg" style="max-width:550px;min-width:400px">
		<div class="modal-content" >
			<div id="carousel-example-generic" class="carousel slide" data-ride="">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="page1 item active">
					
				<div class="modal-body modalTitulo">
						<h3 class="" style="font-size: larger;font-weight: bold;background:bottom">!BIENVENIDO A DUMBBELLS!</h3>
					    <button type="button" class="close" style="font-size: xx-large;color:black;margin-top: -9px;opacity: 1;" data-dismiss="modal">&times;</button>
					
						<form id="frmBienvenida" method="post" id="frmBienvenida" style="padding:0px;margin-top:-10px"> 



							<table role="presentation" style="background:white" width="100%" cellspacing="0" cellpadding="0" border="0"> 
								<tbody>
									<tr> 
										<td> 
											<table role="presentation" id="m_-9034317916659480312bodyTable" style="margin-left:auto;margin-right:auto;min-width:400px"  cellspacing="0" cellpadding="0" border="0" align="center"> 
											<tbody>
											
												<tr> 
												<td>
													<table role="presentation" id="m_-9034317916659480312container" style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
														<tbody>
									
														<tr id="m_-9034317916659480312headlineOnlyb629d158-d4a9-48b3-b09d-d99e0526e1e2"> 
															<td> 
																<table role="presentation" style="min-width:100%" width="100%" cellspacing="0" cellpadding="0" border="0"> 
																	<tbody> 
																		<tr style="white-space:nowrap;background-color:black"> 
																		<td class="m_-9034317916659480312stack" style="font-weight:bold;color:white;text-align:left;font-family:Arial,Helvetica,
																													sans-serf;font-size:22px;line-height:1.1;white-space:normal;background-color:black;
																													padding-top:15px;padding-right:25px;padding-bottom:15px;padding-left:25px"> 
																			<div id="m_-9034317916659480312headlineText04b629d158-d4a9-48b3-b09d-d99e0526e1e2">
																				<span>Encuentra los mejores productos deportivos</span> 
																			</div> 
																		</td> 
																		</tr> 
																	</tbody> 
																</table> 
															</td> 
														</tr>
														
														<tr id="m_-9034317916659480312textOnlyb880a1b3-3e17-4da4-8fce-154d0305787b"> 
														<td> 
															<table role="presentation" style="min-width:100%" width="100%" cellspacing="0" cellpadding="0" border="0"> 
																<tbody> 
																	<tr style="white-space:nowrap;background-color:#ffffff"> 
																		<td class="m_-9034317916659480312stack" style="color:#8f9daa;text-align:left;font-family:Arial,Helvetica,sans-serf;font-size:16px;line-height:24px;white-space:normal;background-color:black;padding-top:15px;padding-right:20px;padding-bottom:15px;padding-left:20px"> 
																			<div id="m_-9034317916659480312plainText05b880a1b3-3e17-4da4-8fce-154d0305787b" 
																				style="background-image: url('https://dumbbells.mx/backoffice/vistas/img/plantilla/cupon1.png');height: 180px;border-radius: 10px;width: 100%;background-repeat: no-repeat;background-size: cover;">
																				<div id="m_-9034317916659480312headlineText046ff6570c-842c-488d-8172-a96d08cbb90a" 
																								style="padding-left: 13px;font-size: x-large;color: white;margin-top:-10px;position: absolute;width: 92%;"> 
																								<div style="height:25px"></div>
																								<span style="font-size:20px">Ofrecemos una amplia oferta y experiencia de compra</span>
																								<div style="height:25px"></div> 
																								<span style="font-size:20px">Las siguientes secciones te ayudaran a encotrar tu producto:</span>
																				
																				</div> 
																			</div> 
																		</td> 
																	</tr> 
																</tbody> 
															</table> 
														</td> 
														</tr>
														
														<tr id="m_-9034317916659480312newFooter"> 
														<td> 
															<table role="presentation" style="min-width:100%" width="100%" cellspacing="0" cellpadding="0" border="0"> 
																<tbody> 
																<tr style="white-space:nowrap;background-color:white"> 
																<td class="m_-9034317916659480312stack" style="font-size: small;color:white;white-space:normal;background-color:black;padding-top:5px;padding-right:25px;padding-bottom:15px;padding-left:25px"> 
																	<div id="m_-9034317916659480312footerContent20"> 
																	    <table role="presentation" style="display:inline-block;width:100%;text-align: center;" cellspacing="0" cellpadding="0" border="0"> 
																			<tbody> 
																				<tr style="height:45px"> 
																					<td valign=top width="33%">Los más vendidos</td> 
																					<td valign=top  width="*">Los más vistos</td> 
																					<td valign=top  width="33%">Buscador de productos</td> 
																				</tr> 
																				<tr style="height:50px"> 
																					<td valign=top  width="33%">Registro por red social y correo</td> 
																					<td valign=top  width="*">Carrito de compras</td> 
																					<td valign=top  width="33%">Perfil y ofertas especiales</td> 
																				</tr> 
																				<tr style="height:30px"> 
																					<td valign=top  width="33%">Direcciones de entrega</td> 
																					<td valign=top  width="*">Contactanos por email o whatsapp</td> 
																					<td valign=top  width="33%">Lista de deseos y compras</td> 
																				</tr> 
																			</tbody> 
																		</table> 
																	</div> 
															    </td> 
																</tr> 
																</tbody> 
															</table> 
														</td> 
														</tr>

														</tbody>
													</table> 
												</td> 
												</tr>  
												<tr> 
												<td> 
													<table role="presentation" id="m_-9034317916659480312gmailModule" style="min-width:100%" width="100%" cellspacing="0" cellpadding="0" border="0"> 
														<tbody>
														<tr style="white-space:nowrap;background-color:#ffffff"> 
														<td style="min-width:400px;opacity:0;font-size:0px;line-height:0px" height="1"> <img src="#m_-9034317916659480312_" style="min-width:400px;max-height:0px;text-decoration:none;border:none" class="CToWUd" height="1"> </td> 
														</tr> 
														</tbody>
													</table> 
												</td> 
												</tr> 
											</tbody>
										</table> 
										</td> 
									</tr> 
								</tbody>
							</table> 
						</form>
					</div>
					<div class="modal-footer">
				
						<table style="width:100%">
							<tr>
								<td style="text-align: left;">
								<button class="btnPages btn btn-default" 
										style="border-style:dashed;border-width: 2px;border-color:black;font-weight: bold;height: 34px;">!REGISTRATE, ES GRATIS!</button> 
								</td>
								<td>
								
								</td>
								<td>
								    <strong >¿Ya tienes una cuenta?</strong>
								</td>
							</tr>
							<tr>
								<td>
									
								</td>
								<td>

								</td>
								<td>
								<a href="#modalIngreso" class="btn btn-success" data-dismiss="modal" data-toggle="modal">Ingresar</a>
								</td>
							</tr>
						</table>

					</div>

				</div>

				<div class="page2 item ">
					<div class="modal-body modalTitulo">
						<h3 class="" style="background:#d0c356;font-size: larger;font-weight: bold;">!REGISTRATE Y OBTÉN TU DESCUENTO!</h3>
						<h4 style="margin-left: 16px;margin-right: 6px;">Es muy sencillo, selecciona tu red social o utiliza tu correo electrónico...</h4>
					    <button type="button" class="close" style="font-size: xx-large;color:black;margin-top: -9px;opacity: 1;" data-dismiss="modal">&times;</button>
						<!--=====================================
						REGISTRO FACEBOOK
						======================================-->
						<div class="col-sm-6 col-xs-12 facebookCupon">
							<p>
							<i class="fa fa-facebook"></i>
								Registro con Facebook
							</p>
						</div>
						<!--=====================================
						REGISTRO GOOGLE
						======================================-->
						<a href="<?php echo $rutaGoogle; ?>">
							<div class="col-sm-6 col-xs-12 googleCupon">
								<p>
								<i class="fa fa-google"></i>
									Registro con Google
								</p>
							</div>
						</a>
						<!--=====================================
						REGISTRO DIRECTO
						======================================-->
						<form id="frmRegistroDirecto" method="post" id="frmRegistroCupon" >    <!--onsubmit="return registroUsuarioCupon()"  -->
							<div id="loader" style="display: none;position: fixed;top: 0;left: 0;right: 0;bottom: 0;width: 100%;
													background: rgba(0,0,0,0.75) url(images/loading2.gif) no-repeat center center;z-index: 10000;"></div>
							<hr>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-user"></i>
										</span>
										<input type="text" class="form-control text-uppercase" id="regUsuarioCupon" name="regUsuarioCupon" placeholder="Nombre Completo" required>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-envelope"></i>
										</span>
										<input type="email" class="form-control" id="regEmailCupon" name="regEmailCupon" placeholder="Correo Electrónico" required>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-lock"></i>
										</span>
										<input type="password" class="form-control" id="regPasswordCupon" name="regPasswordCupon" placeholder="Contraseña" required>
									</div>
								</div>
								<!--=====================================
								https://www.iubenda.com/ CONDICIONES DE USO Y POLÍTICAS DE PRIVACIDAD
								======================================-->
								<div class="checkBox">
									<label>
										<input id="regPoliticasCupon" type="checkbox">
											<small>
												Al registrarse, usted acepta nuestras condiciones de uso y políticas de privacidad
												<br>
												<a href="#modalPrivacidad" data-dismiss="modal" data-toggle="modal">Aviso integral de privacidad</a>
												<!--<a href="//www.iubenda.com/privacy-policy/8146355" class="iubenda-white iubenda-embed" title="condiciones de uso y políticas de privacidad">Leer más</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>-->
											</small>
									</label>
								</div>
									<button class="btn btn-default btn-block" id="btnRegistroCuponx" name="btnRegistroCuponx" 
										style="background:#d0c356;border-style:dashed;border-width: 2px;border-color:black;font-weight: bold;">!QUIERO MI CUPÓN!</button> 
						</form>
					</div>
					<div class="modal-footer">
						<strong>¿Ya tienes una cuenta registrada?</strong> &nbsp;&nbsp; <strong>
							<a href="#modalIngreso" class="btn btn-success" data-dismiss="modal" data-toggle="modal">Ingresar</a></strong>&nbsp;&nbsp;
					</div>

				</div>

				
			</div>

		

			</div>
		</div>


	</div>
</div>

<!--=====================================
VENTANA MODAL PARA EL INGRESO
======================================-->

<div class="modal fade modalFormulario" id="modalIngreso" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

        	<h3 class="backColor">INGRESAR</h3>

           <button type="button" class="close" data-dismiss="modal">&times;</button>
        	
			<!--=====================================
			INGRESO FACEBOOK
			======================================-->

			<div class="col-sm-6 col-xs-12 facebookIngreso">
				
				<p>
				  <i class="fa fa-facebook"></i>
					Ingreso con Facebook
				</p>

			</div>

			<!--=====================================
			INGRESO GOOGLE
			======================================-->
			<a href="<?php echo $rutaGoogle; ?>">
			
				<div class="col-sm-6 col-xs-12 googleIngreso">
					
					<p>
					  <i class="fa fa-google"></i>
						Ingreso con Google
					</p>

				</div>

			</a>

			<!--=====================================
			INGRESO DIRECTO
			======================================-->

			<form method="post">
				
			<hr>

				<div class="form-group">
					
					<div class="input-group">
						
						<span class="input-group-addon">
							
							<i class="glyphicon glyphicon-envelope"></i>
						
						</span>

						<input type="email" class="form-control" id="ingEmail" name="ingEmail" placeholder="Correo Electrónico" required>

					</div>

				</div>

				<div class="form-group">
					
					<div class="input-group">
						
						<span class="input-group-addon">
							
							<i class="glyphicon glyphicon-lock"></i>
						
						</span>

						<input type="password" class="form-control" id="ingPassword" name="ingPassword" placeholder="Contraseña" required>

					</div>

				</div>

				

				<?php

					$ingreso = new ControladorUsuarios();
					$ingreso -> ctrIngresoUsuario();

				?>
				
				<input type="submit" class="btn btn-default backColor btn-block btnIngreso" value="ENTRAR">	

				<br>

				<center>
					
					<a href="#modalPassword" data-dismiss="modal" data-toggle="modal">¿Olvidaste tu contraseña?</a>

				</center>

			</form>

        </div>

        <div class="modal-footer">
          
			¿No tienes una cuenta registrada? | <strong><a href="#modalRegistro" data-dismiss="modal" data-toggle="modal">Registrarse</a></strong>

        </div>
      
    </div>

</div>


<!--=====================================
VENTANA MODAL PARA OLVIDO DE CONTRASEÑA
======================================-->

<div class="modal fade modalFormulario" id="modalPassword" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

        	<h3 class="backColor">SOLICITUD DE NUEVA CONTRASEÑA</h3>

           <button type="button"  class="close" data-dismiss="modal">&times;</button>
        	
			<!--=====================================
			OLVIDO CONTRASEÑA
			======================================-->

			<form  id="frmNuevoPwd" method="post">

				<label class="text-muted">Escribe el correo electrónico con el que estás registrado y allí te enviaremos una nueva contraseña:</label>

				<div class="form-group">
					
					<div class="input-group">
						
						<span class="input-group-addon">
							
							<i class="glyphicon glyphicon-envelope"></i>
						
						</span>
					
						<input type="email" class="form-control" id="passEmail" name="passEmail" placeholder="Correo Electrónico" required>

					</div>

				</div>			

				<?php

					$password = new ControladorUsuarios();
					$password -> ctrOlvidoPassword();
					
				?>
				
				<input type="submit" id="btnEnviaSolNuevoPwd" class="btn btn-default backColor btn-block" value="ENVIAR">	

			</form>

        </div>

        <div class="modal-footer">
          
			¿No tienes una cuenta registrada? | <strong><a href="#modalRegistro" data-dismiss="modal" data-toggle="modal">Registrarse</a></strong>

        </div>
      
    </div>

</div>


<!--=====================================
VENTANA MODAL PRIVACIDAD
======================================-->

<div class="modal fade modalFormulario" id="modalPrivacidad" role="dialog">

    <div class="modal-content modal-dialog">

        <div class="modal-body modalTitulo">

        	<h3 class="backColor">AVISO DE PRIVACIDAD INTEGRAL</h3>

           <button type="button"  class="close" data-dismiss="modal">&times;</button>
        	
		   <iframe src="https://dumbbells.mx/aviso-privacidad.html" style="width:100%;height:600px;border-style: hidden;"></iframe>

		

        </div>

       
      
    </div>

</div>




