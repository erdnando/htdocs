<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="UTF-8">
	<!--<meta http-equiv="Content-Security-Policy" content="default-src https:">-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<?php

	// **PREVENTING SESSION HIJACKING**
	// Prevents javascript XSS attacks aimed to steal the session ID
	ini_set('session.cookie_httponly', 1);

	// **PREVENTING SESSION FIXATION**
	// Session ID cannot be passed through URLs
	ini_set('session.use_only_cookies', 1);

	// Uses a secure connection (HTTPS) if possible
	ini_set('session.cookie_secure', 1);

		session_start();

		$servidor = Ruta::ctrRutaServidor();

		$plantilla = ControladorPlantilla::ctrEstiloPlantilla();

		echo '<link rel="icon" href="'.$servidor.$plantilla["icono"].'">';

		/*=============================================
		MANTENER LA RUTA FIJA DEL PROYECTO
		=============================================*/
		
		$url = Ruta::ctrRuta();

		/*=============================================
		MARCADO DE CABECERA
		=============================================*/

		$rutas = array();

		if(isset($_GET["ruta"])){

			$rutas = explode("/", $_GET["ruta"]);

			$ruta = $rutas[0];

		}else{

			$ruta = "inicio";

		}

		$cabeceras = ControladorPlantilla::ctrTraerOpenGraph($ruta);
        try{
			if(!$cabeceras["ruta"]){
				$ruta = "inicio";
				$cabeceras = ControladorPlantilla::ctrTraerOpenGraph($ruta);
			}
		}catch(Exception $e){
			echo $e;
           //console.log($e);
		}
	?>

	<!--=====================================
	Marcado HTML5
	======================================-->

	<meta name="title" content="<?php echo  $cabeceras['titulo']; ?>">
	<meta name="description" content="<?php echo  $cabeceras['descripcion']; ?>">
	<meta name="keyword" content="<?php echo  $cabeceras['palabrasClaves']; ?>">

	<title><?php echo  $cabeceras['titulo']; ?></title>

	<!--=====================================
	Marcado de Open Graph FACEBOOK
	======================================-->

	<meta property="og:title"   content="<?php echo $cabeceras['titulo'];?>">
	<meta property="og:url" content="<?php echo $url.$cabeceras['ruta'];?>">
	<meta property="og:description" content="<?php echo $cabeceras['descripcion'];?>">
	<meta property="og:image"  content="<?php echo $url.'backoffice/'.$cabeceras['url'];?>">
	<meta property="og:type"  content="website">	
	<meta property="og:site_name" content="Dumbbells MX">
	<meta property="og:locale" content="es_MX">

	<!--=====================================
	Marcado para DATOS ESTRUCTURADOS GOOGLE
	======================================-->
	
	<meta itemprop="name" content="<?php echo $cabeceras['titulo'];?>">
	<meta itemprop="url" content="<?php echo $url.$cabeceras['ruta'];?>">
	<meta itemprop="description" content="<?php echo $cabeceras['descripcion'];?>">
	<meta itemprop="image" content="<?php echo $url.'backoffice/'.$cabeceras['url'];?>">

	<!--=====================================
	Marcado de TWITTER
	======================================-->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:title" content="<?php echo $cabeceras['titulo'];?>">
	<meta name="twitter:url" content="<?php echo $url.$cabeceras['ruta'];?>">
	<meta name="twitter:description" content="<?php echo $cabeceras['descripcion'];?>">
	<meta name="twitter:image" content="<?php echo $url.'backoffice/'.$cabeceras['url'];?>">
	<meta name="twitter:site" content="@dumbbells">


	<!--=====================================
	PLUGINS DE CSS
	======================================-->

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plugins/bootstrap.min.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plugins/font-awesome.min.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plugins/flexslider.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plugins/sweetalert.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plugins/dscountdown.css">

	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
	<!--<script src="https://fonts.googleapis.com/css?family=Ubuntu" integrity="sha384-H9GOu9/rYDmHRow+J6ScGOLhWVZvaphPw1P21SalIBl9ZwCQuZ98QIu3F50vO6Sa" crossorigin="anonymous"></script>
	-->
	<link href="https://fonts.googleapis.com/css?family=Ubuntu|Ubuntu+Condensed" rel="stylesheet">
	<!--<script src="https://fonts.googleapis.com/css?family=Ubuntu%7CUbuntu+Condensed" integrity="sha384-6D01b80k/Ei9xOxLB9HRq57z7KZ3HPnMezSDaaWG6qY9LdaOBl5cWpVd5lOPkBDJ" crossorigin="anonymous"></script>
	-->

	<!--=====================================
	HOJAS DE ESTILO PERSONALIZADAS
	======================================-->

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/plantilla.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/cabezote.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/slide.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/productos.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/infoproducto.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/perfil.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/carrito-de-compras.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/ofertas.css">

	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/footer.css?v=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="<?php echo $url; ?>vistas/css/bootstrap-select.min.css">


	<!--=====================================
	PLUGINS DE JAVASCRIPT
	======================================-->

	<script src="<?php echo $url; ?>vistas/js/plugins/jquery.min.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/bootstrap.min.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/jquery.easing.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/jquery.scrollUp.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/jquery.flexslider.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/sweetalert.min.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/md5-min.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/dscountdown.min.js"></script>

	<script src="<?php echo $url; ?>vistas/js/plugins/knob.jquery.js"></script>

	<script src="https://apis.google.com/js/platform.js" async defer></script>
<!--	<script src="https://apis.google.com/js/platform.js" integrity="sha384-ChOJeuhKaYKnHLQ+dIWPIOzzhrNdPDuyY39W+Bt7aFekDxdpbM8WGYGunuQtWJpx" crossorigin="anonymous"></script>
-->

	<script src="<?php echo $url; ?>vistas/js/plugins/bootstrap-select.min.js"></script>

	<!--<script src="http://malsup.github.io/jquery.blockUI.js"></script>-->
	<script src="<?php echo $url; ?>vistas/js/plugins/blockui/jquery.blockUI.js"></script>

<!-- Latest compiled and minified JavaScript -->

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<!--<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>-->


<!--=====================================
	Konecta payments
	======================================-->
   <!-- <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>-->

	<!--=====================================
	Pixel de Facebook
	======================================-->

	<?php echo $plantilla["pixelFacebook"]; ?>



</head>

<body>

<?php

/*=============================================
CABEZOTE
=============================================*/
$rutasx = array();


	$rutasx = explode("/", $_GET["ruta"]);

	if($rutasx[0] == "api"){

			
		if($rutasx[1]."/".$rutasx[2] == "webhook/listenermx86676660000012"){
			include "modulos/api/webhook/listenermx86676660000012.php";
			return;
		}
		else if($rutasx[1]."/".$rutasx[2] == "webhook/listenermx86676660000013"){
			include "modulos/api/webhook/listenermx86676660000013.php";
			return;
		}

	}else{
		include "modulos/cabezote.php";
	}

	


/*=============================================
CONTENIDO DINÁMICO
=============================================*/

$rutas = array();
$ruta = null;
$infoProducto = null;

if(isset($_GET["ruta"])){
	

	$rutas = explode("/", $_GET["ruta"]);

	$item = "ruta";
	$valor =  $rutas[0];

	/*=============================================
	URL'S AMIGABLES DE CATEGORÍAS
	=============================================*/

	$rutaCategorias = ControladorProductos::ctrMostrarCategorias($item, $valor);

	if($rutas[0] == $rutaCategorias["ruta"]){

		$ruta = $rutas[0];

	}

	

	/*=============================================
	URL'S AMIGABLES DE SUBCATEGORÍAS
	=============================================*/

	$rutaSubCategorias = ControladorProductos::ctrMostrarSubCategorias($item, $valor);

	foreach ($rutaSubCategorias as $key => $value) {
		
		if($rutas[0] == $value["ruta"]){

			$ruta = $rutas[0];

		}

	}

	
	/*=============================================
	URL'S AMIGABLES DE PRODUCTOS
	=============================================*/

	$rutaProductos = ControladorProductos::ctrMostrarInfoProducto($item, $valor);
	
	if($rutas[0] == $rutaProductos["ruta"]){

		$infoProducto = $rutas[0];

	}

	
	if($rutas[0] == "api"){

		
		if($rutas[1]."/".$rutas[2] == "webhook/listenermx86676660000012"){
			include "modulos/api/webhook/listenermx86676660000012.php";
			return;
		}else if($rutas[1]."/".$rutas[2] == "webhook/listenermx86676660000013"){
			include "modulos/api/webhook/listenermx86676660000013.php";
			return;
		}

	}





	

	/*=============================================
	LISTA BLANCA DE URL'S AMIGABLES
	=============================================*/
	

	if($ruta != null || $rutas[0] == "articulos-gratis" || $rutas[0] == "lo-mas-vendido" || $rutas[0] == "lo-mas-visto"){
      //entran las categorias y subcategorias
		include "modulos/productos.php";

	}else if($infoProducto != null){

		include "modulos/infoproducto.php";

	}else if($rutas[0] == "buscador" || $rutas[0] == "verificar" || $rutas[0] == "salir" || $rutas[0] == "perfil" || $rutas[0] == "carrito-de-compras" || $rutas[0] == "error" || $rutas[0] == "finalizar-compra" || $rutas[0] == "thank-you" || $rutas[0] == "cupon" || $rutas[0] == "recomiendanos" || $rutas[0] == "curso" || $rutas[0] == "ofertas" || $rutas[0] == "calificacion"){
		include "modulos/".$rutas[0].".php";

	}else if($rutas[0] == "inicio"){

		include "modulos/slide.php";
		include "modulos/destacados.php";
		

	}else if($rutas[0] == "api"){

		
		if($rutas[1]."/".$rutas[2] == "webhook/listenermx86676660000012"){
			include "modulos/api/webhook/listenermx86676660000012.php";
			return;
		}else if($rutas[1]."/".$rutas[2] == "webhook/listenermx86676660000013"){
			include "modulos/api/webhook/listenermx86676660000013.php";
			return;
		} else{
			include "modulos/error404.php";	
		}

	}else{

		if($rutas[0] == "dashboard"){
			//include "../dashboard/index.php";
			//header('Location: '.$_SERVER['HTTP_HOST'].'/dashboard/');	
			//exit();
		}else{
			include "modulos/error404.php";
		}

		
	}

}else{
  //inicio
	include "modulos/slide.php";

	//include "modulos/cupones.php";
	//include "modulos/destacados.php";
	//include "modulos/banners.php";//ok
	include "modulos/lomasvendido.php";//ok
	include "modulos/lomasvisto.php";//ok

	include "modulos/visitas.php";

	//if session es nul
	//muestra modal con info de q si se registra, obtendra un cupón
	if(!isset($_SESSION["validarSesionAux"])){

		echo '<script type="text/javascript">';
               echo '$(File).ready(function() {';
               echo '$("#modalRegistroConDescuento").modal("show");';
			   echo '$("#cookie-bar").css("visibility", "visible");';
               echo '});';
               echo '</script>';
	}

}

include "modulos/footer.php";

?>


<input type="hidden" value="<?php echo $url; ?>" id="rutaOculta">
<!--=====================================
JAVASCRIPT PERSONALIZADO
VERSIONAMIENTO DE JSs
======================================-->

<script src="<?php echo $url; ?>vistas/js/cabezote.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/plantilla.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/slide.js?version=3"></script>
<script src="<?php echo $url; ?>vistas/js/buscador.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/infoproducto.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/usuarios.js?version=2"></script>
<script src="<?php echo $url; ?>vistas/js/registroFacebook.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/registroFacebookCupon.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/loginFacebook.js?version=1"></script>
<script src="<?php echo $url; ?>vistas/js/carrito-de-compras.js?version=6"></script>
<script src="<?php echo $url; ?>vistas/js/visitas.js?version=1"></script>

<!--=====================================
https://developers.facebook.com/
======================================-->

<?php echo $plantilla["apiFacebook"]; ?>

<script>

  /*=============================================
	COMPARTIR EN FACEBOOK
	https://developers.facebook.com/docs/      
	=============================================*/
	$(".btnFacebook").click(function(){
		FB.ui({
			method: 'share',
			display: 'popup',
			href: '<?php  echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];  ?>',
		}, function(response){});
	})

	/*=============================================
	COMPARTIR EN GOOGLE
	https://developers.google.com/+/web/share/     
	=============================================*/
	$(".btnGoogle").click(function(){
		window.open(
			'https://plus.google.com/share?url=<?php  echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];  ?>',
			'',
			'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=400'
		);
		return false;
	})
</script>

	<!--=====================================
	GOOGLE ANALYTICS
	======================================-->
	<?php echo $plantilla["googleAnalytics"]; ?>


	<!--<div class="ajax-loader">
	   <img src="backoffice/vistas/img/plantilla/status.gif" class="img-responsive" />
	</div>-->





	<!-- GetButton.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+523332422256", // WhatsApp number
            call_to_action: "Aqui estamos", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->

</body>
</html>