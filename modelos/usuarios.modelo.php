<?php

require_once "conexion.php";

class ModeloUsuarios{

	
	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlInsertaRecomendado($tabla, $datos){


		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(correo, recomendadopor, fecha, aplicado) VALUES (:correo, :recomendadopor, sysdate(), 0)");

		$stmt->bindParam(":correo", $datos["emailReferenciado"], PDO::PARAM_STR);
		$stmt->bindParam(":recomendadopor", $datos["email"], PDO::PARAM_STR);
		
		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
		$stmt = null;
	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlRegistroUsuario($tabla, $datos){

		//$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, password, email, foto, modo, verificacion, emailEncriptado,cuponregistro) VALUES (:nombre, :password, :email, :foto, :modo, :verificacion, :emailEncriptado, :cuponregistro)");

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, password, email, foto, modo, verificacion, emailEncriptado,cuponregistro) VALUES (:nombre, :password, :email, :foto, :modo, :verificacion, :emailEncriptado, :cuponregistro)");


		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
		$stmt->bindParam(":modo", $datos["modo"], PDO::PARAM_STR);
		$stmt->bindParam(":verificacion", $datos["verificacion"], PDO::PARAM_INT);
		$stmt->bindParam(":emailEncriptado", $datos["emailEncriptado"], PDO::PARAM_STR);
		$stmt->bindParam(":cuponregistro", $datos["cupon"], PDO::PARAM_STR);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
		$stmt = null;
	}

	/*=============================================
	MOSTRAR USUARIO
	=============================================*/

	static public function mdlMostrarUsuario($tabla, $item, $valor){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item AND estatus = 1");
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();
		$stmt-> close();
		$stmt = null;
	}

	/*=============================================
	MOSTRAR USUARIO HISTORICO
	=============================================*/

	static public function mdlMostrarUsuariohistorico($tabla, $item, $valor){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();
		$stmt-> close();
		$stmt = null;
	}

	/*=============================================
	OBTENER CARRITO POR EMAIL
	=============================================*/

	static public function mdlObtenerCarritoUsuario($tabla, $item, $valor){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();
		$stmt-> close();
		$stmt = null;
	}

	/*=============================================
	OBTENER DIRECCION
	=============================================*/

	static public function mdlObtenerDireccion($tabla, $item, $valor){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();
		$stmt-> close();
		$stmt = null;
	}
	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/
	static public function mdlActualizarUsuario($tabla, $id, $item, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item = :$item WHERE id = :id");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}
	/*=============================================
	ACTUALIZAR PERFIL
	=============================================*/
	static public function mdlActualizarPerfil($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, email = :email, password = :password, foto = :foto WHERE id = :id");

		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR COMPRAS
	=============================================*/

	static public function mdlMostrarCompras($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt-> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR DIRECCIONES
	=============================================*/
	static public function mdlMostrarDirecciones($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt-> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR FORMAS DE PAGO
	=============================================*/
	static public function mdlMostrarformasPago($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt-> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR TRANSACCION
	=============================================*/

	static public function mdlObtenerTransaccion($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();
		$stmt-> close();
		$stmt = null;
	}




	/*=============================================
	MOSTRAR COMENTARIOS EN PERFIL
	=============================================*/

	static public function mdlMostrarComentariosPerfil($tabla, $datos){

		if($datos["idUsuario"] != ""){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto");

			$stmt -> bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
			$stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_producto = :id_producto ORDER BY Rand()");

			$stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt-> close();

		$stmt = null;

	}



		/*=============================================
	MOSTRAR LOS ULTIMOS 5 COMENTARIOS EN HOME
	=============================================*/

	static public function mdlMostrar5ComentariosPerfil($tabla){

	

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla  where comentario != '' ORDER BY FECHA desc LIMIT 4");
			

			$stmt -> execute();

			return $stmt -> fetchAll();

		

		$stmt-> close();

		$stmt = null;

	}




	
	/*=============================================
	ACTUALIZAR COMENTARIO
	=============================================*/

	static public function mdlGetIdComentario($idVenta){

		$stmt = Conexion::conectar()->prepare("SELECT comentarios.id FROM compras, comentarios where comentarios.id_usuario=compras.id_usuario and comentarios.id_producto=compras.id_producto and compras.id=:idVenta LIMIT 1");
		$stmt->bindParam(":idVenta", $idVenta, PDO::PARAM_INT);
		

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR COMENTARIO
	=============================================*/

	static public function mdlActualizarComentario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET calificacion = :calificacion, comentario = :comentario WHERE id = :id");

		$stmt->bindParam(":calificacion", $datos["calificacion"], PDO::PARAM_STR);
		$stmt->bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR DOMICILIO
	=============================================*/

	static public function mdlActualizarDomicilio($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET calle = :calle, cp = :cp, `municipio-colonia` = :municipiocolonia, estado = :estado, pais = :pais, nota = :nota WHERE id = :id");

		$stmt->bindParam(":calle", $datos["calle"], PDO::PARAM_STR);
		$stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		$stmt->bindParam(":municipiocolonia", $datos["municipiocolonia"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
		$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":nota", $datos["nota"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	INSERTA DOMICILIO
	=============================================*/

	static public function mdlInsertaDomicilio($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (calle,cp,`municipio-colonia`,estado,pais,id_usuario,nota) VALUES( :calle, :cp, :municipiocolonia, :estado, :pais, :id_usuario,:nota )");

		$stmt->bindParam(":calle", $datos["calle"], PDO::PARAM_STR);
		$stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		$stmt->bindParam(":municipiocolonia", $datos["municipiocolonia"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
		$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":id_usuario", $datos["idusuario"], PDO::PARAM_INT);
		$stmt->bindParam(":nota", $datos["nota"], PDO::PARAM_STR);
		

		if($stmt -> execute()){
			return "ok";
		}else{
			return "error";
		}
		$stmt-> close();
		$stmt = null;
	}

	/*=============================================
	ACTUALIZAR FORMA PAGO
	=============================================*/

	static public function mdlActualizarFormaPago($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET formapago = :formapago, nombre = :nombre, cuenta = :cuenta, fechacaducidad = :fechacaducidad, tipo = :tipo WHERE id = :id");

		$stmt->bindParam(":formapago", $datos["formapago"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":cuenta", $datos["cuenta"], PDO::PARAM_STR);
		$stmt->bindParam(":fechacaducidad", $datos["fechacaducidad"], PDO::PARAM_STR);
		$stmt->bindParam(":tipo", $datos["tipo"], PDO::PARAM_STR);
		//$stmt->bindParam(":idusuario", $datos["idusuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	INSERTA FORMA DE PAGO
	=============================================*/

	static public function mdlInsertaFormaPago($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (formapago,nombre,cuenta,fechacaducidad,tipo,id_usuario,estatus) VALUES( :formapago, :nombre, :cuenta, :fechacaducidad, :tipo, :id_usuario,:estatus )");

		$stmt->bindParam(":formapago", $datos["formapago"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":cuenta", $datos["cuenta"], PDO::PARAM_STR);
		$stmt->bindParam(":fechacaducidad", $datos["fechacaducidad"], PDO::PARAM_STR);
		$stmt->bindParam(":tipo", $datos["tipo"], PDO::PARAM_STR);
		$stmt->bindParam(":id_usuario", $datos["idusuario"], PDO::PARAM_INT);
		$stmt->bindParam(":estatus", $datos["estatus"], PDO::PARAM_INT);
		

		if($stmt -> execute()){
			return "ok";
		}else{
			return "error";
		}
		$stmt-> close();
		$stmt = null;
	}
	/*=============================================
	AGREGAR A LISTA DE DESEOS
	=============================================*/

	static public function mdlAgregarDeseo($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario, id_producto) VALUES (:id_usuario, :id_producto)");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);	

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR LISTA DE DESEOS
	=============================================*/

	static public function mdlMostrarDeseos($tabla, $item){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario ORDER BY id DESC");

		$stmt -> bindParam(":id_usuario", $item, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/

	static public function mdlQuitarDeseo($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	
	
	/*=============================================
	QUITAR DOMICILIO DE LISTA DE DOMICILIOS
	=============================================*/

	static public function mdlQuitarDomicilio($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	ELIMINAR USUARIO
	=============================================*/

	static public function mdlEliminarUsuario($tabla, $id){

		//$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");
		//No hay borrado fisico, solo logico y el estatus lo define
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET estatus = 0 WHERE id = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}


	/*=============================================
	ELIMINAR COMENTARIOS DE USUARIO
	=============================================*/

	static public function mdlEliminarComentarios($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

		$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}


	/*=============================================
	ELIMINAR COMPRAS DE USUARIO
	=============================================*/

	static public function mdlEliminarCompras($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

		$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}


	/*=============================================
	ELIMINAR LISTA DE DESEOS DE USUARIO
	=============================================*/

	static public function mdlEliminarListaDeseos($tabla, $id){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id_usuario");

		$stmt -> bindParam(":id_usuario", $id, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	INGRESO COMENTARIOS
	=============================================*/

	static public function mdlIngresoComentarios($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario, id_producto) VALUES (:id_usuario, :id_producto)");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

		if($stmt->execute()){ 

			return "ok"; 

		}else{ 

			return "error"; 

		}

		$stmt->close();

		$tmt =null;
	}



	/*=============================================
	OBTENER CUPON REGISTRO
	=============================================*/

	static public function mdlObtenerCuponRegistro($tabla , $claseCupon){

	
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE descripcion = :descripcion");
		
		    $stmt->bindParam(":descripcion", $claseCupon, PDO::PARAM_STR);
		
		
		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt =null;

	}

//
/*=============================================
	VALIDA SI ES UN RECOMENDADO 
	=============================================*/

	static public function mdlObtenerRecomendado($tabla,$correo){

	
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE correo = :correo and aplicado = 0");
	
	    $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
	
	
		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt =null;

}


/*=============================================
	ACTUALIZA RECOMENDADO A APLICADO 
	=============================================*/

	static public function mdlActualizaRecomendado($tabla,$correo){

	
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla set aplicado = 1, fechaAplicado = sysdate() WHERE correo = :correo ");
	
	    $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
	
	
		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

    }



}