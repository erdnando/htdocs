<?php

require_once "conexion.php";

class ModeloCarrito{

	/*=============================================
	MOSTRAR TARIFAS
	=============================================*/

	static public function mdlMostrarTarifas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}




	
		/*=============================================
	ACTUALIZA STOCK
	=============================================*/

	static public function mdlActualizaStock($tabla, $datos){

		try{

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET stock = stock - :cantidad, stockDetalle=:stockDetalle, fecha = sysdate() WHERE id = :id_producto");
			
			$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
			$stmt->bindParam(":cantidad", $datos["cantidad"], PDO::PARAM_INT);
			$stmt->bindParam(":stockDetalle", $datos["stockDetalle"], PDO::PARAM_STR);
			
			
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}
			$stmt-> close();
			$stmt = null;
		}catch(Exception $e){
			return $e->getMessage();

		}
	}


	/*=============================================
	NUEVAS COMPRAS
	=============================================*/

	static public function mdlNuevasCompras($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario, transaccionId, id_producto, metodo, email, direccion, pais, cantidad, detalle, pago, envio) VALUES (:id_usuario, :transaccionId, :id_producto, :metodo, :email, :direccion, :pais, :cantidad, :detalle, :pago, :envio)");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
		$stmt->bindParam(":transaccionId", $datos["transaccionId"], PDO::PARAM_STR);
		$stmt->bindParam(":metodo", $datos["metodo"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":cantidad", $datos["cantidad"], PDO::PARAM_STR);
		$stmt->bindParam(":detalle", $datos["detalle"], PDO::PARAM_STR);
		$stmt->bindParam(":pago", $datos["pago"], PDO::PARAM_STR);
		$stmt->bindParam(":envio", $datos["envio"], PDO::PARAM_INT);
		

		if($stmt->execute()){ 

			return "ok"; 

		}else{ 

			return "error"; 

		}

		$stmt->close();

		$tmt =null;
	}


	/*=============================================
	LIMPIA CARRITO DESPUES DE HABER SIDO ACEPTADA LA COMPRA POR EL METODO DEPAGO A TRAVES DEL WEBHOOK DE RESPUESTA
	=============================================*/
	static public function mdlCleanCarrito($tabla, $id_usuario){
		try{

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET carrito_json = '', ultima_modificacion = sysdate() WHERE id_usuario = :id_usuario");
			
			$stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
			
			
			if($stmt -> execute()){
				return "ok";
			}else{
				return "error";
			}
			$stmt-> close();
			$stmt = null;
		}catch(Exception $e){
			return $e->getMessage();

		}

	}


	/*=============================================
	NUEVA TRANSACCION
	=============================================*/

	static public function mdlInsertaTransaccion($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (transaccionId, detalle, fecha, idUsuario, idDomicilio, estatus,total) VALUES (:transaccionId, :detalle, SYSDATE(4) , :idUsuario, :idDomicilio,0,:total)");

		$stmt->bindParam(":transaccionId", $datos["transaccionId"], PDO::PARAM_STR);
		$stmt->bindParam(":detalle", $datos["detalle"], PDO::PARAM_STR);
		//$stmt->bindParam(":fecha", $datos["metodo"], PDO::PARAM_STR);
		$stmt->bindParam(":idUsuario", $datos["idUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":idDomicilio", $datos["idDomicilio"], PDO::PARAM_STR);
		//$stmt->bindParam(":estatus", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);

		if($stmt->execute()){ 

			return "ok"; 

		}else{ 

			return "error"; 

		}

		$stmt->close();

		$tmt =null;
	}

	/*=============================================
	VERIFICAR PRODUCTO COMPRADO
	=============================================*/

	static public function mdlVerificarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}



	/*=============================================
	OBTENEN CARRITOS
	=============================================*/

	static public function mdlObtenerCarritosAbandonados($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla  WHERE CARRITO_JSON !='' AND NOTIFICADO = 0  AND ULTIMA_MODIFICACION < DATE(NOW()) ");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$tmt =null;

	}



	
	/*=============================================
	OBTENER CUPON INGRESADO
	=============================================*/

	static public function mdlObtenerCupon($tabla, $datos){

		if($datos !=null){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE codigo = :codigo");
		
		    $stmt->bindParam(":codigo", $datos["codigoCupon"], PDO::PARAM_STR);
		}else{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ");

		}

		
		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt =null;

	}



	/*=============================================
	OBTENER STOCK DE UN PRODUCTO
	=============================================*/

	static public function mdlObtenerStock($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("SELECT STOCK FROM $tabla WHERE id = :idProducto");
		$stmt->bindParam(":idProducto", $datos["idProducto"], PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
		$stmt =null;
	}

	/*=============================================
	PERSISTE CARRITO
	=============================================*/

	static public function mdlPersisteCarrito($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET carrito_json = :carrito_json, ultima_modificacion = sysdate() WHERE id_usuario = :id_usuario");
		
		//$datetime = date_create()->format('YYYY-mm-dd');//system date    H:i:s
		//$lastupdated = date('Y-m-d h:i:s');

		
		$stmt->bindParam(":carrito_json", $datos["carrito_json"], PDO::PARAM_STR);
		//$stmt->bindParam(":ultima_modificacion", $lastupdated, PDO::PARAM_STR);
		$stmt->bindParam(":id_usuario", $datos["usuarioCarrito"], PDO::PARAM_INT);
		
		

		if($stmt -> execute()){
			return "ok";
		}else{
			return "error";
		}
		$stmt-> close();
		$stmt = null;
	}



	
	/*=============================================
	ACTUALIZAR NOTIFICACIONES
	=============================================*/

	static public function mdlActualizaCarritoNotificado($tabla, $item, $valor, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item = :$item WHERE $item2 = :$item2 ");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_INT);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}


}