<?php

require_once "../controladores/ventas.controlador.php";
require_once "../modelos/ventas.modelo.php";

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class TablaVentas{

  /*=============================================
  MOSTRAR LA TABLA DE VENTAS
  =============================================*/
  function set_object_vars($object, array $vars) {
    $has = get_object_vars($object);
    foreach ($has as $name => $oldValue) {
        $object->$name = isset($vars[$name]) ? $vars[$name] : NULL;
    }
}

  public function mostrarTabla(){	

	  $ventas = ControladorVentas::ctrMostrarVentas();
	  
	  if($ventas == null){
		  $ventas="";
	  }

  	$datosJson = '{
		 
	 "data": [ ';

	for($i = 0; $i < count($ventas); $i++){

		/*=============================================
		TRAER PRODUCTO
		=============================================*/

		$item = "id";
		$valor = $ventas[$i]["id_producto"];

		$traerProducto = ControladorProductos::ctrMostrarProductos($item, $valor);

		if($traerProducto == null){
			$traerProducto[0]["titulo"]="";
			$traerProducto[0]["portada"]="";
			$traerProducto[0]["tipo"]="";
		}

		$producto = $traerProducto[0]["titulo"];
		if($producto == null){
			$producto="";
		}
		

		$imgProducto = "<img class='img-thumbnail' src='".$traerProducto[0]["portada"]."' width='100px'>";
    
		$tipo = $traerProducto[0]["tipo"];
		$stock = $traerProducto[0]["stock"];
		$imgurl = $traerProducto[0]["portada"];


		/*=============================================
		TRAER CLIENTE
		=============================================*/

		$item2 = "id";
		$valor2 = $ventas[$i]["id_usuario"];

		$traerCliente = ControladorUsuarios::ctrMostrarUsuarios($item2, $valor2);

		if($traerCliente==null){
			$traerCliente["nombre"]="";
			$traerCliente["foto"]="";
			$traerCliente["email"]="";
		}

		$cliente = $traerCliente["nombre"];

		/*=============================================
		TRAER FOTO CLIENTE
		=============================================*/

		if($traerCliente["foto"] != ""){

			$imgCliente = "<img class='img-circle' src='".$traerCliente["foto"]."' width='70px'>";

		}else{

			$imgCliente = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='70px'>";
		}

		/*=============================================
		TRAER EMAIL CLIENTE
		=============================================*/

		if($ventas[$i]["email"] == ""){

			$email = $traerCliente["email"];

		}else{

			$email = $ventas[$i]["email"];
		}



		/*=============================================
		TRAER PAQUETERIA Y GUIA
		=============================================*/

		if($ventas[$i]["paqueteria"] == ""){

			$paqueteria = $ventas[$i]["paqueteria"]."-".$ventas[$i]["guia"];

		}else{

			$paqueteria = $ventas[$i]["paqueteria"]."-".$ventas[$i]["guia"];
		}



		/*=============================================
		TRAER PROCESO DE ENVÍO
		=============================================*/

		if($ventas[$i]["envio"] == 0 && $tipo == "virtual"){

			$envio = "<button class='btn btn-info btn-xs'>Entrega inmediata</button>";
		
		}else if($ventas[$i]["envio"] == 0 && $tipo == "fisico"){

			$envio ="<button class='btn btn-danger btn-xs btnEnvio' imagenProducto='".$imgurl."'  idVenta='".$ventas[$i]["id"]."' producto='".$producto."' email='".$email."'  venta='$".number_format($ventas[$i]["pago"])."' direccion='".$ventas[$i]["direccion"]."' fecha='".$ventas[$i]["fecha"]."' etapa='1'>Despachando el producto</button>";

		}else if($ventas[$i]["envio"] == 1 && $tipo == "fisico"){

			$envio = "<button class='btn btn-warning btn-xs btnEnvio'  imagenProducto='".$imgurl."' idVenta='".$ventas[$i]["id"]."' producto='".$producto."' email='".$email."'  venta=$'".number_format($ventas[$i]["pago"])."' direccion='".$ventas[$i]["direccion"]."' fecha='".$ventas[$i]["fecha"]."' etapa='2'>Enviando el producto</button>";

		}else{

			$envio = "<button class='btn btn-success btn-xs'  imagenProducto='".$imgurl."' idVenta='".$ventas[$i]["id"]."' email='".$email."'  fecha='".$ventas[$i]["fecha"]."' >Producto entregado</button>";

		}

		/*=============================================
		LOGOS PAYPAL Y PAYU
		=============================================*/

		if($ventas[$i]["metodo"] == "paypal"){

			$metodo = "<img class='img-responsive' src='vistas/img/plantilla/paypal.jpg' style='width:100px!important;height:auto'>";
		
		}else if($ventas[$i]["metodo"] == "payu"){

			$metodo = "<img class='img-responsive' src='vistas/img/plantilla/payu.jpg' style='width:100px!important;height:auto'>";
		
		}else{

			//$metodo = "BILLPOCKET";
			$metodo = "<img class='img-responsive' src='vistas/img/plantilla/billpocket720.png' style='width:100px!important;height:auto'>";

		}


		/*=============================================
		DEVOLVER DATOS JSON
		=============================================*/
		$datosJson	 .= '[
			      		"'.($i+1).'",
			      		"'.$producto.'",
			      		"'.$imgProducto.'",
			      		"'.$cliente.'",
			      		"'.$imgCliente.'",
						  "$ '.number_format($ventas[$i]["pago"],2).'",
						  "'.$stock.'",
						 "'.$envio.'",
						 "'.$paqueteria.'",
			      		"'.$metodo.'",	
			      		"'.$email.'",
			      		"'.$ventas[$i]["direccion"].'",
			      		"'.$ventas[$i]["pais"].'",
			      		"'.$ventas[$i]["fecha"].'"	
			      		],';

	} 

	$datosJson = substr($datosJson, 0, -1);

	$datosJson.=  ']
		  
	}'; 
  	
  	echo $datosJson;	

  }

}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaVentas();
$activar -> mostrarTabla(); 

