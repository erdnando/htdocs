<?php
require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class TablaUsuarios{

 	/*=============================================
  	MOSTRAR LA TABLA DE USUARIOS
  	=============================================*/ 
	public function mostrarTabla(){	
		 $item = null;
		 $valor = null;
		 //$i=0;

 		$usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

 		$datosJson = '{
	 	"data": [ ';
		 foreach ((array)$usuarios as $key => $value){
	 	//for($i = 0; $i < count($usuarios); $i++){
	 		/*=============================================
			TRAER FOTO USUARIO
			=============================================*/
			if($value["foto"] != "" ){
				$foto = "<img class='img-circle' src='".$value["foto"]."' width='60px'>";
			}else{
				$foto = "<img class='img-circle' src='vistas/img/usuarios/default/anonymous.png' width='60px'>";
			}
			/*=============================================
  			REVISAR ESTADO
  			=============================================*/
  			if($value["modo"] == "directo"){
	  			if( $value["verificacion"] == 1){
	  				$colorEstado = "btn-danger";
	  				$textoEstado = "No Verificado";
	  				$estadoUsuario = 0;
	  			}else{
	  				$colorEstado = "btn-success";
	  				$textoEstado = "Verificado";
	  				$estadoUsuario = 1;
	  			}

	  			$estado = "<button class='btn btn-xs btnActivar ".$colorEstado."' idUsuario='". $value["id"]."' estadoUsuario='".$estadoUsuario."'>".$textoEstado."</button>";
	  		}else{
	  			$estado = "<button class='btn btn-xs btn-info'>Verificado</button>";
	  		}

	 		/*=============================================
			DEVOLVER DATOS JSON
			=============================================*/
			$datosJson	 .= '[
				"'.$value["id"].'",
				"'.$value["nombre"].'",
				"'.$value["email"].'",
				"'.$value["modo"].'",
				"'.$foto.'",
				"'.$estado.'",
				"'.$value["fecha"].'"  
			],';

			//$i++;
	 	}

	 	$datosJson = substr($datosJson, 0, -1);
		$datosJson.=  ']

		}'; 

		echo $datosJson;
 	}
}

/*=============================================
ACTIVAR TABLA DE VENTAS
=============================================*/ 
$activar = new TablaUsuarios();
$activar -> mostrarTabla();