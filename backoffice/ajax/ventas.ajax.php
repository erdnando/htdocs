<?php

require_once "../controladores/ventas.controlador.php";
require_once "../modelos/ventas.modelo.php";


class AjaxVentas{

	/*=============================================
	ACTUALIZAR PROCESO DE ENVÍO
	=============================================*/
	

  	public $idVenta;
  	public $etapa;

  	public function ajaxEnvioVenta(){

  		$respuesta = ModeloVentas::mdlActualizarVentaSimple("compras", "envio", $this->etapa, "id", $this->idVenta);

  		echo $respuesta;

	}

	
	public $producto;
	public $venta;
	public $direccion;
	public $fecha;
	public $email;
	public $paqueteria;
	public $guia;

  	public function ajaxEnvioVentaLogistica(){

		//ACTUALIZA ESTATUS DEL PEDIDO
		$respuesta = ModeloVentas::mdlActualizarVenta("compras", "envio", $this->etapa, "id", $this->idVenta, "paqueteria", $this->paqueteria, "guia", $this->guia);

		if($respuesta=="ok"){

			$variables = array();
			$variables['GUIA'] = $this->guia;
			$variables['PAQUETERIA'] = $this->paqueteria;
			$variables['IDVENTA'] = "V00000".$this->idVenta;

			$template = file_get_contents("plantilla-logistica-correo.html",true);

			foreach($variables as $key => $value)
			{
				$template = str_replace('{{ '.$key.' }}', $value, $template);
			}

			$datosCorreo = array("tituloFrom"=>"Dumbbells y ". $this->paqueteria,
						"subject"=> "Su Guia de rastreo: ".$this->guia,
						"address"=> $this->email,
						"msgHtml"=> $template);


		$respuestaCorreo = ControladorVentas::ctrMailgenericoCC($datosCorreo);
		}else{
			$respuestaCorreo="error";
		}

		

  		echo $respuestaCorreo;

	}


	
	public function ajaxSolicitaCalificacion(){

			$variables = array();
			$variables['VENTA'] = $this->idVenta;
			//$variables['email'] = $this->email;
			$variables['URL'] = $this->imgurl;
			$variables['PRODUCTO'] = $this->producto;
			$variables['FECHA'] = $this->fecha;
			
			
			$template = file_get_contents("plantilla-calificacion-correo.html",true);

			foreach($variables as $key => $value)
			{
				$template = str_replace('{{ '.$key.' }}', $value, $template);
			}

			$datosCorreo = array("tituloFrom"=>"Cuentanos tu experiencia con tu compra Dumbbells ",
						"subject"=> "Cuentanos tu experiencia con tu compra Dumbbells  ",
						"address"=> $this->email,
						"msgHtml"=> $template);


		$respuestaCorreo = ControladorVentas::ctrMailgenericoCC($datosCorreo);
		
  		echo $respuestaCorreo;
	}

}

/*=============================================
ACTUALIZAR PROCESO DE ENVÍO
=============================================*/
if(isset($_POST["idVenta"])){

	$envioVenta = new AjaxVentas();
	$envioVenta -> idVenta = $_POST["idVenta"];
	$envioVenta -> etapa = $_POST["etapa"];
	$envioVenta -> ajaxEnvioVenta();

}
/*=============================================
SOLICITUD DE CALIFICACION
=============================================*/
if(isset($_POST["f-idVenta"])){
	$envioVenta = new AjaxVentas();
	$envioVenta -> idVenta = $_POST["f-idVenta"];
	$envioVenta -> email = $_POST["f-email"];
	$envioVenta -> imgurl = $_POST["f-imgurl"];
	$envioVenta -> producto = $_POST["f-producto"];
	$envioVenta -> fecha = $_POST["f-fecha"];
	
	
	
	$envioVenta -> ajaxSolicitaCalificacion();
}

/*=============================================
ACTUALIZAR PROCESO DE ENVÍO Y LOGISTICA
=============================================*/
if(isset($_POST["l-idVenta"])){

	$envioVenta = new AjaxVentas();
	$envioVenta -> idVenta = $_POST["l-idVenta"];
	$envioVenta -> etapa = $_POST["l-etapa"];
	$envioVenta -> producto = $_POST["l-producto"];
	$envioVenta -> venta = $_POST["l-venta"];
	$envioVenta -> direccion = $_POST["l-direccion"];
	$envioVenta -> fecha = $_POST["l-fecha"];
	$envioVenta -> email = $_POST["l-email"];
	$envioVenta -> paqueteria = $_POST["l-paqueteria"];
	$envioVenta -> guia = $_POST["l-guia"];
	
	$envioVenta -> ajaxEnvioVentaLogistica();

}
