<?php

class ControladorVentas{

	/*=============================================
	MOSTRAR TOTAL VENTAS
	=============================================*/

	public function ctrMostrarTotalVentas(){

		$tabla = "compras";

		$respuesta = ModeloVentas::mdlMostrarTotalVentas($tabla);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/

	public function ctrMostrarVentas(){

		$tabla = "compras";

		$respuesta = ModeloVentas::mdlMostrarVentas($tabla);

		return $respuesta;

	}



	/*=============================================
	MAIL GENERICO
	=============================================*/

	static public function ctrMailgenerico($datos){
				
		date_default_timezone_set("America/Mexico_City");

		//$url = Ruta::ctrRuta();
		
		//require "../extensiones/PHPMailer/PHPMailerAutoload.php";
		require "../../extensiones/PHPMailer/PHPMailerAutoload.php";
		
		$mail = new PHPMailer();
		try{

			$mail->CharSet = 'UTF-8';
			$mail->isSMTP();   
			$mail->Host       = 'mail.dumbbells.mx'; 
			$mail->SMTPAuth   = true; 
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
			$mail->Username   = 'erdnando@dumbbells.mx'; 
			$mail->Password   = 'Adqdisp06ERV';  
			$mail->Port       = 587;  

			$mail->setFrom('ecommerce@dumbell.com', $datos["tituloFrom"]);
			$mail->Subject = $datos["subject"];
			$mail->addAddress($datos["address"]);
			$mail->msgHTML($datos["msgHtml"]);

			$envio = $mail->Send();
		
		}catch(Exception $e){
			$envio=false;
		}

		if(!$envio){
			return "error";
		}else{
			return "ok";
		}
		
}



	/*=============================================
	MAIL GENERICO CC
	=============================================*/

	static public function ctrMailgenericoCC($datos){
				
		date_default_timezone_set("America/Mexico_City");

		require "../../extensiones/PHPMailer/PHPMailerAutoload.php";
		
		$mail = new PHPMailer();
		try{

			$mail->CharSet = 'UTF-8';
			$mail->isSMTP();   
			$mail->Host       = 'mail.dumbbells.mx'; 
			$mail->SMTPAuth   = true; 
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
			$mail->Username   = 'erdnando@dumbbells.mx'; 
			$mail->Password   = 'Adqdisp06ERV';  
			$mail->Port       = 587;  
			

			$mail->setFrom('ecommerce@dumbell.com', $datos["tituloFrom"]);
			$mail->Subject = $datos["subject"];
			$mail->addAddress($datos["address"]);
			$mail->addAddress("envios@dumbbells.mx");
			$mail->msgHTML($datos["msgHtml"]);

			$envio = $mail->Send();
		
		}catch(Exception $e){
			$envio=false;
		}

		if(!$envio){
			return "error";
		}else{
			return "ok";
		}
		
}






}