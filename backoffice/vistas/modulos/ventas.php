<?php

if($_SESSION["bperfil"] != "administrador"){

echo '<script>
  window.location = "inicio";
</script>';

return;
}

?>

<div class="content-wrapper">
   <section class="content-header">
    <h1>
      Gestor ventas
    </h1>
    <ol class="breadcrumb">
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Gestor ventas</li>
      
    </ol>

  </section>


  <section class="content">
    <div class="box"> 
      <div class="box-header with-border">
        <?php
        include "inicio/grafico-ventas.php";
        ?>
      </div>
      <div class="box-body">
        <div class="box-tools">
          <a href="vistas/modulos/reportes.php?reporte=compras">
              <button class="btn btn-success">Descargar reporte en Excel</button>
          </a>
        </div>
        <br>
        <table class="table table-bordered table-striped dt-responsive tablaVentas" width="100%">
          <thead>
            <tr>
              <th style="width:10px">#</th>
              <th>Producto</th>
              <th>Imagen Producto</th>
              <th>Cliente</th>
              <th>Foto Cliente</th>
              <th>Venta</th>
              <th>Stock</th>
              <th>Proceso de envío</th>    
              <th>Paqueteria</th>         
              <th>Metodo</th>
              <th>Email</th>
              <th>Dirección</th>
              <th>País</th>
              <th>Fecha</th>
            </tr>
          </thead> 
        </table>
      </div>
    </div>
  </section>


<!--modal paqueteria-->
<div  class="modal fade modalFormulario" id="modalPaqueteria" role="dialog" data-backdrop="static" data-keyboard="false" >
	<div class="modal-content modal-dialog">
    <div class="modal-body modalTitulo" style="background: black;">
    
   
    <h3 class="backColor" style="color: white;text-align:center"> Logistica y paqueteria </h3>	
    <button  type="button" style="margin-top: -37px;color: white;font-size: 31px;" class="close" data-dismiss="modal">&times;</button>

		
		</div>
		<div class="modal-footer" style="text-align: left!important;">

    <!--<form method="post" onsubmit="return validardomicilio(this)">-->
				<div class="col-md-12 col-sm-12 col-xs-12">
					
          <div class="col-12">
                <!--=====================================
                ESPACIO PARA EL PRODUCTO
                ======================================-->
                <h2 class="text-muted text-uppercase"><div id="lproducto" name="lproducto"></div>  
                      <small>
                        <span class="label label-success" id="lemail">Nuevo</span>
                      </small>
                </h2>
                <h3 class="text-muted"><div id="lventa"></div></h3><p id="ldireccion"></p>				
                <!--=====================================
                CARACTERÍSTICAS DEL PRODUCTO
                ======================================-->
                <hr>
                <div class="form-group row">
                        <div class="col-md-5 col-xs-12">
                          <select class="form-control seleccionarDetalle" style="padding:6px" id="seleccionarPaqueteria">
                            <option value="0">Seleccionar paqueteria</option>
                            <option value="1">DHL</option>
                            <option value="2">ESTAFETA</option>
                            <option value="3">FEDEX</option>
                            <option value="4">REDPACK</option>
                          </select>
                        </div>

                        <div class="col-md-7 col-xs-12">
                        <input lass="form-control seleccionarDetalle" style="width:100%;height:33px" id="seleccionarGuia" placeholder="Ingrese la GUIA"  maxlength="30"value=""></input>
                          
                        </div>

                </div>
                <!--=====================================
                BOTONES DE COMPRA
                ======================================-->
                <div class="row botonesCompra">
                <div class="col-12"><button class="btn btn-default btn-block btn-lg backColor notificaLogistica"  style="background: rgba(11, 13, 14, 0.94) none repeat scroll 0% 0%; color: rgb(255, 255, 255);">NOTIFICAR AL CLIENTE 
                          <i class="fa fa-send"></i>
                          </button>
                    </div>
                </div>
            </div>  

            <input type="hidden" class="hdnIdVenta" name="hdnIdVenta">
            <input type="hidden" class="hdnEtapa" name="hdnEtapa">
            <input type="hidden" class="hdnProducto" name="hdnProducto">
            <input type="hidden" class="hdnVenta" name="hdnVenta">
            <input type="hidden" class="hdndireccion" name="hdndireccion">
            <input type="hidden" class="hdnFecha" name="hdnFecha">
            <input type="hidden" class="hdnEmail" name="hdnEmail">

				</div>
		<!--	</form>-->

    </div>
	</div>
</div>


</div>