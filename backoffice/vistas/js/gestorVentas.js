/*=============================================
CARGAR LA TABLA DINÁMICA DE VENTAS
=============================================*/

// $.ajax({

// 	url:"ajax/tablaVentas.ajax.php",
// 	success:function(respuesta){

// 		console.log("respuesta", respuesta);

// 	}

// })

$(".tablaVentas").DataTable({
    "ajax": "ajax/tablaVentas.ajax.php",
    "deferRender": true,
    "retrieve": true,
    "processing": true,
    "language": {

        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }

    }


});
/*=============================================
PROCESO DE ENVÍO ABRIR MODAL O FINALIZAR PROCESO DEPENDIENDO DE LA ETAPA
=============================================*/
$(".tablaVentas tbody").on("click", ".btnEnvio", function() {

    var etapa = $(this).attr("etapa");
    console.log($(this));

    if (etapa == 1) {

        //TODO call modal to show paqueteria, guia, update status and send email
        $("#modalPaqueteria").modal("show");

        var idVenta = $(this).attr("idVenta");
        //var etapa = $(this).attr("etapa");
        var producto = $(this).attr("producto");
        var venta = $(this).attr("venta");
        var direccion = $(this).attr("direccion");
        var fecha = $(this).attr("fecha");
        var email = $(this).attr("email");

        $("#lproducto").html(producto);
        $("#lventa").html('MXN ' + venta);
        $("#lemail").html(email);
        $("#ldireccion").html("Enviar a: " + direccion);

        $(".hdnIdVenta").val(idVenta);
        $(".hdnEtapa").val(etapa);
        $(".hdnProducto").val(producto);
        $(".hdnVenta").val(venta);
        $(".hdndireccion").val(direccion);
        $(".hdnFecha").val(fecha);
        $(".hdnEmail").val(email);

    } else {

        var idVenta = $(this).attr("idVenta");

        var datos = new FormData();
        datos.append("idVenta", idVenta);
        datos.append("etapa", etapa);
        console.log("etapa::::::::::::::");
        console.log(etapa);

        $.ajax({
            url: "ajax/ventas.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                console.log("respuesta", respuesta);
            }
        });

        if (etapa == 1) {
            $(this).addClass('btn-warning');
            $(this).removeClass('btn-danger');
            $(this).html('Enviando el producto');
            $(this).attr('etapa', 2);
        }

        if (etapa == 2) {
            $(this).addClass('btn-success');
            $(this).removeClass('btn-warning');
            $(this).html('Producto entregado');
            //send email to request -------------------------------

            var email = $(this).attr("email");
            var idVenta = $(this).attr("idVenta");
            var imgurl = $(this).attr("imagenproducto");
            var producto = $(this).attr("producto");
            var fecha = $(this).attr("fecha");


            var datos = new FormData();
            datos.append("f-idVenta", idVenta);
            datos.append("f-email", email);
            datos.append("f-imgurl", imgurl);
            datos.append("f-producto", producto);
            datos.append("f-fecha", fecha);

            console.log(datos);

            $.ajax({
                url: "ajax/ventas.ajax.php",
                method: "POST",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $.blockUI({
                        message: '<h1>Procesando...</h1>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            basez: 1000,
                            backgroundColor: '#fff',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#000'
                        }
                    });
                },
                success: function(respuesta) {
                    console.log("respuesta:::::", respuesta);


                    if (respuesta != "ok") {
                        swal({
                                title: "¡ERROR!",
                                text: "¡Ha ocurrido un problema al enviar correo de calificacion de la experiencia",
                                type: "error",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    history.back();
                                }
                            });
                    } else {
                        swal({
                            title: 'Solicitud de calificacion enviada!',
                            html: "¡Hemos enviado lla solicitud de calificacion al siguiente correo: <strong>" + email + "</strong>  <br><br>",
                            type: 'success',
                            confirmButtonText: 'Aceptar'
                        }).then(function() {
                            window.location = "ventas";
                        }).catch(function(reason) {
                            console.log("The alert was dismissed by the user: ");
                        });
                    }
                },
                complete: function() {
                    $.unblockUI();
                },
            });

            //-----------------------------------------------------

        }
    }

})

/*=============================================
ENVIO DE CORREO Y ACTUALIZACION DE ESTATUS
=============================================*/

$(".notificaLogistica").click(function(e) {
    $(".alert").remove();
    var paqueteria = $("#seleccionarPaqueteria option:selected").text();
    var guia = $("#seleccionarGuia").val();
    var idVenta = $(".hdnIdVenta").val();
    var etapa = $(".hdnEtapa").val();
    var producto = $(".hdnProducto").val();
    var venta = $(".hdnVenta").val();
    var direccion = $(".hdndireccion").val();
    var fecha = $(".hdnFecha").val();
    var email = $(".hdnEmail").val();

    if (guia.trim() == '') {
        $("#seleccionarPaqueteria").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> La GUIA es requerida</div>');
        return;
    }

    if (paqueteria == 0) {
        $("#seleccionarPaqueteria").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Seleccione una paqueteria</div>');
        return;

    }

    //console.log(guia);
    var datos = new FormData();
    datos.append("l-idVenta", idVenta);
    datos.append("l-etapa", etapa);
    datos.append("l-producto", producto);
    datos.append("l-venta", venta);
    datos.append("l-direccion", direccion);
    datos.append("l-fecha", fecha);
    datos.append("l-email", email);
    datos.append("l-paqueteria", paqueteria);
    datos.append("l-guia", guia);

    $.ajax({
        url: "ajax/ventas.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            $.blockUI({
                message: '<h1>Procesando...</h1>',
                css: {
                    border: 'none',
                    padding: '15px',
                    basez: 1000,
                    backgroundColor: '#fff',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#000'
                }
            });
        },
        success: function(respuesta) {
            console.log("respuesta:::::", respuesta);
            $("#modalPaqueteria").modal("toggle");
            $("#seleccionarGuia").val("");
            $("#seleccionarPaqueteria").val($("#seleccionarPaqueteria option:first").val());

            if (respuesta != "ok") {
                swal({
                        title: "¡ERROR!",
                        text: "¡Ha ocurrido un problema al enviar los datos de la Guia a " + email + ". Por favor intentarlo mas tarde!",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        closeOnConfirm: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            history.back();
                        }
                    });
            } else {
                swal({
                    title: 'Datos de la guia enviados!',
                    html: "¡Hemos enviado los datos de paquetria y Guia al siguiente correo: <strong>" + email + "</strong>  <br><br>",
                    type: 'success',
                    confirmButtonText: 'Aceptar'
                }).then(function() {
                    window.location = "ventas";
                }).catch(function(reason) {
                    console.log("The alert was dismissed by the user: ");
                });
            }
        },
        complete: function() {
            $.unblockUI();
        },
    });

    if (etapa == 1) {
        $(this).addClass('btn-warning');
        $(this).removeClass('btn-danger');
        $(this).html('Enviando el producto');
        $(this).attr('etapa', 2);
    }

    if (etapa == 2) {
        $(this).addClass('btn-success');
        $(this).removeClass('btn-warning');
        $(this).html('Producto entregado');
    }


})