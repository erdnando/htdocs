<?php

require_once "conexion.php";

class ModeloVentas{

	/*=============================================
	MOSTRAR EL TOTAL DE VENTAS
	=============================================*/	

	static public function mdlMostrarTotalVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM(pago) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/	

	static public function mdlMostrarVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	
	/*=============================================
	ACTUALIZAR ENVIO VENTA SIMPLE
	=============================================*/

	static public function mdlActualizarVentaSimple($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
		

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR ENVIO VENTA
	=============================================*/

	static public function mdlActualizarVenta($tabla, $item1, $valor1, $item2, $valor2, $item3, $valor3, $item4, $valor4){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1, $item3 = :$item3, $item4 = :$item4 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}





	


	//
	/*=============================================
	ACTUALIZAR ENVIO VENTA Y LOGISTICA
	=============================================*/

	static public function mdlActualizarVentaLogistica($datos){

		$tabla = "usuarios";
		$item = "email";

		$idVenta =   $datos["idVenta"];
		$etapa =     $datos["etapa"];
		/*$producto =  $datos["producto"];
		$venta =     $datos["venta"];
		$direccion = $datos["direccion"];
		$fecha =     $datos["fecha"];
		$email =     $datos["email"];*/

		$stmt = Conexion::conectar()->prepare("UPDATE COMPRAS SET ENVIO = :etapa WHERE ID = :idVenta");

		$stmt->bindParam(":idVenta", $idVenta, PDO::PARAM_STR);
		$stmt->bindParam(":etapa", $etapa, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}



}