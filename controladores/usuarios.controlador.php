<?php



class ControladorUsuarios{
	/*=============================================
	REGISTRO DE USUARIO CON CUPON
	=============================================*/

	static public function ctrRegistroUsuarioConcupon(){
        
		if(isset($_POST["regUsuarioCupon"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["regUsuarioCupon"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regEmailCupon"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["regPasswordCupon"]))
			   {

			   	$encriptar = crypt($_POST["regPasswordCupon"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
			   		$2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

			   	$encriptarEmail = md5($_POST["regEmailCupon"]);

				$datos = array("nombre"=>$_POST["regUsuarioCupon"],
							   "password"=> $encriptar,
							   "email"=> $_POST["regEmailCupon"],
							   "foto"=>"",
							   "modo"=> "directo",
							   "cupon"=> "si",
							   "verificacion"=> 1,
							   "emailEncriptado"=>$encriptarEmail);

				$tabla = "usuarios";

				$usuarioExistente = ControladorUsuarios::ctrMostrarUsuarioHistorico("email", $_POST["regEmailCupon"]);	

				if($usuarioExistente){
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "estatus", 1);
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "password", $encriptar);
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "nombre", $_POST["regUsuarioCupon"]);
					$respuesta="reingreso";

					//session_start();
					$_SESSION["validarSesionAux"] = "ok";
					$_SESSION["validarSesion"] = "ok";
					$_SESSION["id"] = $usuarioExistente["id"];
					$_SESSION["nombre"] = $_POST["regUsuarioCupon"];
					$_SESSION["foto"] = "";
					$_SESSION["email"] = $_POST["regEmailCupon"];
					$_SESSION["password"] = $encriptar;
					$_SESSION["modo"] = "directo";




					
				}else{	
				    $respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);
				}			

				if($respuesta == "ok"){

					/*=============================================
					ACTUALIZAR NOTIFICACIONES NUEVOS USUARIOS
					=============================================*/

					$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();
					$nuevoUsuario = $traerNotificaciones["nuevosUsuarios"] + 1;

					ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosUsuarios", $nuevoUsuario);

					/*=============================================
					VERIFICACIÓN CORREO ELECTRÓNICO
					=============================================*/


					//-----------------------------------------------------------------------------------
					$url = Ruta::ctrRuta();	
					
					$variables = array();
					$variables['URL'] = $url.'verificar/'.$encriptarEmail;
					//$variables['ENCRIPTADO'] = $encriptarEmail;
					$template = file_get_contents("plantilla-verifica-correo.html",true);
                    //.$url.'verificar/'.$encriptarEmail.'
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{ '.$key.' }}', $value, $template);
					}

					$datosCorreo = array("tituloFrom"=>"Registro ecommerce",
							   "subject"=> "Por favor verifique su dirección de correo electrónico",
							   "address"=> $_POST["regEmailCupon"],
							   "msgHtml"=> $template);

				$respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);

				if($respuestaCorreo!="ok"){
					echo '<script> 
							swal({
								  title: "¡ERROR!",
								  text: "¡Ha ocurrido un problema enviando verificación de correo electrónico a '.$_POST["regEmailCupon"].'!",
								  type:"error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										history.back();
									}
							});
						</script>';
				}else{
					$_SESSION["validarSesionAux"] = "ok";
						echo '<script> 
							swal({
								  title: "Registro exitoso!",
								  html:true,
								  text: "¡Hemos enviado un correo a la cuenta: <strong>'.$_POST["regEmailCupon"].'</strong> para verificar la cuenta! <br><br>Una vez verificada, te enviaremos el cupón de descuento a tu correo.<br><br>No olvides revisar en la sección de SPAM",
								  type:"success",
								  confirmButtonText: "Aceptar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										history.back();
									}
							});
						</script>';

				}
				//---------------------------------------------------------------------------------------------
					
					//-----------------------------------------------------------------------------------------------
				}else if($respuesta == "reingreso"){

						echo '<script> 
							swal({
								  title: "QUE BUENO VERTE DE NUEVO!",
								  html:true,
								  text: "¡Adelante, continua explorando!",
								  type:"success",
								  confirmButtonText: "Aceptar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										window.location = "inicio";
									}
							});
						</script>';

				}
			}else{
				echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡Error al registrar el usuario, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){
								if(isConfirm){
									history.back();
								}
						});
				</script>';
			}
		}
	}
    /*=============================================
	REGISTRO DE USUARIO CON CUPON AJAX
	=============================================*/

	static public function ctrRegistroUsuarioConcuponAjax($datos){
			//$valor = $datos["email"];
		if(isset($datos["nombre"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $datos["nombre"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $datos["email"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $datos["contrasenia"]))
			   {

			   	$encriptar = crypt($datos["contrasenia"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
			   		$2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

			   	$encriptarEmail = md5($datos["email"]);

				$datos = array("nombre"=>$datos["nombre"],
							   "password"=> $encriptar,
							   "email"=> $datos["email"],
							   "foto"=>"",
							   "modo"=> "directo",
							   "cupon"=> "si",
							   "verificacion"=> 1,
							   "emailEncriptado"=>$encriptarEmail);

				$tabla = "usuarios";

				$usuarioExistente = ControladorUsuarios::ctrMostrarUsuarioHistorico("email", $datos["email"]);	

				if($usuarioExistente){
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "estatus", 1);
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "password", $encriptar);
					$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "nombre", $datos["nombre"]);
					$respuesta="reingreso";

					//session_start();
					$_SESSION["validarSesionAux"] = "ok";
					$_SESSION["validarSesion"] = "ok";
					$_SESSION["id"] = $usuarioExistente["id"];
					$_SESSION["nombre"] = $datos["nombre"];
					$_SESSION["foto"] = "";
					$_SESSION["email"] = $datos["email"];
					$_SESSION["password"] = $encriptar;
					$_SESSION["modo"] = "directo";




					
				}else{	
				    $respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);
				}			

				if($respuesta == "ok"){

					/*=============================================
					ACTUALIZAR NOTIFICACIONES NUEVOS USUARIOS
					=============================================*/

					$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();
					$nuevoUsuario = $traerNotificaciones["nuevosUsuarios"] + 1;

					ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosUsuarios", $nuevoUsuario);

					/*=============================================
					VERIFICACIÓN CORREO ELECTRÓNICO
					=============================================*/


					//-----------------------------------------------------------------------------------
					$url = Ruta::ctrRuta();	
					
					$variables = array();
					$variables['URL'] = $url.'verificar/'.$encriptarEmail;
					//$variables['ENCRIPTADO'] = $encriptarEmail;
					$template = file_get_contents("plantilla-verifica-correo.html",true);
                    //.$url.'verificar/'.$encriptarEmail.'
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{ '.$key.' }}', $value, $template);
					}

					$datosCorreo = array("tituloFrom"=>"Registro ecommerce",
							   "subject"=> "Por favor verifique su dirección de correo electrónico",
							   "address"=> $datos["email"],
							   "msgHtml"=> $template);

				$respuestaCorreo = ControladorUsuarios::ctrMailgenericoAjax($datosCorreo);
				
				echo $respuestaCorreo;
				
				//---------------------------------------------------------------------------------------------
					
					//-----------------------------------------------------------------------------------------------
				}
				/*else if($respuesta == "reingreso"){

						echo '<script> 
							swal({
								  title: "QUE BUENO VERTE DE NUEVO!",
								  html:true,
								  text: "¡Adelante, continua explorando!",
								  type:"success",
								  confirmButtonText: "Aceptar",
								  closeOnConfirm: false
								},
								function(isConfirm){
									if(isConfirm){
										window.location = "inicio";
									}
							});
						</script>';

				}*/
			}
				/*else{
			
				echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡Error al registrar el usuario, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){
								if(isConfirm){
									history.back();
								}
						});
				</script>';
			}*/
		}
	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	public function ctrRegistroUsuario(){

		if(isset($_POST["regUsuario"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["regUsuario"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regEmail"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["regPassword"])){

			   	$encriptar = crypt($_POST["regPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
			   		$2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

			   	$encriptarEmail = md5($_POST["regEmail"]);

				$datos = array("nombre"=>$_POST["regUsuario"],
							   "password"=> $encriptar,
							   "email"=> $_POST["regEmail"],
							   "foto"=>"",
							   "modo"=> "directo",
							   "cupon"=> "no",
							   "verificacion"=> 1,
							   "emailEncriptado"=>$encriptarEmail);

				$tabla = "usuarios";

				$respuesta = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);

				if($respuesta == "ok"){

					/*=============================================
					ACTUALIZAR NOTIFICACIONES NUEVOS USUARIOS
					=============================================*/

					$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

					$nuevoUsuario = $traerNotificaciones["nuevosUsuarios"] + 1;

					ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevosUsuarios", $nuevoUsuario);

					/*=============================================
					VERIFICACIÓN CORREO ELECTRÓNICO
					=============================================*/
					/*echo '<script> 
						</script>';*/
					$url = Ruta::ctrRuta();	

					$datosCorreo = array("tituloFrom"=>"Registro ecommerce",
							   "subject"=> "Por favor verifique su dirección de correo electrónico",
							   "address"=> $_POST["regEmail"],
							   "msgHtml"=>'
									<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
									<center>
										<img style="padding:20px; width:10%" src="'.$url.'vistas/img/plantilla/logo1.png">
									</center>
									<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
										<center>
										<img style="padding:20px; width:15%" src="'.$url.'vistas/img/plantilla/logo1.png">
										<h3 style="font-weight:100; color:#999">VERIFIQUE SU DIRECCIÓN DE CORREO ELECTRÓNICO</h3>
										<hr style="border:1px solid #ccc; width:80%">
										<h4 style="font-weight:100; color:#999; padding:0 20px">Para comenzar a usar su cuenta de Tienda Virtual, debe confirmar su dirección de correo electrónico</h4>
										<a href="'.$url.'verificar/'.$encriptarEmail.'" target="_blank" style="text-decoration:none">
										<div style="line-height:60px; background:#0aa; width:60%; color:white">Verifique su dirección de correo electrónico</div>
										</a>
										<br>
										<hr style="border:1px solid #ccc; width:80%">
										<h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>
										</center>
									</div>
								</div>');

				    $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);

                    //-----------------------------------------------------------------------------------------------------------

					if($respuestaCorreo!="ok"){

						echo '<script> 

							swal({
								  title: "¡ERROR!",
								  text: "¡Ha ocurrido un problema enviando verificación de correo electrónico a '.$_POST["regEmail"].'!",
								  type:"error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});
						</script>';
					}else{
						echo '<script> 
							swal({
								  title: "¡OK!",
								  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico '.$_POST["regEmail"].' para verificar la cuenta!",
								  type:"success",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
								},
								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';
					}
			}else{

				echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡Error al registrar el usuario, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});
				</script>';
			}
		}
	  }

	}
	/*=============================================
	MOSTRAR USUARIO
	=============================================*/

	static public function ctrMostrarUsuario($item, $valor){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR USUARIO HISTORICO
	=============================================*/

	static public function ctrMostrarUsuarioHistorico($item, $valor){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlMostrarUsuarioHistorico($tabla, $item, $valor);

		return $respuesta;

	}
	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/

	static public function ctrActualizarUsuario($id, $item, $valor){

		$tabla = "usuarios";

		$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item, $valor);

		return $respuesta;

	}
	/*=============================================
	INGRESO DE USUARIO
	=============================================*/

	public function ctrIngresoUsuario(){

		if(isset($_POST["ingEmail"])){

			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingEmail"]) &&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

				$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla = "usuarios";
				$item = "email";
				$valor = $_POST["ingEmail"];

				$respuesta = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

				if($respuesta["email"] == $_POST["ingEmail"] && $respuesta["password"] == $encriptar){

					if($respuesta["verificacion"] == 1){

						echo'<script>

							swal({
								  title: "¡NO HA VERIFICADO SU CORREO ELECTRÓNICO!",
								  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo para verififcar la dirección de correo electrónico '.$respuesta["email"].'!",
								  type: "error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
							},

							function(isConfirm){
									 if (isConfirm) {	   
									    history.back();
									  } 
							});

							</script>';

					}else{
                        $_SESSION["validarSesionAux"] = "ok";
						$_SESSION["validarSesion"] = "ok";
						$_SESSION["id"] = $respuesta["id"];
						$_SESSION["nombre"] = $respuesta["nombre"];
						$_SESSION["foto"] = $respuesta["foto"];
						$_SESSION["email"] = $respuesta["email"];
						$_SESSION["password"] = $respuesta["password"];
						$_SESSION["modo"] = $respuesta["modo"];

						//$servidor = Ruta::ctrRutaServidor();
						$url = Ruta::ctrRuta();
						//localStorage.setItem("rutaActual", $servidor);

						echo '<script>
						         window.location = "'.$url.'";
							  </script>';

						//window.location = localStorage.getItem("rutaActual");
					}

				}else{

					echo'<script>

							swal({
								  title: "¡ERROR AL INGRESAR!",
								  text: "¡Por favor revise que el email exista o la contraseña coincida con la registrada!",
								  type: "error",
								  confirmButtonText: "Cerrar",
								  closeOnConfirm: false
							},

							function(isConfirm){
									 if (isConfirm) {	   
									    window.location = localStorage.getItem("rutaActual");
									  } 
							});

							</script>';

				}

			}else{

				echo '<script> 

						swal({
							  title: "¡ERROR!",
							  text: "¡Error al ingresar al sistema, no se permiten caracteres especiales!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';

			}

		}

	}

	/*=============================================
	OLVIDO DE CONTRASEÑA
	=============================================*/

	public function ctrOlvidoPassword(){

		

		if(isset($_POST["passEmail"])){

			



			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["passEmail"])){

				
				/*=============================================
				GENERAR CONTRASEÑA ALEATORIA
				=============================================*/

				function generarPassword($longitud){

					$key = "";
					$pattern = "1234567890abcdefghijklmnopqrstuvwxyz";

					$max = strlen($pattern)-1;

					for($i = 0; $i < $longitud; $i++){

						$key .= $pattern{mt_rand(0,$max)};

					}

					return $key;

				}

				$nuevaPassword = generarPassword(11);

				$encriptar = crypt($nuevaPassword, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$tabla = "usuarios";

				$item1 = "email";
				$valor1 = $_POST["passEmail"];

				$respuesta1 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item1, $valor1);

				if($respuesta1){

					$id = $respuesta1["id"];
					$item2 = "password";
					$valor2 = $encriptar;

					$respuesta2 = ModeloUsuarios::mdlActualizarUsuario($tabla, $id, $item2, $valor2);

					if($respuesta2  == "ok"){

						/*=============================================
						CAMBIO DE CONTRASEÑA
						=============================================*/
						$url = Ruta::ctrRuta();	
						
						$datosCorreo = array("tituloFrom"=>"Nueva contraseña",
							   "subject"=> "Solicitud de nueva contraseña",
							   "address"=> $_POST["passEmail"],
							   "msgHtml"=>'
							   <div style="width:100%;position:relative; font-family:sans-serif; padding-bottom:40px">
							   <center>
								   <img style="padding:20px; width:200px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/logo1.png">
							   </center>
							   <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
								   <center>
								   <img style="padding:20px; width:70px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/icon-pass.png">
								   <h3 style="font-weight:100; color:#999">SOLICITUD DE NUEVA CONTRASEÑA</h3>
								   <hr style="border:1px solid #ccc; width:80%">
								   <h4 style="font-weight:100; color:#999; padding:0 20px"><strong>Su nueva contraseña: </strong>'.$nuevaPassword.'</h4>
								   <a href="'.$url.'" target="_blank" style="text-decoration:none">
								   <div style="line-height:60px; background:#0aa; width:60%; color:white">Ingrese nuevamente al sitio</div>
								   </a>
								   <br>
								   <hr style="border:1px solid #ccc; width:80%">
								   <h5 style="font-weight:100; color:#999">Si no se inscribió en esta cuenta, puede ignorar este correo electrónico y la cuenta se eliminará.</h5>
								   </center>
							   </div>
						   </div>');

				        $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);



						if($respuestaCorreo!="ok"){

							echo '<script> 
								swal({
									  title: "¡ERROR!",
									  text: "¡Ha ocurrido un problema enviando cambio de contraseña a '.$_POST["passEmail"].'!",
									  type:"error",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},
									function(isConfirm){
										if(isConfirm){
											history.back();
										}
								});
							</script>';
						}else{
							echo '<script> 
								swal({
									  title: "¡OK!",
									  text: "¡Por favor revise la bandeja de entrada o la carpeta de SPAM de su correo electrónico '.$_POST["passEmail"].' para su cambio de contraseña!",
									  type:"success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
									},
									function(isConfirm){
										if(isConfirm){
											history.back();
										}
								});

							</script>';
						}
					}
				}else{

					echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡El correo electrónico no existe en el sistema!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});
					</script>';
				}
			}else{
				echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡Error al enviar el correo electrónico, está mal escrito!",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';
			}
		}
	}

	/*=============================================
	REGISTRO CON REDES SOCIALES
	=============================================*/

	static public function ctrRegistroRedesSociales($datos){

		$tabla = "usuarios";
		$item = "email";
		$valor = $datos["email"];
		$modoRed = $datos["modo"];
		
		//$url = Ruta::ctrRuta();	
		
		$emailRepetido = false;
		$respuestaFinal= "";

		$respuesta0 = ModeloUsuarios::mdlMostrarUsuariohistorico($tabla, $item, $valor);
		

		//si ya existe, solo dejarlo pasar
		if($respuesta0){

			if($modoRed !="google"){
				/*echo '<script> 
						swal({
							  title: "¡ERROR!",
							  text: "¡El correo electrónico '.$datos["email"].', ya está registrado en el sistema !",
							  type:"error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},
							function(isConfirm){
								if(isConfirm){
									
									window.location = "index.php";
								}
						});
				</script>';*/

				$emailRepetido = true;
				echo "error";
				//return;

			}else{
				//caso de ingreso  de google account

              //update

               $respuesta = ControladorUsuarios::ctrActualizarUsuario($respuesta0["id"], "estatus", 1);

				
				//$respuesta2 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);
				
				$_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta0["id"];
				$_SESSION["nombre"] = $respuesta0["nombre"];
				$_SESSION["foto"] = $respuesta0["foto"];
				$_SESSION["email"] = $respuesta0["email"];
				$_SESSION["password"] = $respuesta0["password"];
				$_SESSION["modo"] = $respuesta0["modo"];

				//validar si este registro proviene de un recomendado, para premiarlo
				//Obtiene el correo del q lo recomendo                              
				$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta0["email"]);
				//si es un recomendado
		        if($recomendado != null){
					//Marca el campo aplicado como 1                                           
					$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta["email"]);
					if($recomendadoAplicado=="ok"){

						//Obtiene el cupon de un nivel1
						$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');

						$variables = array();
						$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
						$variables['NOMBRE'] = $recomendado["recomendadopor"];

						$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

						foreach($variables as $key => $value)
						{
							$template = str_replace('{{ '.$key.' }}', $value, $template);
						}


						//-------------------------------------------------------------------------
						$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
						"subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
						"address"=> $recomendado["recomendadopor"],
						"msgHtml"=> $template);

		                $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
					}
				}
				//-----------------------------------------------------------------------------------------

				//echo "<span style='color:white'>ok</span>";
				$respuestaFinal= "reingreso";
				//---------------------------------------------------------------------
				return $respuestaFinal;
			}
			//$emailRepetido = true;
		}else{

			//registra al usuario que es no es de red social
			$respuesta1 = ModeloUsuarios::mdlRegistroUsuario($tabla, $datos);

		}

		//registro exitoso
		if($respuesta1 == "ok"){

           
			$respuesta2 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

			if($respuesta2["modo"] == "facebook"){

				session_start();
                $_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				//validar si este registro proviene de un recomendado, para premiarlo
				//Obtiene el correo del q lo recomendo                              
				$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta2["email"]);
				//si es un recomendado
		        if($recomendado != null){
					//Marca el campo aplicado como 1                                           
					$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta2["email"]);
					if($recomendadoAplicado=="ok"){

						//Obtiene el cupon de un nivel1
						$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');


						$variables = array();
						$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
						$variables['NOMBRE'] = $recomendado["recomendadopor"];

						$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

						foreach($variables as $key => $value)
						{
							$template = str_replace('{{ '.$key.' }}', $value, $template);
						}


						//-------------------------------------------------------------------------
						$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
						"subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
						"address"=> $recomendado["recomendadopor"],
						"msgHtml"=> $template );

		                $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
					}
				}
				//-----------------------------------------------------------------------------------------

				//echo "ok";

			}else if($respuesta2["modo"] == "google"){

				$_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				//obtiene su cupon por registrarse
				$cuponRegistro = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','REGISTRO');

				$variables = array();
				$variables['CUPON'] = $cuponRegistro["codigo"];
				$variables['NOMBRE'] = $respuesta2["nombre"];

				$template = file_get_contents("plantilla-registro-nuevo.html",true);
				
				foreach($variables as $key => $value)
				{
					$template = str_replace('{{ '.$key.' }}', $value, $template);
				}

				

				$datosCorreoRegistro = array("tituloFrom"=>"!Su cupón de descuento por registrarse!",
						"subject"=> "!Su cupón de descuento por registrarse a Dummbells Mx!",
						"address"=> $respuesta2["email"],
						"msgHtml"=>$template);

					$respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreoRegistro);
					//var_dump($respuestaCorreo);

				//--------------------------------------------------------------------

				//validar si este registro proviene de un recomendado, para premiarlo
				//Obtiene el correo del q lo recomendo                              
				$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta2["email"]);
				//si es un recomendado
		        if($recomendado != null){
					//Marca el campo aplicado como 1                                           
					$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta2["email"]);
					if($recomendadoAplicado=="ok"){

						//Obtiene el cupon de un nivel1
						$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');


						$variables = array();
						$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
						$variables['NOMBRE'] = $recomendado["recomendadopor"];

						$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

						foreach($variables as $key => $value)
						{
							$template = str_replace('{{ '.$key.' }}', $value, $template);
						}


						//-------------------------------------------------------------------------
						$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
						"subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
						"address"=> $recomendado["recomendadopor"],
						"msgHtml"=> $template);

		                $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
					}
				}
				//-----------------------------------------------------------------------------------------

				//echo "<span style='color:white'>ok</span>";
				$respuestaFinal= "registro";

			}
			/*else{
				echo "";
			}*/
		}

		return $respuestaFinal;
	}

	/*=============================================
	REGISTRO CON REDES SOCIALES
	=============================================*/

	static public function ctrLoginRedesSociales($datos){

		$tabla = "usuarios";
		$item = "email";
		$valor = $datos["email"];
		
		//$url = Ruta::ctrRuta();	
		
		$emailValido = false;

		$respuesta0 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

		if($respuesta0){
				//echo "ok";
				$emailValido = true;
				//return;
		}


		if($emailValido == true){

           
			$respuesta2 = ModeloUsuarios::mdlMostrarUsuario($tabla, $item, $valor);

			if($respuesta2["modo"] == "facebook"){

				session_start();
                $_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				//validar si este registro proviene de un recomendado, para premiarlo
				//Obtiene el correo del q lo recomendo                              
				$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta2["email"]);

				//si es un recomendado
		        if($recomendado != null){
					//Marca el campo aplicado como 1                                           
					$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta2["email"]);
					if($recomendadoAplicado=="ok"){

						//Obtiene el cupon de un nivel1
						$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');

						$variables = array();
						$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
						$variables['NOMBRE'] = $recomendado["recomendadopor"];

						$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

						foreach($variables as $key => $value)
						{
							$template = str_replace('{{ '.$key.' }}', $value, $template);
						}

						//-------------------------------------------------------------------------
						$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
						"subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
						"address"=> $recomendado["recomendadopor"],
						"msgHtml"=> $template);

		                $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
					}
				}
				//-----------------------------------------------------------------------------------------

				echo "ok";

			}else if($respuesta2["modo"] == "google"){

				$_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $respuesta2["id"];
				$_SESSION["nombre"] = $respuesta2["nombre"];
				$_SESSION["foto"] = $respuesta2["foto"];
				$_SESSION["email"] = $respuesta2["email"];
				$_SESSION["password"] = $respuesta2["password"];
				$_SESSION["modo"] = $respuesta2["modo"];

				//validar si este registro proviene de un recomendado, para premiarlo
				//Obtiene el correo del q lo recomendo                              
				$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$respuesta2["email"]);
				//si es un recomendado
		        if($recomendado != null){
					//Marca el campo aplicado como 1                                           
					$recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$respuesta2["email"]);
					if($recomendadoAplicado=="ok"){

						//Obtiene el cupon de un nivel1
						$cuponTipoRecomendado = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','NIVEL1');

						$variables = array();
						$variables['CUPON'] = $cuponTipoRecomendado["codigo"];
						$variables['NOMBRE'] = $recomendado["recomendadopor"];

						$template = file_get_contents("plantilla-beneficio-invitacion.html",true);

						foreach($variables as $key => $value)
						{
							$template = str_replace('{{ '.$key.' }}', $value, $template);
						}

						
						//-------------------------------------------------------------------------
						$datosCorreo = array("tituloFrom"=>"Beneficios ecommerce",
						"subject"=> "Un beneficio más por recomendar a Dumbbells.mx",
						"address"=> $recomendado["recomendadopor"],
						"msgHtml"=> $template);

		                $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
					}
				}
				//-----------------------------------------------------------------------------------------

				echo "<span style='color:white'>ok</span>";
			}
			else{
				echo "";
			}

		}
	}

	/*=============================================
	ACTUALIZAR PERFIL
	=============================================*/

	public function ctrActualizarPerfil(){

		if(isset($_POST["editarNombre"])){

			/*=============================================
			VALIDAR IMAGEN
			=============================================*/

			$ruta = $_POST["fotoUsuario"];

			if(isset($_FILES["datosImagen"]["tmp_name"]) && !empty($_FILES["datosImagen"]["tmp_name"])){

				/*=============================================
				PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
				=============================================*/

				$directorio = "vistas/img/usuarios/".$_POST["idUsuario"];

				if(!empty($_POST["fotoUsuario"])){

					unlink($_POST["fotoUsuario"]);
				
				}else{

					mkdir($directorio, 0755);

				}

				/*=============================================
				GUARDAMOS LA IMAGEN EN EL DIRECTORIO
				=============================================*/

				list($ancho, $alto) = getimagesize($_FILES["datosImagen"]["tmp_name"]);

				$nuevoAncho = 500;
				$nuevoAlto = 500;	

				$aleatorio = mt_rand(100, 999);

				if($_FILES["datosImagen"]["type"] == "image/jpeg"){

					$ruta = "vistas/img/usuarios/".$_POST["idUsuario"]."/".$aleatorio.".jpg";

					/*=============================================
					MOFICAMOS TAMAÑO DE LA FOTO
					=============================================*/

					$origen = imagecreatefromjpeg($_FILES["datosImagen"]["tmp_name"]);

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagejpeg($destino, $ruta);

				}

				if($_FILES["datosImagen"]["type"] == "image/png"){

					$ruta = "vistas/img/usuarios/".$_POST["idUsuario"]."/".$aleatorio.".png";

					/*=============================================
					MOFICAMOS TAMAÑO DE LA FOTO
					=============================================*/

					$origen = imagecreatefrompng($_FILES["datosImagen"]["tmp_name"]);

					$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

					imagealphablending($destino, FALSE);
    			
					imagesavealpha($destino, TRUE);

					imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

					imagepng($destino, $ruta);

				}

			}

			if($_POST["editarPassword"] == ""){

				$password = $_POST["passUsuario"];

			}else{

				$password = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

			}

			$datos = array("nombre" => $_POST["editarNombre"],
						   "email" => $_POST["editarEmail"],
						   "password" => $password,
						   "foto" => $ruta,
						   "id" => $_POST["idUsuario"]);

			$tabla = "usuarios";

			$respuesta = ModeloUsuarios::mdlActualizarPerfil($tabla, $datos);

			if($respuesta == "ok"){
                $_SESSION["validarSesionAux"] = "ok";
				$_SESSION["validarSesion"] = "ok";
				$_SESSION["id"] = $datos["id"];
				$_SESSION["nombre"] = $datos["nombre"];
				$_SESSION["foto"] = $datos["foto"];
				$_SESSION["email"] = $datos["email"];
				$_SESSION["password"] = $datos["password"];
				$_SESSION["modo"] = $_POST["modoUsuario"];

				echo '<script> 

						swal({
							  title: "¡OK!",
							  text: "¡Su cuenta ha sido actualizada correctamente!",
							  type:"success",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									history.back();
								}
						});

				</script>';


			}

		}

	}


	/*=============================================
	MOSTRAR COMPRAS
	=============================================*/

	static public function ctrMostrarCompras($item, $valor){

		$tabla = "compras";

		$respuesta = ModeloUsuarios::mdlMostrarCompras($tabla, $item, $valor);

		return $respuesta;

	}


	/*=============================================
	MOSTRAR DIRECCIONES
	=============================================*/

	static public function ctrMostrarDirecciones($item, $valor){

		$tabla = "direcciones";

		$respuesta = ModeloUsuarios::mdlMostrarDirecciones($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR FORMAS DE PAGO
	=============================================*/

	static public function ctrMostrarformasPago($item, $valor){

		$tabla = "formaspago";

		$respuesta = ModeloUsuarios::mdlMostrarformasPago($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR COMENTARIOS EN PERFIL
	=============================================*/

	static public function ctrMostrarComentariosPerfil($datos){

		$tabla = "comentarios";

		$respuesta = ModeloUsuarios::mdlMostrarComentariosPerfil($tabla, $datos);

		return $respuesta;

	}

		/*=============================================
	MOSTRAR 5 COMENTARIOS EN PERFIL
	=============================================*/

	static public function ctrMostrar5ComentariosPerfil(){

		$tabla = "comentarios";

		$respuesta = ModeloUsuarios::mdlMostrar5ComentariosPerfil($tabla);

		return $respuesta;

	}


	/*=============================================
	ENVIAR CALIFICACION
	=============================================*/
	
	public function ctrSendCalificacion(){

		
		
       
		//obtener idUsuario y idProducto

		//obtenr idComentario con los 2 ids anteriores
		//SELECT comentarios.id FROM `compras`, comentarios where comentarios.id_usuario=compras.id_usuario and comentarios.id_producto=compras.id_producto
        //and compras.id=47 LIMIT 1

		//continuar el proceso
		//var_dump( $_POST["idVenta"] );

		
		if(isset($_POST["idVenta"])){
			if(preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["comentario"])){
				
				$idVenta = $_POST["idVenta"];

				$idComentario = ModeloUsuarios::mdlGetIdComentario( $idVenta);
				//var_dump($idComentario[0]);
			//	echo $idComentario;


				if($idComentario[0] != ""){

					$tabla = "comentarios";
					$datos = array("id"=>$idComentario[0],
								   "calificacion"=>$_POST["puntaje"],
								   "comentario"=>$_POST["comentario"]);

					$respuesta = ModeloUsuarios::mdlActualizarComentario($tabla, $datos);

					if($respuesta == "ok"){
						echo'<script>
								swal({
									  title: "¡GRACIAS POR COMPARTIR SU OPINIÓN!",
									  text: "¡Su calificación y comentario ha sido guardado!",
									  type: "success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
								},
								function(isConfirm){
										 if (isConfirm) {	   
										   history.back();
										  } 
								});
							  </script>';
					}

				}else{

					echo'<script>
						swal({
							  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
							  text: "¡El comentario no puede estar vacío!",
							  type: "error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},
						function(isConfirm){
								 if (isConfirm) {	   
								   history.back();
								  } 
						});
					  </script>';
				}	
			}else{

				echo'<script>
					swal({
						  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
						  text: "¡El comentario no puede llevar caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},
					function(isConfirm){
							 if (isConfirm) {	   
							   history.back();
							  } 
					});
				  </script>';
			}

		}

	}

	/*=============================================
	ACTUALIZAR COMENTARIOS
	=============================================*/

	public function ctrActualizarComentario(){

		if(isset($_POST["idComentario"])){

			if(preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["comentario"])){

				if($_POST["comentario"] != ""){

					$tabla = "comentarios";

					$datos = array("id"=>$_POST["idComentario"],
								   "calificacion"=>$_POST["puntaje"],
								   "comentario"=>$_POST["comentario"]);

					$respuesta = ModeloUsuarios::mdlActualizarComentario($tabla, $datos);

					if($respuesta == "ok"){

						echo'<script>

								swal({
									  title: "¡GRACIAS POR COMPARTIR SU OPINIÓN!",
									  text: "¡Su calificación y comentario ha sido guardado!",
									  type: "success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
								},

								function(isConfirm){
										 if (isConfirm) {	   
										   history.back();
										  } 
								});

							  </script>';

					}

				}else{

					echo'<script>

						swal({
							  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
							  text: "¡El comentario no puede estar vacío!",
							  type: "error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   history.back();
								  } 
						});

					  </script>';

				}	

			}else{

				echo'<script>

					swal({
						  title: "¡ERROR AL ENVIAR SU CALIFICACIÓN!",
						  text: "¡El comentario no puede llevar caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},

					function(isConfirm){
							 if (isConfirm) {	   
							   history.back();
							  } 
					});

				  </script>';

			}

		}

	}

	/*=============================================
	ACTUALIZAR DOMICILIO
	=============================================*/

	public function ctrActualizardomicilio(){

		if(isset($_POST["hdIdomicilio"])){

			if(preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["hdIdomicilio"])){

				if($_POST["hdIdomicilio"] != ""){

					$tabla = "direcciones";

					$datos = array("id"=>$_POST["hdIdomicilio"],
								   "calle"=>$_POST["domicilioCalle"],
								   "cp"=>$_POST["domicilioCp"],
								   "municipiocolonia"=>$_POST["domicilioColonia"],
								   "estado"=>$_POST["domicilioEdo"],
								   "pais"=>$_POST["domicilioPais"],
								   "idusuario"=>$_SESSION["id"],
								   "nota"=>$_POST["domicilioNota"]);

					if($_POST["hdIdomicilio"] == "0"){
						$respuesta = ModeloUsuarios::mdlInsertaDomicilio($tabla, $datos);
					}else{
						$respuesta = ModeloUsuarios::mdlActualizarDomicilio($tabla, $datos);
					}

					

					if($respuesta == "ok"){
						echo'<script>
								swal({
									  title: "¡GRACIAS POR SU INFORMACIÓN!",
									  text: "¡Su domicilio de entrega ha sido registrado!",
									  type: "success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
								},

								function(isConfirm){
										 if (isConfirm) {	   
										   history.back();
										  } 
								});
							  </script>';
					}
				}else{
					echo'<script>
						swal({
							  title: "¡ERROR AL ACTUALIZAR SU DOMICILIO!",
							  text: "¡El domicilio no puede estar vacío!",
							  type: "error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   history.back();
								  } 
						});
					  </script>';
				}	
			}else{
				echo'<script>
					swal({
						  title: "¡ERROR AL ACTUALIZAR SU DOMICILIO!",
						  text: "¡El domicilio no puede llevar caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},

					function(isConfirm){
							 if (isConfirm) {	   
							   history.back();
							  } 
					});
				  </script>';
			}
		}
	}

	
	/*=============================================
	ACTUALIZAR FORMA DE PAGO
	=============================================*/

	public function ctrActualizarformapago(){

		if(isset($_POST["hdIdformapago"])){

			if(preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["hdIdformapago"])){

				if($_POST["hdIdformapago"] != ""){

					$tabla = "formaspago";

					$datos = array("id"=>$_POST["hdIdformapago"],
								   "formapago"=>"STRIPE",
								   "nombre"=>strtoupper($_POST["pagonombre"]),
								   "cuenta"=>$_POST["pagotarjeta"],
								   "fechacaducidad"=>$_POST["pagovigencia"],
								   "tipo"=>strtoupper($_POST["pagotipo"]),
								   "estatus"=>1,
								   "idusuario"=>$_SESSION["id"]);

					if($_POST["hdIdformapago"] == "0"){
						$respuesta = ModeloUsuarios::mdlInsertaFormaPago($tabla, $datos);
					}else{
						$respuesta = ModeloUsuarios::mdlActualizarFormaPago($tabla, $datos);
					}

					

					if($respuesta == "ok"){
						echo'<script>
								swal({
									  title: "¡GRACIAS POR SU INFORMACIÓN!",
									  text: "¡Su forma de pago ha sido registrado!",
									  type: "success",
									  confirmButtonText: "Cerrar",
									  closeOnConfirm: false
								},

								function(isConfirm){
										 if (isConfirm) {	   
										   history.back();
										  } 
								});
							  </script>';
					}
				}else{
					echo'<script>
						swal({
							  title: "¡ERROR AL PROCESAR SU FORMA DE PAGO!",
							  text: "¡La forma de pago no puede estar vacío!",
							  type: "error",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   history.back();
								  } 
						});
					  </script>';
				}	
			}else{
				echo'<script>
					swal({
						  title: "¡ERROR AL PROCESAR SU FORMA DE PAGO!",
						  text: "¡La forma de pago no puede llevar caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},

					function(isConfirm){
							 if (isConfirm) {	   
							   history.back();
							  } 
					});
				  </script>';
			}
		}
	}

	/*=============================================
	QUITAR DOMICILIO DE LISTA DE DOMICILIOS
	=============================================*/
	static public function ctrQuitarDomicilio($datos){

		$tabla = "direcciones";

		$respuesta = ModeloUsuarios::mdlQuitarDomicilio($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
	AGREGAR A LISTA DE DESEOS
	=============================================*/

	static public function ctrAgregarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlAgregarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR LISTA DE DESEOS
	=============================================*/

	static public function ctrMostrarDeseos($item){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlMostrarDeseos($tabla, $item);

		return $respuesta;

	}

	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/
	static public function ctrQuitarDeseo($datos){

		$tabla = "deseos";

		$respuesta = ModeloUsuarios::mdlQuitarDeseo($tabla, $datos);

		return $respuesta;

	}

	/*=============================================
	ELIMINAR USUARIO
	=============================================*/

	public function ctrEliminarUsuario(){

		if(isset($_GET["id"])){

			$tabla1 = "usuarios";		
			$tabla2 = "comentarios";
			$tabla3 = "compras";
			$tabla4 = "deseos";

			$id = $_GET["id"];

			if($_GET["foto"] != ""){

				unlink($_GET["foto"]);
				rmdir('vistas/img/usuarios/'.$_GET["id"]);

			}

			$respuesta = ModeloUsuarios::mdlEliminarUsuario($tabla1, $id);
			
			//ModeloUsuarios::mdlEliminarComentarios($tabla2, $id);

			//ModeloUsuarios::mdlEliminarCompras($tabla3, $id);

			//ModeloUsuarios::mdlEliminarListaDeseos($tabla4, $id);

			if($respuesta == "ok"){

		    	$url = Ruta::ctrRuta();

		    	echo'<script>

						swal({
							  title: "¡SU CUENTA HA SIDO BORRADA!",
							  text: "¡Debe registrarse nuevamente si desea ingresar!",
							  type: "success",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
						},

						function(isConfirm){
								 if (isConfirm) {	   
								   window.location = "'.$url.'salir";
								  } 
						});

					  </script>';

		    }

		}

	}

	/*=============================================
	ENVIO CORREO CUPON POR REGISTRO
	=============================================*/
	static public function ctrSendemailCupon($datos){
		/*=============================================
					    	ENVÍO CORREO ELECTRÓNICO
		=============================================*/
			if(isset($_POST['nombre'])){

				if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombre"]) &&
			       preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["email"])){

					$respuesta = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','REGISTRO');
					 
					date_default_timezone_set("America/Mexico_City");



				$variables = array();
				$variables['CUPON'] = $respuesta["codigo"];
				$variables['NOMBRE'] = $_POST["nombre"];
				$template = file_get_contents("plantilla-registro-nuevo.html",true);
				foreach($variables as $key => $value)
				{
					$template = str_replace('{{ '.$key.' }}', $value, $template);
				}



				//$url = Ruta::ctrRuta();	
				$mailx = new PHPMailer;

				try{
					$mailx->CharSet = 'UTF-8';
					$mailx->isSMTP();   
					$mailx->Host       = 'mail.dumbbells.mx'; 
					$mailx->SMTPAuth   = true;//true; 
					$mailx->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);
					$mailx->Username   = 'erdnando@dumbbells.mx'; 
					$mailx->Password   = 'Adqdisp06ERV';  
					$mailx->Port       = 587;    
					$mailx->setFrom("contacto@dumbbells.mx", '!Su cupón de descuento por registrarse!');
					$mailx->Subject = "!Su cupón de descuento por registrarse al ecommerce Dummbells!";
					$mailx->addAddress($_POST["email"]);
					$mailx->msgHTML($template);

					$envio = $mailx->Send();
					if(!$envio){
						echo '<script> 

							swal({
								title: "¡ERROR!",
								text: "¡Ha ocurrido un problema enviando el mensaje!",
								type:"error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';

					}else{
                       //lo envia cuando una red social google se registra con cupon 1 vez
						echo '<script> 
							swal({
							title: "¡Gracias por registrarte!",
							text: "¡Su cupón ha sido enviado a su correo, gracias por participar!",
							type: "success",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){
									if (isConfirm) {	  
											history.back();
										}
							});
						</script>';
					}
				}
				catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}

			    }
				

				echo "ok";
			}
				//-----------------------------------------------------------------------------------------------------

	}

    static public function ctrRecomendarUsuario(){
	/*=============================================
	ENVÍO CORREO ELECTRÓNICO 
	=============================================*/
		

			if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailReferenciado"])){

				$tabla = "recomendados";
				$datos = array("email"=>$_SESSION["email"],
							   "emailReferenciado"=>$_POST["emailReferenciado"],
							  );
				 
				$respuesta = ModeloUsuarios::mdlInsertaRecomendado($tabla,$datos);

				if($respuesta=="ok"){

					$url = Ruta::ctrRuta();	

					$variables = array();
					$variables['TEINVITA'] = $_SESSION["nombre"];
					$template = file_get_contents("plantilla-invitacion-correo.html",true);
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{ '.$key.' }}', $value, $template);
					}
					
                    
					$datosCorreo = array("tituloFrom"=>'!Esta tienda esta super '.$_SESSION["nombre"].'!',
							   "subject"=> "!Dummbells y ".$_SESSION["nombre"]." te invitan a conocer esta tienda online!",
							   "address"=> $_POST["emailReferenciado"],
							   "msgHtml"=> $template);

				       $respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
				//-------------------------------------------------------------------------------------------------------

					

						if($respuestaCorreo!="ok"){
							echo '<script> 

								swal({
									title: "¡ERROR!",
									text: "¡Ha ocurrido un problema enviando el mensaje!",
									type:"error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
									},
									function(isConfirm){

										if(isConfirm){
											window.location = "index.php";
										}
								});

							</script>';
						}else{
							echo '<script> 
								swal({
									html:true,
									title: "¡Gracias por recomendarnos!",
									text: "¡En cuanto tu invitado se registre recibirás tu cupón.<br>Gracias por participar!<br><br>No te detengas, invita a todos.",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
								},

								function(isConfirm){
										if (isConfirm) {	  
											window.location = "index.php";
											}
								});

							</script>';
						}
					
				}
			}
			//-----------------------------------------------------------------------------------------------------

    }


	static public function ctrInsertaVentaBillPocket($datosHook){

		$continua = false;

		try{

		$transaccion = ModeloUsuarios::mdlObtenerTransaccion('transacciones','transaccionId',$datosHook->externalId);
		$usuario = ModeloUsuarios::mdlMostrarUsuario("usuarios", "id", $transaccion["idUsuario"]);
		$direccion = ModeloUsuarios::mdlObtenerDireccion("direcciones", "id", $transaccion["idDomicilio"]);
		 

		$objJson = json_decode($transaccion["detalle"]);

		//echo json_encode($objJson);
		$arrIdProductos = explode("," , $objJson->{'xidProductoArray'});
	    $arrCantidadProductos = explode("," , $objJson->{'xcantidadArray'});//<--cantidadProd1,cantidadProd2,cantidadProd3, ...
		$arrPrecioProductos = explode("," , $objJson->{'xvalorItemArray'});
		$arrTituloProductos = explode("," , $objJson->{'xtituloArray'});//<--transaccionId|producto-talla-color , transaccionId|producto-talla-color ,...
		
		//echo json_encode($arrIdProductos);
		$listProductos='';
		$altura = 250;
		$veces=0;

		$stockDetalle="";

		//Genera detalle stock
		for($ii = 0; $ii < count($arrIdProductos); $ii ++){
		
			//obtener la talla y cantidad del producto q se esta recorriendo
			$tituloProducto = explode("|" , $arrTituloProductos[$ii]);  // T825347315|aux3-ch-azul-weee,
			$productoTallaColor = explode("-" , $tituloProducto[1]);   // aux3-ch-azul-weee

			$talla = $productoTallaColor[1];// ch
			$cantidadCompradas = $arrCantidadProductos[$ii];// 1
			//-----------------------------------------------------------------------------------------------

			//Obtiene datos del producto de DB
			$producto = ModeloProductos::mdlMostrarInfoProducto("productos", "id",$arrIdProductos[$ii]);

			//se obtiene la definicion actual de tallas y cantidades de DB en un array
			$arrStockDetalleFromDB = explode("|" , $producto["stockDetalle"]); //ch-10|m-20|g-30|

			//se recorren las tallas-cantidades originales para restarles lo comprado
			for($x=0; $x < count($arrStockDetalleFromDB); $x++){

				$tallax = explode("-" , $arrStockDetalleFromDB[$x]);

				if(count($tallax) > 0){
					if($tallax[0] == $talla){
						$stockDetalle.=  $talla ."-" . ($tallax[1]-$cantidadCompradas) . "|";
					}else{
						//$stockDetalle .= $arrStockDetalleFromDB[$x]."|";
					}
			    }
			}
		}

	//	echo json_encode($stockDetalle);







		//Por cada producto
		for($i = 0; $i < count($arrIdProductos); $i ++){

			//$stockDetalle="ch-10|m-20|g-30|";
          try{


			$datos = array(
				"idUsuario"=>$transaccion["idUsuario"],
				"transaccionId"=>$transaccion["transaccionId"],
				"idProducto"=>$arrIdProductos[$i],
				"metodo"=>"billpocket",
				"email"=>$usuario["email"],
				"direccion"=>$direccion["calle"].", ".$direccion["municipio-colonia"]." ,".$direccion["cp"].". ".$direccion["estado"]." ,".$direccion["pais"]." . NOTA:".$direccion["nota"],
				"pais"=>"MEXICO",
				"cantidad"=>$arrCantidadProductos[$i],
				"stockDetalle"=>$stockDetalle,
				"envio"=> 0,
				"pago"=>$arrPrecioProductos[$i]);

			$listProductos.=
				'<tr style="background-color: #f9f9f9;">
					<td class="valorTitulo" style="padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;">'.$arrTituloProductos[$i].'</td>
					<td class="valorCantidad" style="padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;">'.$arrCantidadProductos[$i].'</td>
					<td style="padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;">$<span class="valorItem" valor="'.$arrPrecioProductos[$i].'">'.$arrPrecioProductos[$i].'</span></td>
				</tr>
				<tr></tr>';

				$veces++;
	
			ModeloCarrito::mdlNuevasCompras("compras", $datos);
			ModeloCarrito::mdlActualizaStock("productos", $datos);
			ModeloUsuarios::mdlIngresoComentarios("comentarios", $datos);
			ModeloCarrito::mdlCleanCarrito("carritos", $transaccion["idUsuario"]);
			$continua=true;

		}catch(Exception $ex){
			echo 'Excepción capturada: ',  $ex->getMessage(), "\n";
		}
		}

		echo "ok";
		
		}catch(Exception $e){
			return $e->getMessage();
			$continua=false;
		}
		/*=============================================
		INSERTA VENTA RECIBIDA EN WEBHOOK BILLPOCKET 
		=============================================*/
		if($continua==true){

			/*=============================================
			ACTUALIZAR NOTIFICACIONES NUEVAS VENTAS
			=============================================*/

			$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

			$nuevaVenta = $traerNotificaciones["nuevasVentas"] + 1;

			ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevasVentas", $nuevaVenta);

			//ENVIA EMAIL DE LA VENTA
			$url = Ruta::ctrRuta();	

			$variables = array();
			$variables['NOMBRE'] = $usuario["nombre"];


			$variables['DETALLE'] = '
				<table class="table table-striped tablaProductos" style="margin-top:0px;width:92%;color:black;font-size: small;
				max-width: 100%;background-color: transparent;border-spacing: 0;border-collapse: collapse;
				margin-bottom: 20px;text-align: left;">
					<thead>
						<tr style="background-color: #f9f9f9;">		
							<th>Producto</th>
							<th>Cantidad</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>'.$listProductos.
					'</tbody>
				</table>';

			$variables['FECHA'] = $transaccion["fecha"];
			$variables['SUBTOTAL'] = "MXN $".$objJson->{'xsubtotal'};
			$variables['IMPUESTO'] = "MXN $".$objJson->{'ximpuesto'};
			$variables['ENVIO'] = "MXN $".$objJson->{'xenvio'};
			$variables['TOTAL'] = "MXN $".$transaccion["total"];

			
			$variables['ALTURA'] = ($altura + ($veces*30)) . "px";

			$template = file_get_contents("plantilla-compra-correo.html",true);

			foreach($variables as $key => $value)
			{
				$template = str_replace('{{ '.$key.' }}', $value, $template);
			}


			$datosCorreo = array("tituloFrom"=>'!Gracias por su compra '.$usuario["nombre"].'!',
								"subject"=> "!Recibo de compra en Dummbells Mx!",
								"address"=> $usuario["email"],
								"msgHtml"=> $template);

			ControladorUsuarios::ctrMailgenerico($datosCorreo);
		}
		echo "saliendo";

				
				//-----------------------------------------------------------------------------------------------------

	}

	static public function ctrSendemailCuponx($datos){
		/*=============================================
					    	ENVÍO CORREO ELECTRÓNICO
				=============================================*/
			

				if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $datos["nombre"]) &&
			       preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $datos["email"])){
					 
					$respuesta = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','REGISTRO');

				date_default_timezone_set("America/Mexico_City");

				$variables = array();
				$variables['CUPON'] = $respuesta["codigo"];
				$variables['NOMBRE'] = $_POST["nombre"];

				$template = file_get_contents("plantilla-registro-nuevo.html",true);
				
				foreach($variables as $key => $value)
				{
					$template = str_replace('{{ '.$key.' }}', $value, $template);
				}


				//$url = Ruta::ctrRuta();	
				$mailx = new PHPMailer;

				try{
					$mailx->CharSet = 'UTF-8';
					$mailx->isSMTP();   
					$mailx->Host       = 'mail.dumbbells.mx'; 
					$mailx->SMTPAuth   = true;//true; 
					$mailx->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);
					$mailx->Username   = 'erdnando@dumbbells.mx'; 
					$mailx->Password   = 'Adqdisp06ERV';  
					$mailx->Port       = 587;    
					$mailx->setFrom("contacto@dumbbells.mx", '!Su cupón de descuento por registrarse!');
					$mailx->Subject = "!Su cupón de descuento por registrarse al ecommerce Dummbells!";
					$mailx->addAddress($_POST["email"]);
					$mailx->msgHTML($template);

					$envio = $mailx->Send();
					if(!$envio){
						//echo 'Mailer Error: ' . $mail->ErrorInfo;
						echo '<script> 

							swal({
								title: "¡ERROR!",
								text: "¡Ha ocurrido un problema enviando el mensaje!",
								type:"error",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
								},

								function(isConfirm){

									if(isConfirm){
										history.back();
									}
							});

						</script>';

					}else{

						echo '<script> 

							swal({
								title: "¡Gracias por registrarte!",
								text: "¡Su cupón ha sido enviado a su correo, gracias por participar!",
							type: "success",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){
									if (isConfirm) {	  
											history.back();
										}
							});

						</script>';
					}


					
				}
				catch (phpmailerException $e) {
					echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
					echo $e->getMessage(); //Boring error messages from anything else!
				}

			    }
				

				echo "ok";
			
				//-----------------------------------------------------------------------------------------------------

	}

	/*=============================================
	FORMULARIO CONTACTENOS
	=============================================*/

	static public function ctrFormularioContactenos(){

		if(isset($_POST['mensajeContactenos'])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nombreContactenos"]) &&
				preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["mensajeContactenos"]) &&
				preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["emailContactenos"])){

					/*=============================================
					ENVÍO CORREO ELECTRÓNICO
					=============================================*/
					$url = Ruta::ctrRuta();	
					$datosCorreo = array("tituloFrom"=>"Consulta cliente ecommerce",
								"subject"=> "Ha recibido una consulta",
								"address"=> "erdnando@gmail.com",
								"msgHtml"=>'
								<div style="width:100%; position:relative; font-family:sans-serif; padding-bottom:40px">
								<center><img style="padding:20px; width:200px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/logo1.png"></center>
								<div style="position:relative; margin:auto; width:600px; background:white; padding-bottom:20px">
									<center>
									<img style="padding-top:20px; width:70px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/icon-email.png">
									<h3 style="font-weight:100; color:#999;">HA RECIBIDO UNA CONSULTA</h3>
									<hr style="width:80%; border:1px solid #ccc">
									<h4 style="font-weight:100; color:#999; padding:0px 20px; text-transform:uppercase">'.$_POST["nombreContactenos"].'</h4>
									<h4 style="font-weight:100; color:#999; padding:0px 20px;">De: '.$_POST["emailContactenos"].'</h4>
									<h4 style="font-weight:100; color:#999; padding:0px 20px">'.$_POST["mensajeContactenos"].'</h4>
									<hr style="width:80%; border:1px solid #ccc">
									</center>
								</div>
							</div>');

					$respuestaCorreo = ControladorUsuarios::ctrMailgenerico($datosCorreo);
						
					if($respuestaCorreo!="ok"){
								echo '<script> 
									swal({
										title: "¡ERROR!",
										text: "¡Ha ocurrido un problema enviando el mensaje!",
										type:"error",
										confirmButtonText: "Cerrar",
										closeOnConfirm: false
										},
										function(isConfirm){
											if(isConfirm){
												
												history.back();
											}
									});
								</script>';

								
							}else{
								echo '<script> 
									swal({
									title: "¡OK!",
									text: "¡Su mensaje ha sido enviado, muy pronto le responderemos!",
									type: "success",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
									},
									function(isConfirm){
											if (isConfirm) {	  
												window.location = "index.php";	
												}
									});
								</script>';

								
							}

			}else{

				echo'<script>
					swal({
						  title: "¡ERROR!",
						  text: "¡Problemas al enviar el mensaje, revise que no tenga caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},
					function(isConfirm){
							 if (isConfirm) {	   
							   	window.location =  history.back();
							  } 
					});
					</script>';

					
					
					return "error";
			}
          //return $respuestaCorreo;
			
		}
	}

	/*=============================================
	FORMULARIO CONTACTENOS AJAX
	=============================================*/

	static public function ctrFormularioContactenosAjax($datos){

		

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $datos["nombre"]) &&
				preg_match('/^[,\\.\\a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $datos["mensaje"]) &&
				preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $datos["email"])){

					/*=============================================
					ENVÍO CORREO ELECTRÓNICO
					=============================================*/
					//$url = Ruta::ctrRuta();	
					$datosCorreo = array("tituloFrom"=>"Consulta cliente ecommerce",
								"subject"=> "Ha recibido una consulta",
								"address"=> "erdnando@gmail.com",
								"msgHtml"=>'
								<div style="width:100%; position:relative; font-family:sans-serif; padding-bottom:40px">
								<center><img style="padding:20px; width:200px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/logo1.png"></center>
								<div style="position:relative; margin:auto; width:600px; background:white; padding-bottom:20px">
									<center>
									<img style="padding-top:20px; width:70px;height:auto" src="https://dumbbells.mx/backoffice/vistas/img/plantilla/icon-email.png">
									<h3 style="font-weight:100; color:#999;">HA RECIBIDO UNA CONSULTA</h3>
									<hr style="width:80%; border:1px solid #ccc">
									<h4 style="font-weight:100; color:#999; padding:0px 20px; text-transform:uppercase">'.$datos["nombre"].'</h4>
									<h4 style="font-weight:100; color:#999; padding:0px 20px;">De: '.$datos["email"].'</h4>
									<h4 style="font-weight:100; color:#999; padding:0px 20px">'.$datos["mensaje"].'</h4>
									<hr style="width:80%; border:1px solid #ccc">
									</center>
								</div>
							</div>');

					$respuestaCorreo = ControladorUsuarios::ctrMailgenericoAjax($datosCorreo);
			}else{

				echo'<script>
					swal({
						  title: "¡ERROR!",
						  text: "¡Problemas al enviar el mensaje, revise que no tenga caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},
					function(isConfirm){
							 if (isConfirm) {	   
							   	window.location =  history.back();
							  } 
					});
					</script>';

					
					
					return "error";
			}
          return $respuestaCorreo;
			
		
	}

	//
	/*=============================================
	RECREA CARRITO USUARIO AJAX
	=============================================*/
	static public function ctrCarritoUsuarioAjax($datos){
		
		/*$datosEmail = array("email"=>$datos["carrito_email"]
						);*/
			
		$datosUsuario = ModeloUsuarios::mdlMostrarUsuario("usuarios","email", $datos["carrito_email"]);
		
		$datosCarrito = ModeloUsuarios::mdlObtenerCarritoUsuario("carritos","id_usuario", $datosUsuario["id"]);
		//var_dump($datosCarrito);

		//$arrJson = json_decode($datosCarrito["carrito_json"]);
		//var_dump($datosCarrito["carrito_json"]);

		return $datosCarrito["carrito_json"];
    }



	/*=============================================
	FORMULARIO RECOMIENDA USUARIO AJAX
	=============================================*/
	static public function ctrRecomendarUsuarioAjax($datos){
		

		if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $datos["ref_email"])){

			    $tabla = "recomendados";
				$datosReferenciado = array("email"=>$datos["yomismo"],
							   "emailReferenciado"=>$datos["ref_email"],
							  );
				 
				$respuesta = ModeloUsuarios::mdlInsertaRecomendado($tabla,$datosReferenciado);

				if($respuesta=="ok"){

					/*=============================================
					ENVÍO CORREO ELECTRÓNICO
					=============================================*/
					
					//require_once "../modelos/usuarios.modelo.php";
					require "../modelos/rutas.php";
					$url = Ruta::ctrRuta();	

					$variables = array();
					$variables['TEINVITA'] = $datos["yomismo"];
					
					$template = file_get_contents("plantilla-invitacion-correo.html",true);
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{ '.$key.' }}', $value, $template);
					}

					$datosCorreo = array("tituloFrom"=>'!Esta tienda esta super '.$datos["yomismo"].'!',
							   "subject"=> "!Dummbells y ".$datos["yomismo"]." te invitan a conocer esta tienda online!",
							   "address"=> $datos["ref_email"],
							   "msgHtml"=> $template);

					$respuestaCorreo = ControladorUsuarios::ctrMailgenericoAjax($datosCorreo);
				}

		}else{
				return "error";
		}
	  return $respuestaCorreo;
    }


	/*=============================================
	FORMULARIO ENVIO CORREO POR CUPON EN EL REGISTRO DE RED SOCIAL
	=============================================*/

	static public function ctrCuponXRegistro(){

		if(isset($_SESSION["validarSesion"])){

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_SESSION["nombre"]) &&
			preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_SESSION["email"])){


				$respuesta = ModeloUsuarios::mdlObtenerCuponRegistro('cupones','REGISTRO');

				/*=============================================
				ENVÍO CORREO ELECTRÓNICO
				=============================================*/

					date_default_timezone_set("America/Mexico_City");

					$url = Ruta::ctrRuta();	


					$variables = array();
					$variables['CUPON'] = $respuesta["codigo"];
					$variables['NOMBRE'] = $_SESSION["nombre"];

					$template = file_get_contents("plantilla-registro-nuevo.html",true);
					
					foreach($variables as $key => $value)
					{
						$template = str_replace('{{ '.$key.' }}', $value, $template);
					}

					$mail = new PHPMailer;

					try{

						$mail->CharSet = 'UTF-8';

						$mail->isSMTP();   
						$mail->Host       = 'mail.dumbbells.mx'; 

						$mail->SMTPAuth   = true;//true; 
						//$mail->SMTPAutoTLS = true; 
						$mail->SMTPOptions = array(
							'ssl' => array(
								'verify_peer' => false,
								'verify_peer_name' => false,
								'allow_self_signed' => true
							)
						);

						$mail->Username   = 'erdnando@dumbbells.mx'; 
						$mail->Password   = 'Adqdisp06ERV';  
						$mail->Port       = 587;    
						$mail->setFrom("contacto@dumbbells.mx", '!Su cupón de descuento por registrarse!');
					$mail->Subject = "!Su cupón de descuento por registrarse al ecommerce Dummbells!";
					$mail->addAddress($_SESSION["email"]);
					$mail->msgHTML($template);

						$envio = $mail->Send();

						if(!$envio){
                            //echo 'Mailer Error: ' . $mail->ErrorInfo;
							echo '<script> 

								swal({
									title: "¡ERROR!",
									text: "¡Ha ocurrido un problema enviando el mensaje!",
									type:"error",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
									},

									function(isConfirm){

										if(isConfirm){
											window.location = localStorage.getItem("rutaActual");
										}
								});

							</script>';

						}else{

							echo '<script> 

								swal({
									title: "¡Gracias por registrarte!",
									text: "¡Su cupón ha sido enviado a su correo, gracias por participar!",
								type: "success",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
								},

								function(isConfirm){
										if (isConfirm) {	  
											window.location = localStorage.getItem("rutaActual");
											}
								});

							</script>';
						}

				    }
                    catch (phpmailerException $e) {
						echo $e->errorMessage(); //Pretty error messages from PHPMailer
					} catch (Exception $e) {
						echo $e->getMessage(); //Boring error messages from anything else!
					}





			}else{

				echo'<script>

					swal({
						  title: "¡ERROR!",
						  text: "¡Problemas al enviar el mensaje, revise que no tenga caracteres especiales!",
						  type: "error",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
					},

					function(isConfirm){
							 if (isConfirm) {	   
							    window.location = localStorage.getItem("rutaActual");
							  } 
					});

					</script>';


			}

		}

	}

/*=============================================
	MAIL GENERICO
	=============================================*/

	static public function ctrMailgenericoAjax($datos){
				
						date_default_timezone_set("America/Mexico_City");

						//$url = Ruta::ctrRuta();
						require "../extensiones/PHPMailer/PHPMailerAutoload.php";
                        //require "../extensiones/vendor/autoload.php";	

						$mail = new PHPMailer();
						try{

							$mail->CharSet = 'UTF-8';
							$mail->isSMTP();   
							$mail->Host       = 'mail.dumbbells.mx'; 
							$mail->SMTPAuth   = true; 
							$mail->SMTPOptions = array(
								'ssl' => array(
									'verify_peer' => false,
									'verify_peer_name' => false,
									'allow_self_signed' => true
								)
							);
							$mail->Username   = 'erdnando@dumbbells.mx'; 
							$mail->Password   = 'Adqdisp06ERV';  
							$mail->Port       = 587;  

							$mail->setFrom('ecommerce@dumbell.com', $datos["tituloFrom"]);
							$mail->Subject = $datos["subject"];
							$mail->addAddress($datos["address"]);
							$mail->msgHTML($datos["msgHtml"]);

							$envio = $mail->Send();
							/*$envio = $mail->send->then(
								function() {
									echo 'Message sent via Google SMTP'.PHP_EOL;
								},
								function ( \Exception $ex ) {
									echo 'SMTP error '.$ex->getCode().' '.$ex->getMessage().PHP_EOL;
								}
							);*/

							

						
					    }catch(Exception $e){
							$envio=false;
						}

                        if(!$envio){
							return "error";
						}else{
							return "ok";
						}
						
	}





/*=============================================
	MAIL GENERICO 2
	=============================================*/
	
	static public function ctrMailgenericoAjax2($datos){



		$template = $datos["msgHtml"];
		if($template=="template1"){
			$url = Ruta::ctrRuta();	
					
			$encriptarEmail = md5($datos["address"]);
			$variables = array();
			$variables['URL'] = $url.'verificar/'.$encriptarEmail;
			$template = file_get_contents("plantilla-verifica-correo.html",true);

			foreach($variables as $key => $value)
			{
				$template = str_replace('{{ '.$key.' }}', $value, $template);
			}
		}else{
			$template = "Correo no disponible.";
		}




				
		date_default_timezone_set("America/Mexico_City");

		//$url = Ruta::ctrRuta();
		require "../extensiones/PHPMailer/PHPMailerAutoload.php";
		//require "../extensiones/vendor/autoload.php";	

		$mail = new PHPMailer();
		try{

			$mail->CharSet = 'UTF-8';
			$mail->isSMTP();   
			$mail->Host       = 'mail.dumbbells.mx'; 
			$mail->SMTPAuth   = true; 
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
			$mail->Username   = 'erdnando@dumbbells.mx'; 
			$mail->Password   = 'Adqdisp06ERV';  
			$mail->Port       = 587;  

			$mail->setFrom('ecommerce@dumbell.com', $datos["tituloFrom"]);
			$mail->Subject = $datos["subject"];
			$mail->addAddress($datos["address"]);
			$mail->msgHTML($template);

			$envio = $mail->Send();


			

		
		}catch(Exception $e){
			$envio=false;
		}

		if(!$envio){
			return "error";
		}else{
			return "ok";
		}
		
}


	/*=============================================
	MAIL GENERICO
	=============================================*/

	static public function ctrMailgenerico($datos){
				
		date_default_timezone_set("America/Mexico_City");

		//$url = Ruta::ctrRuta();
		

		$mail = new PHPMailer();
		try{

			$mail->CharSet = 'UTF-8';
			$mail->isSMTP();   
			$mail->Host       = 'mail.dumbbells.mx'; 
			$mail->SMTPAuth   = true; 
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
			$mail->Username   = 'erdnando@dumbbells.mx'; 
			$mail->Password   = 'Adqdisp06ERV';  
			$mail->Port       = 587;  

			$mail->setFrom('ecommerce@dumbell.com', $datos["tituloFrom"]);
			$mail->Subject = $datos["subject"];
			$mail->addAddress($datos["address"]);
			$mail->msgHTML($datos["msgHtml"]);

			$envio = $mail->Send();
		
		}catch(Exception $e){
			$envio=false;
		}

		if(!$envio){
			return "error";
		}else{
			return "ok";
		}
		
}



}