<?php

class ControladorCarrito{

	/*=============================================
	MOSTRAR TARIFAS
	=============================================*/

	public function ctrMostrarTarifas(){

		$tabla = "comercio";

		$respuesta = ModeloCarrito::mdlMostrarTarifas($tabla);

		return $respuesta;

	}	

	/*=============================================
	NUEVAS COMPRAS
	=============================================*/

	static public function ctrNuevasCompras($datos){

		$tabla = "compras";

		$respuesta = ModeloCarrito::mdlNuevasCompras($tabla, $datos);

		if($respuesta == "ok"){

			$tabla = "comentarios";
			ModeloUsuarios::mdlIngresoComentarios($tabla, $datos);

			/*=============================================
			ACTUALIZAR NOTIFICACIONES NUEVAS VENTAS
			=============================================*/

			$traerNotificaciones = ControladorNotificaciones::ctrMostrarNotificaciones();

			$nuevaVenta = $traerNotificaciones["nuevasVentas"] + 1;

			ModeloNotificaciones::mdlActualizarNotificaciones("notificaciones", "nuevasVentas", $nuevaVenta);


		}

		return $respuesta;

	}

	/*=============================================
	VERIFICAR PRODUCTO COMPRADO
	=============================================*/

	static public function ctrVerificarProducto($datos){
		$tabla = "compras";
		$respuesta = ModeloCarrito::mdlVerificarProducto($tabla, $datos);
	    return $respuesta;
	}

	/*=============================================
	OBTENER CUPON INGRESADO
	=============================================*/

	static public function ctrObtenerCupon($datos){
		$tabla = "cupones";
		$respuesta = ModeloCarrito::mdlObtenerCupon($tabla, $datos);
	    return $respuesta;
	}

	
	/*=============================================
	OBTENER STOCK DE UN PRODUCTO
	=============================================*/

	static public function ctrObtenerStock($datos){
		$tabla = "productos";
		$respuesta = ModeloCarrito::mdlObtenerStock($tabla, $datos);
	    return $respuesta;
	}

	
	/*=============================================
	PERSISTE CARRITO
	=============================================*/

	static public function ctrPersisteCarrito($datos){
		$tabla = "carritos";
		$respuesta = ModeloCarrito::mdlPersisteCarrito($tabla, $datos);
	    return $respuesta;
	}

	/*=============================================
	PAGO BILLPOCKET
	=============================================*/

	static public function ctrPagoBillPocket($datosBP, $datosTransaccion){

		//INSERT TRANSACCION INTO DB
		$tabla = "transacciones";
		$respuesta = ModeloCarrito::mdlInsertaTransaccion($tabla, $datosTransaccion);

		if($respuesta=="ok"){
            //BILLPOCKET XXX
		  $url = 'https://test.paywith.billpocket.com/api/v1/checkout';
			//$url = 'https://paywith.billpocket.com/api/v1/checkout';
			
		
			$options = array(
					'http' => array(
					'header'  => "Content-type: application/json\r\n",
					'method'  => 'POST',
					'content' => json_encode($datosBP),
				)
			);

				$context  = stream_context_create($options);
				$result = file_get_contents( $url, false, $context );
				$response = json_decode( $result );
			
			return $response;

		}else{
			echo "error";
		}
		
		
	}


    /*=============================================
	OBTENER CARRITOS ABANDONADOS
	=============================================*/

	public function ctrObtenrCarritosAbandonados(){

		$tabla = "carritos";

		$respuesta = ModeloCarrito::mdlObtenerCarritosAbandonados($tabla);

		//var_dump($respuesta);

		return $respuesta;

	}	


}