<?php

require_once "../extensiones/paypal.controlador.php";

require_once "../controladores/carrito.controlador.php";
require_once "../modelos/carrito.modelo.php";

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

class AjaxCarrito{

	/*=============================================
	MÉTODO PAYPAL
	=============================================*/	

	public $divisa;
	public $total;
	public $totalEncriptado;
	public $impuesto;
	public $envio;
	public $subtotal;
	public $tituloArray;
	public $cantidadArray;
	public $valorItemArray;
	public $idProductoArray;

	public function ajaxEnviarPaypal(){
		if(md5($this->total) == $this->totalEncriptado){
				$datos = array(
						"divisa"=>$this->divisa,
						"total"=>$this->total,
						"impuesto"=>$this->impuesto,
						"envio"=>$this->envio,
						"subtotal"=>$this->subtotal,
						"tituloArray"=>$this->tituloArray,
						"cantidadArray"=>$this->cantidadArray,
						"valorItemArray"=>$this->valorItemArray,
						"idProductoArray"=>$this->idProductoArray,
					);

				$respuesta = Paypal::mdlPagoPaypal($datos);
				echo $respuesta;
		}
	}
	/*=============================================
	MÉTODO PAYU
	=============================================*/
	public function ajaxTraerComercioPayu(){
		$respuesta = ControladorCarrito::ctrMostrarTarifas(); 
		echo json_encode($respuesta);
	}
	
	/*=============================================
	OBTENER CUPON
	=============================================*/
	public $codigoCupon;
	public function ajaxObtenerCupon(){

		$datos = array("codigoCupon"=>$this->codigoCupon);

		$respuesta = ControladorCarrito::ctrObtenerCupon($datos); 
		echo json_encode($respuesta);
	}


    /*=============================================
	VALIDA STOCK
	=============================================*/
	public $cantidad_stock;
	public $precio_stock;
	public $item_stock;
	public $objResp;
	public function ajaxValidastock(){

		$datos = array("idProducto"=>$this->idProducto);

		$respuesta = ControladorCarrito::ctrObtenerStock($datos); 

		$objResp = new stdClass();
		$objResp->idProducto = $this->idProducto;
		$objResp->cantidad = $this->cantidad_stock;
		$objResp->precio = $this->precio_stock;
		$objResp->item = $this->item_stock;
		$objResp->stock = $respuesta;

		echo json_encode($objResp);
	}

	
	/*=============================================
	PERSISTE CARRITO
	=============================================*/
	public $carrito_json;
	public $usuarioCarrito;
	public function ajaxPersisteCarrito(){

		$datos = array("usuarioCarrito"=>$this->usuarioCarrito,
					   "carrito_json"=>$this->carrito_json);

		$respuesta = ControladorCarrito::ctrPersisteCarrito($datos); 
		echo json_encode($respuesta);
	}

	/*=============================================
	VERIFICAR QUE NO TENGA EL PRODUCTO ADQUIRIDO
	=============================================*/
	public $idUsuario;
	public $idProducto;

	public function ajaxVerificarProducto(){
		$datos = array("idUsuario"=>$this->idUsuario,
					   "idProducto"=>$this->idProducto);
		$respuesta = ControladorCarrito::ctrVerificarProducto($datos);
		echo json_encode($respuesta);
	}



	/*=============================================
	REALIZA PAGO BILL POCKET
	=============================================*/
	public $xidDireccion;
	public $xidUsuario;
	public $xtotal;
	public $xdescuento;//
	public $xtotalEncriptado;
	public $ximpuesto;//
	public $xenvio;//
	public $xsubtotal;//
	public $xcantidadArray;//
	public $xvalorItemArray;//
	public $xidProductoArray;//
	public $xtituloArray;//
	public $xtransaccionId;

	public function ajaxPagoBillPocket(){

		$transaccionId = rand(1001,9999999999);

		//desarrollo
		//H9X88DT-T8WMQZH-HFQRT29-RXY2233

		//prod
		//A17K9M4-YH34AWR-JWTAQEP-K32E9SV
		//BILLPOCKET XXX
		
		if(md5($this->xtotal) == $this->xtotalEncriptado){
			$datosBP = array(
					"apiKey"=>"H9X88DT-T8WMQZH-HFQRT29-RXY2233",
					"externalId"=>$this->xtransaccionId,
					"items"=>$this->xtituloArray,
					"total"=>$this->xtotal
				);

			$jsonDetalle = new stdClass();
			$jsonDetalle->xdescuento = $this->xdescuento;
			$jsonDetalle->ximpuesto = $this->ximpuesto;
			$jsonDetalle->xenvio = $this->xenvio;
			$jsonDetalle->xsubtotal = $this->xsubtotal;
			$jsonDetalle->xcantidadArray = $this->xcantidadArray;
			$jsonDetalle->xvalorItemArray = $this->xvalorItemArray;
			$jsonDetalle->xidProductoArray = $this->xidProductoArray;
			$jsonDetalle->xtituloArray = $this->xtituloArray;

			$myJSON = json_encode($jsonDetalle);

			$datosTransaccion = array(
					"transaccionId"=>$this->xtransaccionId,
					"idUsuario"=>$this->xidUsuario,
					"idDomicilio"=>$this->xidDireccion,
					"total"=>$this->xtotal,
					"detalle"=>$myJSON
				);

				$respuesta = ControladorCarrito::ctrPagoBillPocket($datosBP, $datosTransaccion); 
				echo json_encode($respuesta);
	}

	}

}

/*=============================================
MÉTODO PAYPAL
=============================================*/	
if(isset($_POST["divisa"])){
	$idProductos = explode("," , $_POST["idProductoArray"]);
	$cantidadProductos = explode("," , $_POST["cantidadArray"]);
	$precioProductos = explode("," , $_POST["valorItemArray"]);
	$item = "id";

	for($i = 0; $i < count($idProductos); $i ++){
		$valor = $idProductos[$i];
		$verificarProductos = ControladorProductos::ctrMostrarInfoProducto($item, $valor);
		$divisa = file_get_contents("https://free.currconv.com/api/v7/convert?q=MXN_".$_POST["divisa"]."&compact=ultra&apiKey=a01ebaf9a1c69eb4ff79");
		$jsonDivisa = json_decode($divisa, true);
		$conversion = number_format($jsonDivisa["MXN_".$_POST["divisa"]],2);

		if($verificarProductos["precioOferta"] == 0){
			$precio = $verificarProductos["precio"]*$conversion;
		}else{
			$precio = $verificarProductos["precioOferta"]*$conversion;
		}

		$verificarSubTotal = $cantidadProductos[$i]*$precio;
		// echo number_format($verificarSubTotal,2)."<br>";
		// echo number_format($precioProductos[$i],2)."<br>";
		// return;

		if(number_format($verificarSubTotal,2) != number_format($precioProductos[$i],2)){
			echo "carrito-de-compras";
			return;
		}
	}

	$paypal = new AjaxCarrito();
	$paypal ->divisa = $_POST["divisa"];
	$paypal ->total = $_POST["total"];
	$paypal ->totalEncriptado = $_POST["totalEncriptado"];
	$paypal ->impuesto = $_POST["impuesto"];
	$paypal ->envio = $_POST["envio"];
	$paypal ->subtotal = $_POST["subtotal"];
	$paypal ->tituloArray = $_POST["tituloArray"];
	$paypal ->cantidadArray = $_POST["cantidadArray"];
	$paypal ->valorItemArray = $_POST["valorItemArray"];
	$paypal ->idProductoArray = $_POST["idProductoArray"];
	$paypal -> ajaxEnviarPaypal();
}

/*=============================================
MÉTODO PAYU
=============================================*/	

if(isset($_POST["metodoPago"]) && $_POST["metodoPago"] == "payu"){

	$idProductos = explode("," , $_POST["idProductoArray"]);
	$cantidadProductos = explode("," , $_POST["cantidadArray"]);
	$precioProductos = explode("," , $_POST["valorItemArray"]);
	$item = "id";

	for($i = 0; $i < count($idProductos); $i ++){
		$valor = $idProductos[$i];
		$verificarProductos = ControladorProductos::ctrMostrarInfoProducto($item, $valor);
		$divisa = file_get_contents("https://free.currconv.com/api/v7/convert?q=MXN_".$_POST["divisaPayu"]."&compact=ultra&apiKey=a01ebaf9a1c69eb4ff79");
		$jsonDivisa = json_decode($divisa, true);
		$conversion = number_format($jsonDivisa["MXN_".$_POST["divisaPayu"]],2);

		if($verificarProductos["precioOferta"] == 0){
			$precio = $verificarProductos["precio"]*$conversion;
		}else{
			$precio = $verificarProductos["precioOferta"]*$conversion;
		}

		$verificarSubTotal = $cantidadProductos[$i]*$precio;

		// echo number_format($verificarSubTotal,2)."<br>";
		// echo number_format($precioProductos[$i],2)."<br>";

		// return;
		if(number_format($verificarSubTotal,2) != number_format($precioProductos[$i],2)){
			echo "carrito-de-compras";
			return;
		}
	}

	$payu = new AjaxCarrito();
	$payu -> ajaxTraerComercioPayu();
}

/*=============================================
VERIFICAR QUE NO TENGA EL PRODUCTO ADQUIRIDO
=============================================*/	

if(isset($_POST["idUsuario"])){

	$deseo = new AjaxCarrito();
	$deseo -> idUsuario = $_POST["idUsuario"];
	$deseo -> idProducto = $_POST["idProducto"];
	$deseo ->ajaxVerificarProducto();
}

/*=============================================
VERIFICAR CUPON
=============================================*/	

if(isset($_POST["codigoCupon"])){

	$cupon = new AjaxCarrito();
	$cupon -> codigoCupon = $_POST["codigoCupon"];
	$cupon ->ajaxObtenerCupon();
}


/*=============================================
PERSISTE CARRITO
=============================================*/	

if(isset($_POST["carrito_json"])){

	$carrito = new AjaxCarrito();
	$carrito -> carrito_json = $_POST["carrito_json"];
	$carrito -> usuarioCarrito = $_POST["usuarioCarrito"];
	$carrito ->ajaxPersisteCarrito();
}


/*=============================================
BILL POCKET
=============================================*/	
if(isset($_POST["x-pago"])){

	$pago = new AjaxCarrito();
	$pago -> xtransaccionId = $_POST["x-transaccionId"];
	$pago -> xidDireccion = $_POST["x-idDireccion"];
	$pago -> xidUsuario = $_POST["x-idUsuario"];
	
	$pago -> xtotal = $_POST["x-total"];
	$pago -> xdescuento = $_POST["x-descuento"];
	$pago -> xtotalEncriptado = $_POST["x-totalEncriptado"];
	$pago -> ximpuesto = $_POST["x-impuesto"];
	$pago -> xenvio = $_POST["x-envio"];
	$pago -> xsubtotal = $_POST["x-subtotal"];
	$pago -> xcantidadArray = $_POST["x-cantidadArray"];
	$pago -> xvalorItemArray = $_POST["x-valorItemArray"];
	$pago -> xidProductoArray = $_POST["x-idProductoArray"];
	$pago -> xtituloArray = $_POST["x-tituloArray"];
	

	$pago ->ajaxPagoBillPocket();
}



/*=============================================
VALIDA STOCK CARRITO
=============================================*/	

if(isset($_POST["idProducto_stock"])){

	$carrito = new AjaxCarrito();
	$carrito -> idProducto = $_POST["idProducto_stock"];
	$carrito -> cantidad_stock = $_POST["cantidad_stock"];
	$carrito -> precio_stock = $_POST["precio_stock"];
	$carrito -> item_stock = $_POST["item_stock"];
	$carrito ->ajaxValidastock();
}
