<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../modelos/rutas.php";

class AjaxMail{



	/*=============================================
	REGISTRO CON FACEBOOK
	=============================================*/

	public $email;
	public $nombre;

	public function ajaxEnvioCorreoCupon(){

		$datos = array("nombre"=>$this->nombre,
					   "email"=>$this->email
					  );

		$respuesta = ControladorUsuarios::ctrSendemailCupon($datos);

		echo $respuesta;
	}	


	/*=============================================
	REGISTRO EMAIL
	=============================================*/

	public $opEmailRegistro_tituloFrom;
	public $opEmailRegistro_subject;
	public $opEmailRegistro_address;
	public $opEmailRegistro_msgHtml;

	public function ajaxRegistroCorreoCupon(){

		

		$datos = array("tituloFrom"=>$this->opEmailRegistro_tituloFrom,
					   "subject"=>$this->opEmailRegistro_subject,
					   "address"=>$this->opEmailRegistro_address,
					   "msgHtml"=>$this->opEmailRegistro_msgHtml
					  );

		$respuesta = ControladorUsuarios::ctrMailgenericoAjax2($datos);

		echo $respuesta;
	}	





















}

/*=============================================
ENVIA EMAIL-CUPON POR REGISTRO
=============================================*/	

if(isset($_POST["email"])){

	$sendEmail = new AjaxMail();
	$sendEmail -> email = $_POST["email"];
	$sendEmail -> nombre = $_POST["nombre"];
	$sendEmail -> contrasenia = $_POST["contrasenia"];
	$sendEmail -> ajaxEnvioCorreoCupon();

}


/*=============================================
ENVIA EMAIL-CUPON POR REGISTRO
=============================================*/	

if(isset($_POST["opEmailRegistro"])){

	$sendEmail = new AjaxMail();
	$sendEmail -> opEmailRegistro_tituloFrom = $_POST["opEmailRegistro-tituloFrom"];
	$sendEmail -> opEmailRegistro_subject = $_POST["opEmailRegistro-subject"];
	$sendEmail -> opEmailRegistro_address = $_POST["opEmailRegistro-address"];
	$sendEmail -> opEmailRegistro_msgHtml = $_POST["opEmailRegistro-msgHtml"];
	$sendEmail -> ajaxRegistroCorreoCupon();

}
