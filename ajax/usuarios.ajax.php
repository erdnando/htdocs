<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../controladores/notificaciones.controlador.php";
require_once "../modelos/usuarios.modelo.php";
require_once "../modelos/notificaciones.modelo.php";
require_once "../modelos/rutas.php";

class AjaxUsuarios{

	/*=============================================
	VALIDAR EMAIL EXISTENTE
	=============================================*/	

	public $validarEmail;

	public function ajaxValidarEmail(){

		$datos = $this->validarEmail;

		$respuesta = ControladorUsuarios::ctrMostrarUsuario("email", $datos);

		echo json_encode($respuesta);

	}

	/*=============================================
	REGISTRO CON FACEBOOK
	=============================================*/

	public $email;
	public $nombre;
	public $foto;
	public $cupon;

	public function ajaxRegistroFacebook(){

		$datos = array("nombre"=>$this->nombre,
					   "email"=>$this->email,
					   "foto"=>$this->foto,
					   "password"=>"null",
					   "modo"=>"facebook",
					   "cupon"=>$this->cupon,
					   "verificacion"=>0,
					   "emailEncriptado"=>"null");

					   try{
						$usuarioExistente = ControladorUsuarios::ctrMostrarUsuarioHistorico("email", $this->email);

						if($usuarioExistente){
							//update
							$respuesta = ControladorUsuarios::ctrActualizarUsuario($usuarioExistente["id"], "estatus", 1);

							session_start();
							$_SESSION["validarSesionAux"] = "ok";
							$_SESSION["validarSesion"] = "ok";
							$_SESSION["id"] = $usuarioExistente["id"];
							$_SESSION["nombre"] = $usuarioExistente["nombre"];
							$_SESSION["foto"] = $usuarioExistente["foto"];
							$_SESSION["email"] = $usuarioExistente["email"];
							$_SESSION["password"] = $usuarioExistente["password"];
							$_SESSION["modo"] = $usuarioExistente["modo"];

							$respuesta="noCupon";
						}else{
							//insert
							$respuesta = ControladorUsuarios::ctrRegistroRedesSociales($datos);
							$respuesta="ok";
						}

						$recomendado = ModeloUsuarios::mdlObtenerRecomendado('recomendados',$this->email);
                        //si es un recomendado
                        if($recomendado != null){
                            //Marca el campo aplicado como 1                                           
                            $recomendadoAplicado = ModeloUsuarios::mdlActualizaRecomendado('recomendados',$this->email);
                        }


					   }catch(Exception $e){
						$respuesta = $e->getMessage();
					   }
		echo $respuesta;
	}
	
	/*=============================================
	REGISTRO CON FACEBOOK
	=============================================*/
	public $login;

	public function ajaxLoginRedSocial(){
		$datos = array("email"=>$this->email);
		$respuesta = ControladorUsuarios::ctrLoginRedesSociales($datos);
		echo $respuesta;
	}

	/*=============================================
	AGREGAR A LISTA DE DESEOS
	=============================================*/	
	public $idUsuario;
	public $idProducto;

	public function ajaxAgregarDeseo(){
		$datos = array("idUsuario"=>$this->idUsuario,
					   "idProducto"=>$this->idProducto);
		$respuesta = ControladorUsuarios::ctrAgregarDeseo($datos);
		echo $respuesta;
	}

	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/
	public $idDeseo;	

	public function ajaxQuitarDeseo(){
		$datos = $this->idDeseo;
		$respuesta = ControladorUsuarios::ctrQuitarDeseo($datos);
		echo $respuesta;
	}
	
	/*=============================================
	QUITAR PRODUCTO DE LISTA DE DESEOS
	=============================================*/
	public $idDomicilio;	

	public function ajaxQuitarDomicilio(){
		$datos = $this->idDomicilio;
		$respuesta = ControladorUsuarios::ctrQuitarDomicilio($datos);
		echo $respuesta;
	}


	public $nuevopwd;	

	public function ajaxNuevoPwd(){
		$datos = $this->nuevopwd;
		$respuesta = ControladorUsuarios::ctrOlvidoPassword();
		echo $respuesta;
	}

/*=============================================
	CONTACTANOS
	=============================================*/
	public $mensaje;

	public function ajaxOpContactanos(){
		$datos = array("nombre"=>$this->nombre,
		               "email"=>$this->email,
					   "mensaje"=>$this->mensaje);

		$respuesta = ControladorUsuarios::ctrFormularioContactenosAjax($datos);
		echo $respuesta;
	}


	/*=============================================
	EMAIL REFERENCIADO
	=============================================*/
	public $yomismo;
	public $ref_email;
	public $ref_nombre;

	public function ajaxOpEmailReferenciado(){
		$datos = array("yomismo"=>$this->yomismo,
		               "ref_nombre"=>$this->ref_nombre,
		               "ref_email"=>$this->ref_email);

		$respuesta = ControladorUsuarios::ctrRecomendarUsuarioAjax($datos);
		echo $respuesta;
	}


	/*=============================================
	CARRITO POR EMAIL
	=============================================*/
	public $carrito_email;

	public function ajaxOpCarritoEmail(){
		$datos = array("carrito_email"=>$this->carrito_email);
		$respuesta = ControladorUsuarios::ctrCarritoUsuarioAjax($datos);
		//return $respuesta;
		echo $respuesta;
	}


//---------------
/*=============================================
	CARRITO POR EMAIL
	=============================================*/
	public $registro_nombre;
	public $registro_email;
	public $registro_contrasenia;

	public function ajaxOpRegistroCupon(){
		$datos = array("nombre"=>$this->registro_nombre,
		"email"=>$this->registro_email,
		"contrasenia"=>$this->registro_contrasenia);

		$respuesta = ControladorUsuarios::ctrRegistroUsuarioConcuponAjax($datos);
		//return $respuesta;
		echo $respuesta;
	}




}

/*=============================================
VALIDAR EMAIL EXISTENTE
=============================================*/	

if(isset($_POST["validarEmail"])){
	$valEmail = new AjaxUsuarios();
	$valEmail -> validarEmail = $_POST["validarEmail"];
	$valEmail -> ajaxValidarEmail();
}

/*=============================================
REGISTRO CON FACEBOOK
=============================================*/
if(isset($_POST["email"])){
	$regFacebook = new AjaxUsuarios();
	$regFacebook -> email = $_POST["email"];
	$regFacebook -> nombre = $_POST["nombre"];
	$regFacebook -> foto = $_POST["foto"];
	$regFacebook -> cupon = $_POST["cupon"];
	$regFacebook -> ajaxRegistroFacebook();
}

/*=============================================
AGREGAR A LISTA DE DESEOS
=============================================*/	
if(isset($_POST["idUsuario"])){
	$deseo = new AjaxUsuarios();
	$deseo -> idUsuario = $_POST["idUsuario"];
	$deseo -> idProducto = $_POST["idProducto"];
	$deseo ->ajaxAgregarDeseo();
}

/*=============================================
QUITAR PRODUCTO DE LISTA DE DESEOS
=============================================*/
if(isset($_POST["idDeseo"])){
	$quitarDeseo = new AjaxUsuarios();
	$quitarDeseo -> idDeseo = $_POST["idDeseo"];
	$quitarDeseo ->ajaxQuitarDeseo();
}

/*=============================================
QUITAR DOMICILIO DE LISTA DE DOMICILIOS
=============================================*/
if(isset($_POST["idDomicilio"])){
	$quitarDomicilioo = new AjaxUsuarios();
	$quitarDomicilioo -> idDomicilio = $_POST["idDomicilio"];
	$quitarDomicilioo ->ajaxQuitarDomicilio();
}

if(isset($_POST["passEmail"])){
	$nuevopwd = new AjaxUsuarios();
	$nuevopwd -> nuevopwd = $_POST["passEmail"];
	$nuevopwd ->ajaxNuevoPwd();
}

/*=============================================
LOGIN RED SOCIAL
=============================================*/
if(isset($_POST["login"])){
	$login = new AjaxUsuarios();
	$login -> login = $_POST["login"];
	$login -> email = $_POST["emailLogin"];
	$login ->ajaxLoginRedSocial();
}

/*=============================================
CONTACTANOS
=============================================*/
if(isset($_POST["opContactanos"])){
	$login = new AjaxUsuarios();
	$login -> nombre = $_POST["opContactanos-nombre"];
	$login -> email = $_POST["opContactanos-email"];
	$login -> mensaje = $_POST["opContactanos-mensaje"];
	$login ->ajaxOpContactanos();
}

/*=============================================
EMAIL REFERENCIADO
=============================================*/
if(isset($_POST["opEmailReferenciado"])){
	$usuario = new AjaxUsuarios();
	$usuario -> yomismo = $_POST["opEmailReferenciado-yomismo"];
	$usuario -> ref_email = $_POST["opEmailReferenciado-email"];
	$usuario -> ref_nombre = $_POST["opEmailReferenciado-nombre"];
	$usuario ->ajaxOpEmailReferenciado();
}

/*=============================================
GET CARRITO POR EMAIL
=============================================*/
if(isset($_POST["optCarritoEmail"])){

	$usuario = new AjaxUsuarios();
	$usuario -> carrito_email = $_POST["optCarritoEmail-email"];
	$usuario ->ajaxOpCarritoEmail();
}



/*=============================================
REGISTRO USUARIO CUPON
=============================================*/
if(isset($_POST["opRegistroCupon"])){

	$usuario = new AjaxUsuarios();
	$usuario -> registro_nombre = $_POST["opRegistroCupon-nombre"];
	$usuario -> registro_email = $_POST["opRegistroCupon-email"];
	$usuario -> registro_contrasenia = $_POST["opRegistroCupon-contrasenia"];
	$usuario ->ajaxOpRegistroCupon();
}
