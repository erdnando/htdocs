<?php

require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

class AjaxDomicilios{

/*=============================================
	OBTENER DOMICILIOS
	=============================================*/
	public $id;	
	public function ajaxGetDomicilios(){
		$datos = $this->id;
		$item = "id_usuario";
		//$valor = $_POST["id"];
		$respuesta = ControladorUsuarios::ctrMostrarDirecciones($item, $datos);
		echo json_encode($respuesta);
	}



	/*=============================================
	OBTENER DOMICILIOS X ID
	=============================================*/
	public $idDomicilio;	
	public function ajaxGetCPXid(){
		$datos = $this->idDomicilio;
		$item = "id";
		//$valor = $_POST["id"];
		$respuesta = ControladorUsuarios::ctrMostrarDirecciones($item, $datos);
		echo json_encode($respuesta);
	}
}
/*=============================================
OBTENER LISTA DE DOMICILIOS
=============================================*/

if(isset($_POST["id"])){

	$obtenerDomicilios = new AjaxDomicilios();
	$obtenerDomicilios -> id = $_POST["id"];
	$obtenerDomicilios ->ajaxGetDomicilios();
}

if(isset($_POST["idDomicilio"])){

	$obtenerDomicilios = new AjaxDomicilios();
	$obtenerDomicilios -> idDomicilio = $_POST["idDomicilio"];
	$obtenerDomicilios ->ajaxGetCPXid();
}

